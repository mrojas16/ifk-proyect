<?php

namespace XWork;

use \Exception;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        index.php
 * @desc        Arranque inicial del Sistema
 * @subpackage  XWork
 *
 */
final class index {

    private function __construct() {
        
    }

    /**
     * Sistema de Arranque inicial
     */
    public static function init() {
        self::verify(CORE, 'Autoload.php');
        try {
            Core\Autoload::init();
            self::runConfigs();
            Core\Session::init();
            try {
                Core\Bootstrap::run(new Core\Request);
            } catch (\Exception $exc) {
                Core\Error::exception($exc);
            }
        } catch (Exception $exc) {
            echo die('{"RESPONSE":0,"DATA":{"MESSAGE":"AUTOLOAD FAILED","EXTENDED_MESSAGE":"' . $exc->getMessage() . '"}}');
        }
    }

    /**
     * Verifica la existencia de un modulo
     * @param string $dir Ubicacion del archivo
     * @param string $file nombre del archivo
     * @throws Exception
     */
    private static function verify($dir, $file) {
        if (file_exists($dir . $file)) {
            require_once $dir . $file;
        } else {
            throw new \Exception('Error al intentar cargar el modulo "' . str_replace('.php', '', $file) . '" (' . $dir . $file . ') no se ha encontrado o no existe! es necesario para iniciar los servicios, contacte a su administrador');
        }
    }

    /**
     * Ejecuta las configuraciones iniciales
     */
    private static function runConfigs() {
        if (DEVELOPMENT_ENVIRONMENT) {
            ini_set('display_errors', 1);
            error_reporting(-1);
            ini_set('error_reporting', E_ALL);
        } else {
            ini_set('display_errors', 1);
            error_reporting(E_ALL ^ E_NOTICE);
            ini_set('error_reporting', E_ALL ^ E_NOTICE);
        }

        set_exception_handler(array("\XWork\Core\Error", "exception"));
        set_error_handler(array("\XWork\Core\Error", "trigger_error"));

        $dir = opendir(CONFIGS);
        while ($file = readdir($dir)) {
            if (!is_dir($file)) {
                self::verify(CONFIGS, $file);
            }
        }

        date_default_timezone_set(TIMEZONE);
        setlocale(LC_ALL, "es_CL");
    }
    
    /**
     * Retorn el tiempo para Benchmark
     * @return float
     */
    public static function getmicrotime() {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    /**
     * Powered By XWORK
     * @return string
     */
    public static function powerby() {
        return 'Powered by <a href="http://xwork.codestore.cl/">XWork Framework API</a>.';
    }

    /**
     * Benchmark; tiempo de carga
     * @param boolean $html formato de salida
     * @return mixed
     */
    public static function benchmark($html = false) {
        $tiempo_fin = self::getmicrotime();
        $tiempo_total = round($tiempo_fin - START_TIME, 3);
        if ($html) {
            return '<!-- Generado en ' . $tiempo_total . ' segundos -->';
        } else {
            return $tiempo_total;
        }
    }

    /**
     * Version
     * @return array
     */
    public static function version() {
        return array('XWORK' => '1.0.0 v7');
    }

    public function __clone() {
        return false;
    }

    public function __wakeup() {
        return false;
    }

}

ini_set('display_errors', 1);
error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', realpath(dirname(__FILE__)) . DS);

define('APP', ROOT . 'app' . DS);
define('CORE', ROOT . 'core' . DS);
define('EXCEPTIONS', CORE . 'exceptions' . DS);
define('DBCONNECTORS', CORE . 'dbconectors' . DS);
define('INTERFACES', CORE . 'interfaces' . DS);
define('CONFIGS', CORE . 'config' . DS);
define('WAREHOUSE', ROOT . 'warehouse' . DS);
define('TEMP', ROOT . 'temp' . DS);

define('XWORKVERSION', '1.0.0 v7');

define('DEVELOPMENT_ENVIRONMENT', TRUE);

if (!defined('__HALT_XWORK')) {
    index::init();
}


//php /Users/mrojas/Documents/Resources/PHPDOCUMENTATOR/apigen.phar generate -s "/Users/mrojas/Sites/dev.xwork.cod" -d "/Users/mrojas/Sites/dev.xwork.cod/DOCUMENTATION/ApiGen" --title "XWORK 1.0.9 v7" 