<?php 
/* Creado con XMUG v0.1.00 */

namespace XWork\ORMConnectors\fbr_acopio;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\ORM;
use \XWork\Core\ORM\ORMTable;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 * 
 * @name        fbr_recepcion.php
* @desc        Database Abstraction Layer For ORM 
 * @database    fbr_acopio
 * @table       fbr_recepcion
 * @subpackage  XWork\ORMConnectors\fbr_acopio
 *
 */
class fbr_recepcion extends ORM implements ORMTable {

    public $idrecepcion;
    public $idacopio;
    public $idtemporada;
    public $idproductor;
    public $idproducto;
    public $fecha_operacion;
    public $correlativo;
    public $num_guia;
    public $patente;
    public $chofer;
    public $cant_muestra;
    public $res_iqf;
    public $res_pulpa;
    public $kilos_netos;
    public $cant_bandejas;
    public $precio_iqf;
    public $precio_pulpa;
    public $estado;
    public $observaciones;
    public $num_oc;
    public $estado_envio = "0";
    public $fecha_creacion;
    public $fecha_edicion;
    public $fecha_eliminacion;
    public $idcreador;
    public $idmodificador;
    public $ideliminador;
    public $activo = "1";

    public static $_primaryKey = "idrecepcion";

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function foreignKeys(){
        return array(
           "idacopio"=>array(
                    "schema"=>"fbr_acopio",
                    "table"=>"fbr_acopio",
                    "column"=>"idacopio",
                ),
           "idtemporada"=>array(
                    "schema"=>"fbr_acopio",
                    "table"=>"fbr_temporada",
                    "column"=>"idtemporada",
                ),
           "idproductor"=>array(
                    "schema"=>"fbr_acopio",
                    "table"=>"fbr_productor",
                    "column"=>"idproductor",
                ),
           "idproducto"=>array(
                    "schema"=>"fbr_acopio",
                    "table"=>"fbr_producto",
                    "column"=>"idproducto",
                ),
        );
    }

    public static function describe(){
        return array(
            "idrecepcion"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>"auto_increment"
                ),
            "idacopio"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idtemporada"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idproductor"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idproducto"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_operacion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "correlativo"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "num_guia"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "patente"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "chofer"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "cant_muestra"=>array(
                    "TYPE"=>"float",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "res_iqf"=>array(
                    "TYPE"=>"float",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "res_pulpa"=>array(
                    "TYPE"=>"float",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "kilos_netos"=>array(
                    "TYPE"=>"float",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "cant_bandejas"=>array(
                    "TYPE"=>"float",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "precio_iqf"=>array(
                    "TYPE"=>"float",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "precio_pulpa"=>array(
                    "TYPE"=>"float",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "estado"=>array(
                    "TYPE"=>"tinyint",
                    "LENGTH"=>1,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "observaciones"=>array(
                    "TYPE"=>"text",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "num_oc"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "estado_envio"=>array(
                    "TYPE"=>"tinyint",
                    "LENGTH"=>1,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"0",
                    "EXTRA"=>""
                ),
            "fecha_creacion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_edicion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_eliminacion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idcreador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idmodificador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "ideliminador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "activo"=>array(
                    "TYPE"=>"tinyint",
                    "LENGTH"=>1,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"1",
                    "EXTRA"=>""
                ),
        );
    }
}
