<?php 
/* Creado con XMUG v0.1.00 */

namespace XWork\ORMConnectors\fbr_acopio;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\ORM;
use \XWork\Core\ORM\ORMTable;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 * 
 * @name        fbr_mov_envase.php
* @desc        Database Abstraction Layer For ORM 
 * @database    fbr_acopio
 * @table       fbr_mov_envase
 * @subpackage  XWork\ORMConnectors\fbr_acopio
 *
 */
class fbr_mov_envase extends ORM implements ORMTable {

    public $idmov_envase;
    public $idtemporada;
    public $idacopio;
    public $idproductor;
    public $tipo_movimiento;
    public $flujo_ajuste;
    public $num_guia;
    public $patente;
    public $nombre_chofer;
    public $observacion;
    public $fecha_operacion;
    public $fecha_creacion;
    public $fecha_edicion;
    public $fecha_eliminacion;
    public $idcreador;
    public $idmodificador;
    public $ideliminador;
    public $activo = "1";

    public static $_primaryKey = "idmov_envase";

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function foreignKeys(){
        return array(
           "idacopio"=>array(
                    "schema"=>"fbr_acopio",
                    "table"=>"fbr_acopio",
                    "column"=>"idacopio",
                ),
           "idproductor"=>array(
                    "schema"=>"fbr_acopio",
                    "table"=>"fbr_productor",
                    "column"=>"idproductor",
                ),
           "idtemporada"=>array(
                    "schema"=>"fbr_acopio",
                    "table"=>"fbr_temporada",
                    "column"=>"idtemporada",
                ),
        );
    }

    public static function describe(){
        return array(
            "idmov_envase"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>"auto_increment"
                ),
            "idtemporada"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idacopio"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idproductor"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>TRUE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "tipo_movimiento"=>array(
                    "TYPE"=>"tinyint",
                    "LENGTH"=>1,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "flujo_ajuste"=>array(
                    "TYPE"=>"tinyint",
                    "LENGTH"=>1,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "num_guia"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>20,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "patente"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>10,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "nombre_chofer"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>200,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "observacion"=>array(
                    "TYPE"=>"text",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_operacion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_creacion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_edicion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_eliminacion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idcreador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idmodificador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "ideliminador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "activo"=>array(
                    "TYPE"=>"tinyint",
                    "LENGTH"=>1,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"1",
                    "EXTRA"=>""
                ),
        );
    }
}
