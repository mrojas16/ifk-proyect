<?php 
/* Creado con XMUG v0.1.00 */

namespace XWork\ORMConnectors\fbr_acopio;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\ORM;
use \XWork\Core\ORM\ORMTable;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 * 
 * @name        fbr_permiso.php
* @desc        Database Abstraction Layer For ORM 
 * @database    fbr_acopio
 * @table       fbr_permiso
 * @subpackage  XWork\ORMConnectors\fbr_acopio
 *
 */
class fbr_permiso extends ORM implements ORMTable {

    public $idperfil;
    public $idmenu;

    public static $_primaryKey = "idtemporada";

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function foreignKeys(){
        return array(
           "idperfil"=>array(
                    "schema"=>"fbr_acopio",
                    "table"=>"fbr_perfil",
                    "column"=>"idperfil",
                ),
           "idmenu"=>array(
                    "schema"=>"fbr_acopio",
                    "table"=>"fbr_menu",
                    "column"=>"idmenu",
                ),
        );
    }

    public static function describe(){
        return array(
            "idperfil"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idmenu"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
        );
    }
}
