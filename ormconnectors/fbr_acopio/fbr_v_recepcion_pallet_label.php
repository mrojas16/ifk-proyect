<?php 
/* Creado con XMUG v0.1.00 */

namespace XWork\ORMConnectors\fbr_acopio;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\ORM;
use \XWork\Core\ORM\ORMView;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 * 
 * @name        fbr_v_recepcion_pallet_label.php
* @desc        Database Abstraction Layer For ORM 
 * @database    fbr_acopio
 * @table       fbr_v_recepcion_pallet_label
 * @subpackage  XWork\ORMConnectors\fbr_acopio
 *
 */
class fbr_v_recepcion_pallet_label extends ORM implements ORMView {

    public $idpallet;
    public $idpesaje;
    public $idrecepcion;
    public $cant_bandejas_sum;
    public $kilos_netos_sum;
    public $peso_promedio_sum;
    public $idproductor;
    public $productor;

    public static $_primaryKey = "idusuario";

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function foreignKeys(){
        return array(
        );
    }

    public static function describe(){
        return array(
            "idpallet"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idpesaje"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idrecepcion"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "cant_bandejas_sum"=>array(
                    "TYPE"=>"double",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "kilos_netos_sum"=>array(
                    "TYPE"=>"double",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "peso_promedio_sum"=>array(
                    "TYPE"=>"double",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idproductor"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "productor"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>248,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
        );
    }
}
