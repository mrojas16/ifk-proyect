<?php 
/* Creado con XMUG v0.1.00 */

namespace XWork\ORMConnectors\fbr_acopio;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\ORM;
use \XWork\Core\ORM\ORMTable;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 * 
 * @name        fbr_param_especie.php
* @desc        Database Abstraction Layer For ORM 
 * @database    fbr_acopio
 * @table       fbr_param_especie
 * @subpackage  XWork\ORMConnectors\fbr_acopio
 *
 */
class fbr_param_especie extends ORM implements ORMTable {

    public $idparametro;
    public $idespecie;
    public $posicion;

    public static $_primaryKey = "idespecie";

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function foreignKeys(){
        return array(
           "idespecie"=>array(
                    "schema"=>"fbr_acopio",
                    "table"=>"fbr_especie",
                    "column"=>"idespecie",
                ),
           "idparametro"=>array(
                    "schema"=>"fbr_acopio",
                    "table"=>"fbr_parametro",
                    "column"=>"idparametro",
                ),
        );
    }

    public static function describe(){
        return array(
            "idparametro"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idespecie"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "posicion"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
        );
    }
}
