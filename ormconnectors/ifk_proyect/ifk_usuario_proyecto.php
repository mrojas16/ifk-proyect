<?php 
/* Creado con XMUG v0.1.00 */

namespace XWork\ORMConnectors\ifk_proyect;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\ORM;
use \XWork\Core\ORM\ORMTable;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 * 
 * @name        ifk_usuario_proyecto.php
* @desc        Database Abstraction Layer For ORM 
 * @database    ifk_proyect
 * @table       ifk_usuario_proyecto
 * @subpackage  XWork\ORMConnectors\ifk_proyect
 *
 */
class ifk_usuario_proyecto extends ORM implements ORMTable {

    public $idproyecto;
    public $idusuario;

    public static $_primaryKey = "idobservacion";

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function foreignKeys(){
        return array(
           "idproyecto"=>array(
                    "schema"=>"ifk_proyect",
                    "table"=>"ifk_proyecto",
                    "column"=>"idproyecto",
                ),
           "idusuario"=>array(
                    "schema"=>"ifk_proyect",
                    "table"=>"ifk_usuario",
                    "column"=>"idusuario",
                ),
        );
    }

    public static function describe(){
        return array(
            "idproyecto"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idusuario"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
        );
    }
}
