<?php 
/* Creado con XMUG v0.1.00 */

namespace XWork\ORMConnectors\byl_cert;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\ORM;
use \XWork\Core\ORM\ORMTable;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 * 
 * @name        byl_curso_instancia.php
* @desc        Database Abstraction Layer For ORM 
 * @database    byl_cert
 * @table       byl_curso_instancia
 * @subpackage  XWork\ORMConnectors\byl_cert
 *
 */
class byl_curso_instancia extends ORM implements ORMTable {

    public $idcurso_instancia;
    public $idcurso;
    public $idcliente;
    public $idrelator;
    public $fecha_inicio;
    public $fecha_termino;
    public $duracion;
    public $fecha_expiracion;
    public $tipo;
    public $fecha_creacion;
    public $fecha_edicion;
    public $fecha_eliminacion;
    public $idcreador;
    public $idmodificador;
    public $ideliminador;
    public $activo;
    public $tipo_equipo;
    public $marca;
    public $modelo;
    public $anio_equipo;
    public $observaciones;

    public static $_primaryKey = "idcurso_instancia";

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function foreignKeys(){
        return array(
           "idcurso"=>array(
                    "schema"=>"byl_cert",
                    "table"=>"byl_curso",
                    "column"=>"idcurso",
                ),
           "idcliente"=>array(
                    "schema"=>"byl_cert",
                    "table"=>"byl_cliente",
                    "column"=>"idcliente",
                ),
           "idrelator"=>array(
                    "schema"=>"byl_cert",
                    "table"=>"byl_relator",
                    "column"=>"idrelator",
                ),
        );
    }

    public static function describe(){
        return array(
            "idcurso_instancia"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>"auto_increment"
                ),
            "idcurso"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idcliente"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idrelator"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>TRUE,
                    "PK"=>TRUE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_inicio"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_termino"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "duracion"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_expiracion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "tipo"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>1,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_creacion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_edicion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_eliminacion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idcreador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idmodificador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "ideliminador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "activo"=>array(
                    "TYPE"=>"tinyint",
                    "LENGTH"=>1,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "tipo_equipo"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "marca"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "modelo"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "anio_equipo"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "observaciones"=>array(
                    "TYPE"=>"text",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
        );
    }
}
