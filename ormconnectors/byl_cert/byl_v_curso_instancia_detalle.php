<?php 
/* Creado con XMUG v0.1.00 */

namespace XWork\ORMConnectors\byl_cert;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\ORM;
use \XWork\Core\ORM\ORMView;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 * 
 * @name        byl_v_curso_instancia_detalle.php
* @desc        Database Abstraction Layer For ORM 
 * @database    byl_cert
 * @table       byl_v_curso_instancia_detalle
 * @subpackage  XWork\ORMConnectors\byl_cert
 *
 */
class byl_v_curso_instancia_detalle extends ORM implements ORMView {

    public $idcurso_instancia_detalle = "0";
    public $idcurso_instancia;
    public $idcurso;
    public $activo;
    public $nombre_curso;
    public $version_curso;
    public $idcliente;
    public $rut_cliente;
    public $razon_social_cliente;
    public $idrelator;
    public $rut_relator;
    public $nombre_relator;
    public $profesion_relator;
    public $reg_sns;
    public $reg_sernageomin;
    public $fecha_inicio;
    public $fecha_creacion;
    public $fecha_termino;
    public $duracion;
    public $tipo;
    public $tipo_equipo;
    public $marca;
    public $modelo;
    public $anio_equipo;
    public $idtrabajador;
    public $rut_trabajador;
    public $nombre_trabajador;
    public $apellido_paterno_trabajador;
    public $apellido_materno_trabajador;
    public $fecha_nacimiento;
    public $edad;
    public $escolaridad;
    public $cargo;
    public $licencia_conducir;
    public $estado;
    public $nota;
    public $observaciones;

    public static $_primaryKey = "";

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function foreignKeys(){
        return array(
        );
    }

    public static function describe(){
        return array(
            "idcurso_instancia_detalle"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"0",
                    "EXTRA"=>""
                ),
            "idcurso_instancia"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idcurso"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "activo"=>array(
                    "TYPE"=>"tinyint",
                    "LENGTH"=>1,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "nombre_curso"=>array(
                    "TYPE"=>"text",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "version_curso"=>array(
                    "TYPE"=>"float",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idcliente"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "rut_cliente"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "razon_social_cliente"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idrelator"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "rut_relator"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "nombre_relator"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "profesion_relator"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "reg_sns"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "reg_sernageomin"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_inicio"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_creacion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_termino"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "duracion"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "tipo"=>array(
                    "TYPE"=>"tinyint",
                    "LENGTH"=>1,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "tipo_equipo"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "marca"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "modelo"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "anio_equipo"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idtrabajador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "rut_trabajador"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "nombre_trabajador"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "apellido_paterno_trabajador"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "apellido_materno_trabajador"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_nacimiento"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "edad"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "escolaridad"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "cargo"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "licencia_conducir"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "estado"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "nota"=>array(
                    "TYPE"=>"float",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "observaciones"=>array(
                    "TYPE"=>"text",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
        );
    }
}
