<?php 
/* Creado con XMUG v0.1.00 */

namespace XWork\ORMConnectors\byl_cert;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\ORM;
use \XWork\Core\ORM\ORMView;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 * 
 * @name        byl_v_curso_instancia_detalle_trabajador.php
* @desc        Database Abstraction Layer For ORM 
 * @database    byl_cert
 * @table       byl_v_curso_instancia_detalle_trabajador
 * @subpackage  XWork\ORMConnectors\byl_cert
 *
 */
class byl_v_curso_instancia_detalle_trabajador extends ORM implements ORMView {

    public $idcurso_instancia_detalle = "0";
    public $idcurso_instancia;
    public $idtrabajador;
    public $rut;
    public $estado;
    public $nota;
    public $observaciones;

    public static $_primaryKey = "";

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function foreignKeys(){
        return array(
        );
    }

    public static function describe(){
        return array(
            "idcurso_instancia_detalle"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"0",
                    "EXTRA"=>""
                ),
            "idcurso_instancia"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idtrabajador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "rut"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "estado"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "nota"=>array(
                    "TYPE"=>"float",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "observaciones"=>array(
                    "TYPE"=>"text",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
        );
    }
}
