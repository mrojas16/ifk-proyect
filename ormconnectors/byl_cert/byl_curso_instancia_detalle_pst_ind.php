<?php 
/* Creado con XMUG v0.1.00 */

namespace XWork\ORMConnectors\byl_cert;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\ORM;
use \XWork\Core\ORM\ORMTable;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 * 
 * @name        byl_curso_instancia_detalle_pst_ind.php
* @desc        Database Abstraction Layer For ORM 
 * @database    byl_cert
 * @table       byl_curso_instancia_detalle_pst_ind
 * @subpackage  XWork\ORMConnectors\byl_cert
 *
 */
class byl_curso_instancia_detalle_pst_ind extends ORM implements ORMTable {

    public $idcurso_instancia_detalle;
    public $idbat_pst_ind;
    public $resultado = "1";

    public static $_primaryKey = "idbat_pst_ind";

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function foreignKeys(){
        return array(
           "idbat_pst_ind"=>array(
                    "schema"=>"byl_cert",
                    "table"=>"byl_bat_pst_ind",
                    "column"=>"idbat_pst_ind",
                ),
           "idcurso_instancia_detalle"=>array(
                    "schema"=>"byl_cert",
                    "table"=>"byl_curso_instancia_detalle",
                    "column"=>"idcurso_instancia_detalle",
                ),
        );
    }

    public static function describe(){
        return array(
            "idcurso_instancia_detalle"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idbat_pst_ind"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "resultado"=>array(
                    "TYPE"=>"tinyint",
                    "LENGTH"=>1,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"1",
                    "EXTRA"=>""
                ),
        );
    }
}
