<?php 
/* Creado con XMUG v0.1.00 */

namespace XWork\ORMConnectors\byl_cert;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\ORM;
use \XWork\Core\ORM\ORMTable;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 * 
 * @name        byl_relator.php
* @desc        Database Abstraction Layer For ORM 
 * @database    byl_cert
 * @table       byl_relator
 * @subpackage  XWork\ORMConnectors\byl_cert
 *
 */
class byl_relator extends ORM implements ORMTable {

    public $idrelator;
    public $rut;
    public $nombre;
    public $profesion;
    public $reg_sns;
    public $reg_sernageomin;
    public $fono;
    public $mail;
    public $tipo;
    public $fecha_creacion;
    public $fecha_edicion;
    public $fecha_eliminacion;
    public $idcreador;
    public $idmodificador;
    public $ideliminador;
    public $activo;

    public static $_primaryKey = "idrelator";

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function foreignKeys(){
        return array(
        );
    }

    public static function describe(){
        return array(
            "idrelator"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>"auto_increment"
                ),
            "rut"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "nombre"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "profesion"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "reg_sns"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "reg_sernageomin"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fono"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>45,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "mail"=>array(
                    "TYPE"=>"varchar",
                    "LENGTH"=>255,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "tipo"=>array(
                    "TYPE"=>"tinyint",
                    "LENGTH"=>1,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_creacion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_edicion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "fecha_eliminacion"=>array(
                    "TYPE"=>"date",
                    "LENGTH"=>NULL,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idcreador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idmodificador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "ideliminador"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "activo"=>array(
                    "TYPE"=>"tinyint",
                    "LENGTH"=>1,
                    "NULL"=>TRUE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
        );
    }
}
