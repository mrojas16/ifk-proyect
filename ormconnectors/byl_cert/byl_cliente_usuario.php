<?php 
/* Creado con XMUG v0.1.00 */

namespace XWork\ORMConnectors\byl_cert;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\ORM;
use \XWork\Core\ORM\ORMTable;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 * 
 * @name        byl_cliente_usuario.php
* @desc        Database Abstraction Layer For ORM 
 * @database    byl_cert
 * @table       byl_cliente_usuario
 * @subpackage  XWork\ORMConnectors\byl_cert
 *
 */
class byl_cliente_usuario extends ORM implements ORMTable {

    public $idcliente;
    public $idusuario;

    public static $_primaryKey = "idusuario";

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function foreignKeys(){
        return array(
           "idusuario"=>array(
                    "schema"=>"byl_cert",
                    "table"=>"byl_usuario",
                    "column"=>"idusuario",
                ),
           "idcliente"=>array(
                    "schema"=>"byl_cert",
                    "table"=>"byl_cliente",
                    "column"=>"idcliente",
                ),
        );
    }

    public static function describe(){
        return array(
            "idcliente"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
            "idusuario"=>array(
                    "TYPE"=>"int",
                    "LENGTH"=>11,
                    "NULL"=>FALSE,
                    "FK"=>FALSE,
                    "PK"=>FALSE,
                    "DEFAULT"=>"",
                    "EXTRA"=>""
                ),
        );
    }
}
