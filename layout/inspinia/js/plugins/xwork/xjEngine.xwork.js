/**
 * Copyright ©2014-2015 by Marcel Rojas
 */
'use strict';

var xjEngine = function () {
    this.hash = "#index/dashboard";
    this.container = document.getElementById("maincontainerAPP");
    this.base_url = BASE_URL_JS;
    this.menu_container = document.getElementById("side-menu");
    this.main_title = 'IFK Intranet';
};

xjEngine.prototype.init = function () {
    if (window.location.hash) {
        this.hash = window.location.hash;
    } else {
        window.location.hash = this.hash;
    }
    var t = this;
    setTimeout(function () {
        t.run();
    }, 10);

};

xjEngine.prototype.run = function () {
    this.checkURL();
    this.markDashboardByTitle();
};

xjEngine.prototype.markDashboardByTitle = function () {
    var t = App.xDom;
    document.getElementById("no-mark-menu").addEventListener('click', function () {
        window.location.hash = "#index/dashboard";
        t.init();
        return false;
    }, false);
};

xjEngine.prototype.checkURL = function () {
    var url = this.hash.replace(/^#/, '');
    var full_url = this.base_url + this.hash;
    var mt = this.main_title;
    if (url) {
        var lis = this.menu_container.querySelectorAll('li');
        Array.prototype.forEach.call(lis, function (el, i) {
            if (el.classList) {
                el.classList.remove('active');
            } else {
                el.className = el.className.replace(new RegExp('(^|\\b)' + 'active'.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
            }
            var wul = el.querySelector('ul');
            if (wul) {
                if (wul.classList) {
                    wul.classList.remove('in');
                } else {
                    wul.className = wul.className.replace(new RegExp('(^|\\b)' + 'in'.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
                }
            }
            var wu = el.querySelector('a[href="' + full_url + '"]');
            if (wu) {
                if (el.classList) {
                    el.classList.add('active');
                } else {
                    el.className += ' ' + 'active';
                }

                var title = (wu.getAttribute('title'));

                if (typeof title === "undefined") {
                    title = mt;
                } else {
                    title = mt + ' | ' + title;
                }
                document.title = title;
                var par = false;
                var elp = wu;
                do {
                    elp = elp.parentNode;
                    var matches = function (el, selector) {
                        return (el.matches || el.matchesSelector || el.msMatchesSelector || el.mozMatchesSelector || el.webkitMatchesSelector || el.oMatchesSelector).call(el, selector);
                    };
                    if (matches(elp, 'li')) {
                        if (elp.classList) {
                            elp.classList.add('active');
                        } else {
                            elp.className += ' ' + 'active';
                        }
                    }
                    if (matches(elp, 'ul')) {
                        if (elp.classList) {
                            elp.classList.add('in');
                        } else {
                            elp.className += ' ' + 'in';
                        }
                    }
                    if (matches(elp, '#side-menu')) {
                        par = true;
                    }
                } while (!par);
            }
        });


        if (url === "index" || url === "index/" || url === "index/index" || url === "index/index/" || url === "index/index/index" || url === "index/index/index/") {
            window.location.hash = '#index/dashboard';
            this.hash = '#index/dashboard';
        } else {
            this.loadURL(url + location.search, this.container);
        }

    } else {
        window.location.hash = '#index/dashboard';
        this.hash = '#index/dashboard';
        this.run();
    }
};

xjEngine.prototype.getAJAX = function () {
    var obj = null;
    if (window.XMLHttpRequest) {
        obj = new XMLHttpRequest();
    } else {
        try {
            obj = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
            alert('El navegador utilizado no está soportado');
        }
    }
    return obj;
};

xjEngine.prototype.loadURL = function (dir, container) {
    var full_dir = this.base_url + dir;
    var t = this;
    container.innerHTML = '<h1><i class="fa fa-cog fa-spin"></i> Cargando...</h1>';
    t.scrollTo(document.body, 0, 300);

    var request = this.getAJAX();
    request.open('POST', full_dir, true);
    request.setRequestHeader('XJENGINE_XWORK', 'V0.1');

    request.onload = function () {
        if (request.status >= 200 && request.status < 400) {
                var resp = request.responseText;
                container.style.opacity = 0;
                container.style.filter = "alpha(opacity=0)";
                container.innerHTML = resp;
                setTimeout(function () {
                    function fadeIn(el) {
                        el.style.opacity = 0;

                        var last = +new Date();
                        var tick = function () {
                            el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
                            last = +new Date();

                            if (+el.style.opacity < 1) {
                                (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
                            }
                        };

                        tick();
                    }

                    fadeIn(container);
                    if (document.getElementById("postrunnable")) {
                        eval(document.getElementById("postrunnable").innerHTML);
                    }
                }, 50);
        } else {
            var resp = request.responseText;
            container.style.opacity = 0;
            container.style.filter = "alpha(opacity=0)";
            container.innerHTML = resp;
            setTimeout(function () {
                function fadeIn(el) {
                    el.style.opacity = 0;

                    var last = +new Date();
                    var tick = function () {
                        el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
                        last = +new Date();

                        if (+el.style.opacity < 1) {
                            (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
                        }
                    };

                    tick();
                }

                fadeIn(container);
            }, 50);
        }
    };
    request.onerror = function () {
        container.html('<h4 style="margin-top:10px; display:block; text-align:left"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Pagina no encontrada.</h4>');
    };

    request.send();
};

xjEngine.prototype.scrollTo = function (element, to, duration) {
    if (duration < 0)
        return;
    var difference = to - element.scrollTop;
    var perTick = difference / duration * 10;

    setTimeout(function () {
        element.scrollTop = element.scrollTop + perTick;
        if (element.scrollTop === to)
            return;
        scrollTo(element, to, duration - 10);
    }, 10);
};
