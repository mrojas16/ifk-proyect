;
(function ($) {
    $.fn.focusNextInput = function () {
        if ($(this).is('[data-firstfirmeza]')) {
            return this.each(function () {
                $(this).closest('tr').find('td:nth-child(7) input').focus().select();
                return false;
            });
        } else if ($(this).is('[data-secondfirmeza]')) {
            return this.each(function () {
                $(this).closest('tr').next().find('td:nth-child(6) input').focus().select();
                return false;
            });
        } else {
            return this.each(function () {
                var fields = $(this).parents('form:eq(0),body').find('button,input,textarea,select');
                var index = fields.index(this);
                if (index > -1 && (index + 1) < fields.length) {
                    fields.eq(index + 1).focus().select();
                }
                return false;
            });
        }
    };

    $.fn.xcaseconvert = function (options) {

        var settings = $.extend({
            type: 'upper'
        }, options);

        this.each(function () {
            var $this = $(this);
            function getCaretPosition(ctrl) {
                var CaretPos = 0;    // IE Support
                if (document.selection) {
                    ctrl.focus();
                    var Sel = document.selection.createRange();
                    Sel.moveStart('character', -ctrl.value.length);
                    CaretPos = Sel.text.length;
                }
                // Firefox support
                else if (ctrl.selectionStart || ctrl.selectionStart == '0') {
                    CaretPos = ctrl.selectionStart;
                }

                return CaretPos;
            }

            function setCaretPosition(ctrl, pos) {
                if (ctrl.setSelectionRange) {
                    ctrl.focus();
                    ctrl.setSelectionRange(pos, pos);
                }
                else if (ctrl.createTextRange) {
                    var range = ctrl.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', pos);
                    range.moveStart('character', pos);
                    range.select();
                }
            }

            $this.keydown(function (e) {
                if (e.which != 9) {
                    var $t = this;
                    setTimeout(function () {
                        var caretPosition = getCaretPosition($t);
                        if (settings.type == 'upper') {
                            $t.value = $t.value.toLocaleUpperCase();
                        } else if (settings.type == 'lower') {
                            $t.value = $t.value.toLocaleLowerCase();
                        }
                        setCaretPosition($t, caretPosition);
                    });
                }
            });
        });

        return this;

    };

    var _oldShow = $.fn.show;
    var _oldHide = $.fn.hide;

    $.fn.show = function (speed, oldCallback) {
        return $(this).each(function () {
            var obj = $(this),
                    newCallback = function () {
                        if ($.isFunction(oldCallback)) {
                            oldCallback.apply(obj);
                        }
                        obj.trigger('afterShow');
                    };

            obj.trigger('beforeShow');
            _oldShow.apply(obj, [speed, newCallback]);
        });
    };

    $.fn.hide = function (speed, oldCallback) {
        return $(this).each(function () {
            var obj = $(this),
                    newCallback = function () {
                        if ($.isFunction(oldCallback)) {
                            oldCallback.apply(obj);
                        }
                        obj.trigger('afterHide');
                    };

            obj.trigger('beforeHide');
            _oldHide.apply(obj, [speed, newCallback]);
        });
    };

}(jQuery));

Number.prototype.round = function (places) {
    var multiplier = Math.pow(10, places);
    return (Math.round(this * multiplier) / multiplier);
};


$('form').on('reset', function (e) {
    var $this = $(this);
    setTimeout(function () {
        $this.find('select').each(function () {
            $(this).val(null).trigger('chosen:updated');
        });
    }, 1);
});