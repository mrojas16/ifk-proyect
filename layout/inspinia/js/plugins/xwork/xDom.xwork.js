/**
 * Copyright ©2014-2015 by Marcel Rojas
 */
'use strict';

/**
 * XDOM form XWork with Inspinia
 * @returns {xDom}
 */
var xDom = function () {
    this.daemonInterval = 1;
    this.Xconf = function (conf) {
        for (var i in conf) {
            this[i] = conf[i];
        }
    };
};

xDom.prototype.init = function (xconfig) {
    this.Xconf(xconfig);
    var config = {
        '.chosen-select': {'min-width': '100px', 'white-space': 'nowrap', no_results_text: 'No Existen Resultados!'},
        '.chosen-select-deselect': {allow_single_deselect: true, no_results_text: 'No Existen Resultados!'},
        '.chosen-select-no-single': {disable_search_threshold: 10, no_results_text: 'No Existen Resultados!'},
        '.chosen-select-no-results': {no_results_text: 'No Existen Resultados!'},
        '.chosen-select-width': {width: "95%", no_results_text: 'No Existen Resultados!'}
    };
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    setTimeout(function () {
        $(".chosen-container").attr('style', 'width:100%');
    }, 100);


    $("[data-addnew]").bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        App.xDom.openForm();
    });

    $("[data-cancel_form]").bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        App.xDom.closeForm();
    });

    $("[data-rut]").Rut({
        format_on: 'keyup'
    });
    this.validationExtras();
};

xDom.prototype.refreshForm = function (form, callback) {
    form = (form) ? form : '#primal-form';
    if ($(form)[0]) {
        $(form)[0].reset();
    }
    $(form + ' .i-checks').iCheck('uncheck');
    $(form + ' select').val(null).trigger('chosen:updated');
    if (App.xDom.onRefreshFormCallback && typeof App.xDom.onRefreshFormCallback === "function") {
        App.xDom.onRefreshFormCallback();
        if (callback && typeof callback === "function") {
            callback();
        }
    }
    if (form === "#primal-form") {
        $(".pk_form").val(0);
    }
};

xDom.prototype.closeForm = function (callback) {
    $('[data-maincontainerform]').slideUp(function () {
        App.xDom.refreshForm();
        if (App.xDom.onCloseFormCallback && typeof App.xDom.onCloseFormCallback === "function") {
            App.xDom.onCloseFormCallback();
        }
        if (typeof callback === "function") {
            callback();
        }
    });
};

xDom.prototype.openForm = function () {
    if ($("#primal-form")) {
        App.xDom.closeForm();
        setTimeout(function () {
            App.xDom.refreshForm();
            $('.only_on_edit').hide();
            $('.form-title').html((typeof App.xDom.onOpenFormTitleAdd !== "undefined") ? App.xDom.onOpenFormTitleAdd : "Agregar");
            setTimeout(function () {
                $('[data-maincontainerform]').slideDown(function () {
                    if (App.xDom.onOpenFormCallback && typeof App.xDom.onOpenFormCallback === "function") {
                        App.xDom.onOpenFormCallback();
                    }
                });
            }, 10);
        }, 400);
    } else {
        App.xDom.refreshForm();
        $('.only_on_edit').hide();
        $('.form-title').html((typeof App.xDom.onOpenFormTitleAdd !== "undefined") ? App.xDom.onOpenFormTitleAdd : "Agregar");
        setTimeout(function () {
            $('[data-maincontainerform]').slideDown(function () {
                if (App.xDom.onOpenFormCallback && typeof App.xDom.onOpenFormCallback === "function") {
                    App.xDom.onOpenFormCallback();
                }
            });
        }, 10);
    }
};

xDom.prototype.strPad = function (input, pad_length, pad_string, pad_type) {
    var half = '', pad_to_go;

    var str_pad_repeater = function (s, len) {
        var collect = '',
                i;

        while (collect.length < len) {
            collect += s;
        }
        collect = collect.substr(0, len);

        return collect;
    };

    input += '';
    pad_string = pad_string !== undefined ? pad_string : ' ';

    if (pad_type !== 'STR_PAD_LEFT' && pad_type !== 'STR_PAD_RIGHT' && pad_type !== 'STR_PAD_BOTH') {
        pad_type = 'STR_PAD_RIGHT';
    }
    if ((pad_to_go = pad_length - input.length) > 0) {
        if (pad_type === 'STR_PAD_LEFT') {
            input = str_pad_repeater(pad_string, pad_to_go) + input;
        } else if (pad_type === 'STR_PAD_RIGHT') {
            input = input + str_pad_repeater(pad_string, pad_to_go);
        } else if (pad_type === 'STR_PAD_BOTH') {
            half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
            input = half + input + half;
            input = input.substr(0, pad_length);
        }
    }

    return input;
};

xDom.prototype.switchs = function () {
    $('.dropdown-acopios li.active a').bind('click', function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    $('.dropdown-acopios li:not(.active) a').bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var h = $(this).attr('data-v');
        xmodal.show(null, "¿Desea cambiar este acopio?", function () {
            $.ajax({
                type: "POST",
                url: BASE_URL_JS + "index/verify/topswitch",
                data: "id=" + h + "&_t=acopio",
                beforeSend: function (request) {
                    request.setRequestHeader("XJENGINE_XWORK", 'V0.1');
                    request.setRequestHeader("XDOM_METHOD", 'JQUERY');
                }
            }).done(function (data) {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        $.gritter.add({
                            title: "OK!",
                            text: "<i class='fa fa-clock-o'></i> <i>Se ha cambiado correctamente el acopio</i>",
                            class_name: 'growl-success',
                            sticky: false,
                            time: 2000
                        });
                        var $target = $('.dropdown-acopios a[data-v="' + h + '"]>div');
                        $('.acopio-actual').html($target.text());
                        $('.dropdown-acopios li').removeClass('active');
                        $target.closest('li').addClass('active');
                        App.xDom.switchs();
                    } else {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar el acopio</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    }
                    xmodal.hide();
                } catch (e) {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar el acopio</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                    xmodal.hide();
                }
            }).error(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar el acopio</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
                xmodal.hide();
            });
        });
    });
    
    $('.dropdown-temporadas li.active a').bind('click', function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    $('.dropdown-temporadas li:not(.active) a').bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var h = $(this).attr('data-v');
        xmodal.show(null, "¿Desea cambiar esta Temporada?", function () {
            $.ajax({
                type: "POST",
                url: BASE_URL_JS + "index/verify/topswitch",
                data: "id=" + h + "&_t=acopio",
                beforeSend: function (request) {
                    request.setRequestHeader("XJENGINE_XWORK", 'V0.1');
                    request.setRequestHeader("XDOM_METHOD", 'JQUERY');
                }
            }).done(function (data) {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        $.gritter.add({
                            title: "OK!",
                            text: "<i class='fa fa-clock-o'></i> <i>Se ha cambiado correctamente el acopio</i>",
                            class_name: 'growl-success',
                            sticky: false,
                            time: 2000
                        });
                        var $target = $('.dropdown-temporadas a[data-v="' + h + '"]>div');
                        $('.temporada-actual').html($target.text());
                        $('.dropdown-temporadas li').removeClass('active');
                        $target.closest('li').addClass('.active');
                        App.xDom.switchs();
                    } else {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar el acopio</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    }
                    xmodal.hide();
                } catch (e) {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar el acopio</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                    xmodal.hide();
                }
            }).error(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar el acopio</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
                xmodal.hide();
            });
        });
    });
};

xDom.prototype.daemon = function () {
    setInterval(function () {
        var dir = BASE_URL_JS + "index/verify/session";
        var hash = window.location.hash;
        $.ajax({
            type: "POST",
            url: dir,
            beforeSend: function (request) {
                request.setRequestHeader("XJENGINE_XWORK", 'V0.1');
                request.setRequestHeader("XDOM_METHOD", 'JQUERY');
            }
        }).done(function (data) {
            try {
                var r = JSON.parse(data);
                if (r.response !== 1) {
                    window.location = BASE_URL_JS + "index/lock" + hash;
                }
            } catch (e) {
                window.location = BASE_URL_JS + "index/lock" + hash;
            }
        });
    }, this.daemonInterval * 1000 * 60);
};

xDom.prototype.addEvent = function (el, eventType, handler) {
    if (el.addEventListener) {
        el.addEventListener(eventType, handler, false);
    } else if (el.attachEvent) {
        el.attachEvent('on' + eventType, handler);
    } else {
        el['on' + eventType] = handler;
    }
};

xDom.prototype.contentLoaded = function (win, fn) {
    var done = false, top = true, doc = win.document, root = doc.documentElement, modern = doc.addEventListener,
            add = modern ? 'addEventListener' : 'attachEvent',
            rem = modern ? 'removeEventListener' : 'detachEvent',
            pre = modern ? '' : 'on',
            init = function (e) {
                if (e.type == 'readystatechange' && doc.readyState != 'complete')
                    return;
                (e.type == 'load' ? win : doc)[rem](pre + e.type, init, false);
                if (!done && (done = true))
                    fn.call(win, e.type || e);
            },
            poll = function () {
                try {
                    root.doScroll('left');
                } catch (e) {
                    setTimeout(poll, 50);
                    return;
                }
                init('poll');
            };

    if (doc.readyState == 'complete') {
        fn.call(win, 'lazy');
    }
    else {
        if (!modern && root.doScroll) {
            try {
                top = !win.frameElement;
            } catch (e) {
            }
            if (top)
                poll();
        }
        doc[add](pre + 'DOMContentLoaded', init, false);
        doc[add](pre + 'readystatechange', init, false);
        win[add](pre + 'load', init, false);
    }
};

xDom.prototype.validationExtras = function () {
    $.validator.setDefaults({ignore: []});
    $.validator.addMethod("rut", function (value, element) {
        return this.optional(element) || $.Rut.validar(value);
    }, "Este campo debe ser un rut valido.");

    $.validator.addMethod("maxStock", function (value, element) {
        var pk = $(element).closest('form').find('[name="idinsumo"]').val();
        var lote = $(element).closest('form').find('[name="lote"]').val();
        var bod = $('[name=idbodega]').val();
        App.RevisionErrorsDespacho = {
            pk: 0,
            bod: 0,
            lote: 0
        };
        if (pk === "undefined" || !pk || pk === null || pk + "" === "") {
            App.RevisionErrorsDespacho.pk = 1;
            return false;
        } else {
            App.RevisionErrorsDespacho.pk = 0;
        }
        if (bod === "undefined" || !bod || bod === null || bod + "" === "") {
            App.RevisionErrorsDespacho.bod = 1;
            return false;
        } else {
            App.RevisionErrorsDespacho.bod = 0;
        }
        if (lote === "undefined" || !lote || lote === null || lote + "" === "") {
            App.RevisionErrorsDespacho.lote = 1;
            return false;
        } else {
            App.RevisionErrorsDespacho.lote = 0;
        }

        var dir = BASE_URL_JS + "index/verify/max_stock";
        var data = "_v=true&_k=" + pk + "&_b=" + bod + "&_l=" + lote;
        App.tableDetalleIDInsumo = pk;
        var transaction = new App.sJax('POST', dir, data, false);
        var response = transaction.getReturn();
        try {
            var r = JSON.parse(response);
            var cant = r.data.cant;
            App.cantMaxStockRevisionReturn = cant;
            var cantFinal = parseInt(cant) - parseInt((typeof App.xDom.tableDetalleCounterCant[pk] !== "undefined") ? App.xDom.tableDetalleCounterCant[pk] : 0);
            return (parseInt(value) <= cantFinal);
        } catch (e) {
            return true;
        }

    }, function () {
        if (App.RevisionErrorsDespacho.pk === 1) {
            return "Debe seleccionar un insumo";
        }
        if (App.RevisionErrorsDespacho.bod === 1) {
            return "Debe seleccionar una bodega";
        }
        if (App.RevisionErrorsDespacho.lote === 1) {
            return "Debe escribir un lote válido";
        }
        var cantFinal = parseInt(App.cantMaxStockRevisionReturn) - parseInt((typeof App.xDom.tableDetalleCounterCant[App.tableDetalleIDInsumo] !== "undefined") ? App.xDom.tableDetalleCounterCant[App.tableDetalleIDInsumo] : 0);
        if (cantFinal === 0) {
            return "El lote ingresado no existe o no posee Stock";
        }
        return "La cantidad no puede exceder a " + cantFinal;
    });

    $.validator.addMethod("noRepeat", function (value, element) {
        var v = value;
        var pk = $(element).closest('form').find('.pk_form').val();
        var params = $(element).attr('data-verificable');
        if (v === "undefined" || params === "undefined" || pk === "undefined" || !v || !pk || !params || v === null || pk === null || params === null) {
            return true;
        } else {
            var dir = BASE_URL_JS + "index/verify/no_repeat";
            var data = "_v=" + v + "&_k=" + pk + "&_p=" + params;
            var transaction = new App.sJax('POST', dir, data, false);
            var response = transaction.getReturn();
            try {
                var r = JSON.parse(response);
                return r.response === 1;
            } catch (e) {
                return true;
            }
        }
    }, "Este valor ya está en la base de datos.");

    $.validator.addMethod("lote", function (value, element) {
        var v = value;
        var bod = $('[name=idbodega]').val();
        App.RevisionErrorsLote = {
            value: 0,
            bod: 0
        };
        if (v === "undefined" || !v || v === null) {
            App.RevisionErrorsLote.value = 1;
            return false;
        } else {
            App.RevisionErrorsLote.value = 0;
        }
        if (bod === "undefined" || !bod || bod === null) {
            App.RevisionErrorsLote.bod = 1;
            return false;
        } else {
            App.RevisionErrorsLote.bod = 0;
        }

        var dir = BASE_URL_JS + "index/verify/lote";
        var data = "_v=" + v + "&_b=" + bod;
        var transaction = new App.sJax('POST', dir, data, false);
        var response = transaction.getReturn();
        try {
            var r = JSON.parse(response);
            return r.response === 1;
        } catch (e) {
            return true;
        }
    }, function () {
        if (App.RevisionErrorsDespacho.v === 1) {
            return "Debe ingresar un lote";
        }
        if (App.RevisionErrorsDespacho.bod === 1) {
            return "Debe seleccionar una bodega";
        }
        return "El lote ingresado NO existe ";
    });
};

xDom.prototype.dataTable = function (url) {
    App.oTable = $('[data-tablemain]').dataTable({
        responsive: true,
        "oLanguage": {
            "sUrl": BASE_URL_JS + "layout/inspinia/js/datatables-es.json"
        },
        "bProcessing": true,
        "bServerSide": true,
        "bRetrieve": true,
        "sAjaxSource": url,
        "fnInitComplete": function (oSettings, json) {
            $(".dataTables_filter").addClass('pull-right');
            $("#primal-table_paginate").addClass('pull-right');
        }
    });
};

xDom.prototype.editStandard = function (config) {
    App.xDom.closeForm(function () {
        blackout.show('Espere...', function () {
            $.ajax({
                type: "POST",
                url: config.url,
                data: "id=" + config.id
            }).done(function (data) {
                try {
                    var arr_data = JSON.parse(data);
                    var all = arr_data.data.data;
                    for (var i in all) {
                        $("[name=" + i + "]").val(all[i]).trigger("chosen:updated");
                    }
                    $('.form-title').html((typeof App.xDom.onOpenFormTitleEdit !== "undefined") ? App.xDom.onOpenFormTitleEdit : "Agregar");
                    $(".pk_form").val(config.id);
                    blackout.hide(function () {
                        $("[data-maincontainerform]").slideDown();
                        if (typeof config.callback === "function") {
                            config.callback(arr_data);
                        }
                    });
                } catch (e) {
                    console.log(e);
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                        if (typeof config.fallback === "function") {
                            config.fallback();
                        }
                    });
                }
            }).error(function () {
                blackout.hide(function () {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 4000
                    });
                    if (typeof config.fallback === "function") {
                        config.fallback();
                    }
                });
            });
        });
    });
};

xDom.prototype.deleteStandard = function (config) {
    App.xDom.closeForm(function () {
        xmodal.show(null, App.xDom.prompDeleteMessage, function () {
            $.ajax({
                type: "POST",
                url: config.url,
                data: "id=" + config.id
            }).done(function (data) {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        App.oTable.fnDraw();
                        $.gritter.add({
                            title: "OK!",
                            text: "<i class='fa fa-clock-o'></i> <i>" + App.xDom.deleteMessageSuccess + "</i>",
                            class_name: 'growl-success',
                            sticky: false,
                            time: 2000
                        });
                        if (typeof config.callback === "function") {
                            config.callback();
                        }
                    } else {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>" + App.xDom.deleteMessageFail + "</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                        if (typeof config.fallback === "function") {
                            config.fallback();
                        }
                    }
                    xmodal.hide();
                } catch (e) {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>" + App.xDom.deleteMessageFail + "</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                    if (typeof config.fallback === "function") {
                        config.fallback();
                    }
                    xmodal.hide();
                }
            }).error(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>" + App.xDom.deleteMessageFail + "</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
                if (typeof config.fallback === "function") {
                    config.fallback();
                }
                xmodal.hide();
            });
        });
    });
};

xDom.prototype.bloquear = function () {
    var h = window.location.hash;
    window.location = BASE_URL_JS + 'index/lock/' + h;
};

xDom.prototype.logout = function () {
    var h = window.location.hash.replace(/^#/, '');
    window.location = BASE_URL_JS + 'index/login/logout/' + h;
};
/**
 * No asynchronous AJAX
 * @returns {SJAX}
 */
var SJAX = function (method, dir, params, async) {
    if (typeof async === "undefined") {
        async = false;
    }
    var oXML = this.Obj();
    oXML.open(method, dir, async);
    if (method == "POST") {
        oXML.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    }
    oXML.setRequestHeader('XJENGINE_XWORK', 'V0.1');
    oXML.setRequestHeader('XDOM_METHOD', 'SJAX');
    oXML.send(params);
    this.response = oXML.responseText;
};

SJAX.prototype.Obj = function () {
    var obj;
    if (window.XMLHttpRequest) {
        obj = new XMLHttpRequest();
    } else {
        try {
            obj = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
            alert('El navegador utilizado no está soportado');
        }
    }
    return obj;
};

SJAX.prototype.getReturn = function () {
    return this.response;
};

var xLoader = function () {

};

xLoader.prototype.loadCSS = function (filename, callback) {
    if (this.cssExists(filename))
        return;
    var fileref = document.createElement("link"), loaded;
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", filename);
    if (callback) {
        fileref.onreadystatechange = fileref.onload = function () {
            if (!loaded) {
                if (typeof callback === "function")
                    callback();
            }
            loaded = true;
        };
    }
    document.getElementsByTagName("head")[0].appendChild(fileref);
    return this;
};

xLoader.prototype.loadJS = function (filename, callback) {
    if (this.jsExists(filename))
        return;
    var fileref = document.createElement("script"), loaded;
    fileref.setAttribute("type", "text/javascript");
    fileref.setAttribute("src", filename);
    if (callback) {
        fileref.onreadystatechange = fileref.onload = function () {
            if (!loaded) {
                if (typeof callback === "function")
                    callback();
            }
            loaded = true;
        };
    }
    document.getElementsByTagName("head")[0].appendChild(fileref);

    return this;
};

xLoader.prototype.load = function (filename, callback) {
    var parts = filename.match(/.*\.(.*)/);
    if (parts.length >= 2) {
        var ext = parts[1];
        switch (ext) {
            case 'css':
                return this.loadCss(filename, callback);
            case 'js':
                return this.loadJS(filename, callback);
            default:
                console.warn('No se ha podido determinar el tipo para ' + filename + '.\nUtilice una carga específica.');
        }
    }
};

xLoader.prototype.cssExists = function (filename) {
    var css = document.getElementsByTagName("link");
    for (var i = 0; i < css.length; i++)
        if (filename === css[i].attributes.href.nodeValue)
            return true;
    return false;
};

xLoader.prototype.unloadCss = function (regex, callback) {
    var css = document.getElementsByTagName("link");
    var parent = document.getElementsByTagName("head")[0];
    for (var i = 0; i < css.length; i++) {
        if (css[i].attributes.href.nodeValue.match(regex)) {
            if (callback) {
                if (typeof callback === "function")
                    callback(regex, css[i].attributes.href.nodeValue);
            }
            parent.removeChild(css[i]);
        }
    }
};

xLoader.prototype.jsExists = function (filename) {
    var js = document.getElementsByTagName("script");
    for (var i = 0; i < js.length; i++)
        if (filename === js[i].src)
            return true;
    return false;
};


var App = {};
App.sJax = SJAX;
App.ONSESSION = true;
App.xDom = new xDom();
App.oTable = null;
App.xDom.daemon();
App.xDom.switchs();
App.loader = new xLoader();
