/**
 * jQuery littleTree
 *
 * @version  0.1
 * @author   Mikahil Matyunin <free.all.bums@gmail.com>
 */

/**
 * <ul id="tree">
 *   <li><label><input type="checkbox" />Item1</label></li>
 *   <li>
 *     <label><input type="checkbox" />ItemWithSubitems</label>
 *     <ul>
 *       <li><label><input type="checkbox" />Subitem1</label></li>
 *     </ul>
 *   </li>
 * </ul>
 *
 * Usage:
 *
 * $('ul#tree').checktree();
 *
 */

(function($){
    $.fn.extend({

        checktree: function(){
            $(this)
                .addClass('checktree-root')
                .on('change', 'input[type="checkbox"]', function(e){
                    e.stopPropagation();
                    e.preventDefault();

                    checkParents($(this));
                    checkChildren($(this));
                })
            ;

            var checkParents = function (c)
            {
                if(c.is(':checked')){
                    c.parent().addClass('checked');
                } else {
                    c.parent().removeClass('checked');
                }
                
                var parentLi = c.parents('ul:eq(0)').parents('li:eq(0)');

                if (parentLi.length)
                {
                    var siblingsChecked = parseInt($('input[type="checkbox"]:checked', c.parents('ul:eq(0)')).length),
                        rootCheckbox = parentLi.find('input[type="checkbox"]:eq(0)')
                    ;

                    if (c.is(':checked')){
                        rootCheckbox.prop('checked', true);
                        rootCheckbox.parent().addClass('checked');
                    } else if (siblingsChecked === 0) {
                        rootCheckbox.prop('checked', false);
                        rootCheckbox.parent().removeClass('checked');
                    }

                    checkParents(rootCheckbox);
                }
            };

            var checkChildren = function (c)
            {
                var childLi = $('ul li input[type="checkbox"]', c.parents('li:eq(0)'));

                if (childLi.length){
                    if(c.is(':checked')){
                        childLi.prop('checked', true);
                        childLi.parent().addClass('checked');
                    } else {
                        childLi.prop('checked', false);
                        childLi.parent().removeClass('checked');
                    }
                }
            };
        }

    });
})(jQuery);
/////**
// * jQuery littleTree
// *
// * @version  0.1
// * @author   Mikahil Matyunin <free.all.bums@gmail.com>
// */
//
///**
// * <ul id="tree">
// *   <li><label><input type="checkbox" />Item1</label></li>
// *   <li>
// *     <label><input type="checkbox" />ItemWithSubitems</label>
// *     <ul>
// *       <li><label><input type="checkbox" />Subitem1</label></li>
// *     </ul>
// *   </li>
// * </ul>
// *
// * Usage:
// *
// * $('ul#tree').checktree();
// *'input[type="checkbox"]', 
// */
//var $TT;
//
//(function($) {
//    $.fn.extend({
//        checktree: function() {
//            $(this).addClass('checktree-root').bind('ifChanged', 'input[type="checkbox"]',  function(e) {
//                e.stopPropagation();
//                e.preventDefault();
//                alert('a');
//                checkParents($(this));
//                checkChildren($(this));
//            });
//
//            var checkParents = function(c) {
//                var parentLi = c.parents('ul:eq(0)').parents('li:eq(0)');
//                if (parentLi.length)
//                {
//                    var siblingsChecked = parseInt($('input[type="checkbox"]:checked', c.parents('ul:eq(0)')).length),
//                            rootCheckbox = parentLi.find('input[type="checkbox"]:eq(0)')
//                            ;
//
////                parentLi.each(function(e,i){
////                    console.log(i);
////                });
//                    if (c.is(':checked'))
//                        rootCheckbox.iCheck('check');//.prop('checked', true)
//                    else if (siblingsChecked === 0)
//                        rootCheckbox.iCheck('uncheck');//.prop('checked', false)
//                    ;
//
//                    checkParents(rootCheckbox);
//                }
//            };
//
//            var checkChildren = function(c) {
//                var childLi = $('ul li input[type="checkbox"]', c.parents('li:eq(0)'));
//                if (childLi.length) {
//                    var ic = 'unckeck';
//                    if (c.is(':checked')) {
//                        ic = 'check';
//                    }
//                    childLi.iCheck(ic);//.prop('checked', c.is(':checked'))
//                }
//            };
////            var $this = $(this);
////            var $checks = $this.find('input[type="checkbox"]');
////            $checks.each(function(i,e){
////                var $obj = $(e);
////                $obj.bind('ifChanged',function(e){
////                    e.preventDefault();
////                    if($(this).is(':checked')){
////                        checkAllParents($(this));
////                        checkAllChilds($(this));
////                    } else {
////                        checkAllParents($(this));
////                        checkAllChilds($(this));
////                    }
//////                    checkParents($(this));
//////                    checkChilds($(this));
////                });
////            });
////            
////            var checkAllChilds = function($obj){
////                var $childs = $obj
////                
////            };
////            
//////            
//////            var checkParents = function($obj){
//////                var $parents = $obj.parents('ul:eq(0)').parents('li:eq(0)');
//////                if($obj.is(':checked')){
//////                    $parents.parents('ul:eq(0)').parents('li:eq(0)').find('input[type=checkbox]').first().iCheck('check');
//////                } else {
//////                    $obj.parents('ul:eq(0)').find('input[type=checkbox]').each(function(i,e){
//////                        if($(e).is(':checked')){
//////                            return;
//////                        }
//////                    });
//////                    $parents.parents('ul:eq(0)').parents('li:eq(0)').find('input[type=checkbox]').first().iCheck('uncheck');
//////                }
//////            };
//////            
//////            var checkChilds = function($obj){
//////                var ic;
//////                if($obj.is(':checked')){
//////                    ic = 'check';
//////                } else {
//////                    ic = 'unckeck';
//////                }
//////                var $childs = $obj.parents('li:eq(0)').find('ul li input[type=checkbox]');
//////                $childs.each(function(i,e){
//////                    $(e).iCheck(ic);
//////                });
//////            };
//        }
//
//    });
//})(jQuery);
