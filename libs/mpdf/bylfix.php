<?php

require_once LIBS . 'mpdf' . DS . 'mpdf.php';

class PDF_Rotate extends mPDF {
    
    public function __construct($mode='',$format='A4',$default_font_size=0,$default_font='',$mgl=15,$mgr=15,$mgt=16,$mgb=16,$mgh=9,$mgf=9, $orientation='P') {
        parent::mPDF($mode, $format, $default_font_size, $default_font, $mgl, $mgr, $mgt, $mgb, $mgh, $mgf, $orientation);
    }

    var $angle = 0;

    function Rotate($angle, $x = -1, $y = -1) {
        if ($x == -1)
            $x = $this->x;
        if ($y == -1)
            $y = $this->y;
        if ($this->angle != 0)
            $this->_out('Q');
        $this->angle = $angle;
        if ($angle != 0) {
            $angle*=M_PI / 180;
            $c = cos($angle);
            $s = sin($angle);
            $cx = $x * $this->k;
            $cy = ($this->h - $y) * $this->k;
            $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
        }
    }

    function _endpage() {
        if ($this->angle != 0) {
            $this->angle = 0;
            $this->_out('Q');
        }
        parent::_endpage();
    }

    function RotatedText($x, $y, $txt, $angle, $orientation = 'L') {
        //Text rotated around its origin
        $this->Rotate($angle, $x, $y);
        $this->Text($x, $y, $txt);
        $this->Rotate(0);
    }

    function RotatedImage($file, $x, $y, $w, $h, $angle) {
        //Image rotated around its upper-left corner
        $this->Rotate($angle, $x, $y);
        $this->Image($file, $x, $y, $w, $h);
        $this->Rotate(0);
    }

}

class Cabeceras extends PDF_Rotate{
    
    public $code = 'NONE';
    public $typeDoc;

    public function __construct($type = false,$mode='',$format='A4',$default_font_size=0,$default_font='',$mgl=15,$mgr=15,$mgt=16,$mgb=16,$mgh=9,$mgf=9, $orientation='P') {
        $this->typeDoc = $type;
        parent::__construct($mode, $format, $default_font_size, $default_font, $mgl, $mgr, $mgt, $mgb, $mgh, $mgf, $orientation);
    }
    
    public function Header($Harray = array(), $side = '', $write = false) {
        $url_image = BASE_URL . 'warehouse/public/img/byl/azul_membrete.jpg';
        $code = $this->code;
        $header = <<<HEADER
        <div style="padding-top:20px; padding-left:10px;width:40%">
            <img alt="image" width="184" height="98" src="$url_image">
        </div> 
HEADER;
        $this->WriteHTML($header);
//        $this->Code128(10, 10, $code, 100, 15);
        $this->WriteBarcode($code, 1, 135, 10, 1, 0, 1, 1, 2, 2, 1, false, false, 'C128A');
        $this->SetXY(153,19);
        $this->Cell(30, 5, $code, 0, 0, 'C', 0, '');
        $this->SetXY(153,7);
        $this->Cell(30, 5, 'Código Verificación:', 0, 0, 'C', 0, '');
        $this->SetXY(10,70);
        //$disc = '<div style="margin-left:90px;"><textcircle r="65mm" top-text="DOCUMENTO NO VÁLIDO" bottom-text="DOCUMENTO NO VÁLIDO" divider="&bull;" style="font-size: auto; color:#bbbbbb" /><div>';
        if ($this->typeDoc) {
            $disc = '<div style="margin-left:10px;"><img src="' . BASE_URL . 'layout/inspinia/img/doc_no_valido.jpg" width="700" /><div>';
        }
        $this->WriteHTML($disc);
        $this->SetXY(10,45);
        
//        $this->WriteBarcode($code, $showtext, $x, $y, $size, $border, $paddingL, $paddingR, $paddingT, $paddingB, $height, $bgcol, $col, $btype, $supplement, $supplement_code, $k);
        $this->watermarkTextAlpha = 0.1;
        $this->watermarkImageAlpha = 0.5;
        $this->SetWatermarkText('DRAFT');
        $this->showWatermarkText = true;
    }
    
    public function Footer() {
        $url_image = BASE_URL . 'warehouse/public/img/byl/bottom.jpg';
        $header = <<<HEADER
        <div style="padding:0px !important; 
                    position:absolute;
                    width:820px;
                    height:98px!important;
                    padding:0px !important; 
                    width:720px; 
                    position:absolute;
                    bottom:30px;
                    left:30px;">
            <img alt="image" data-width="720" data-height="98" src="$url_image" style="">
        </div>
HEADER;
        $this->WriteHTML($header);
        
//        $this->StartTransform();
//$this->Rotate(-90);
////$this->Cell(0,0,'This is a sample data',1,1,'L',0,'');
//        $this->Cell(0, 0, 'MIERDA!!!', 1, 0, 'C', 0, '', 0, 0, 0);
//$this->StopTransform();
    }
}



class bylfix extends Cabeceras{
    public function __construct($type = false, $mode = '', $format = 'A4', $default_font_size = 0, $default_font = '', $mgl = 15, $mgr = 15, $mgt = 16, $mgb = 16, $mgh = 9, $mgf = 9, $orientation = 'P') {
        parent::__construct($type, $mode, $format, $default_font_size, $default_font, $mgl, $mgr, $mgt, $mgb, $mgh, $mgf, $orientation);
    }
}