<?php

namespace XWork\Core\Exceptions;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        BootstrapException.php
 * @desc        Sistema de excepciones de Arranque
 * @subpackage  XWork\Core\Exceptions
 *
 */
class BootstrapException extends \Exception {

    public function __construct($message, $code = NULL, $previous = NULL) {
        parent::__construct($message, $code, $previous);
    }

    /**
     * String JSON con el resumen de la excepcion
     * @return String
     */
    public function __toString() {
        return '{"RESPONSE":0,"DATA":{"MESSAGE":"EXCEPTION RAISED","EXCEPTION_INFO":{"TYPE":"BOOTSTRAP","CODE":"' . $this->getCode() . '","MESSAGE":"' . htmlentities($this->getMessage()) . '"}}}';
    }

    public static function getStaticException($exception) {
        $exception->getException();
    }
}
