<?php

namespace XWork\Core;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Autoload.php
 * @desc        Sistema de carga de archivos automático
 * @subpackage  XWork\Core
 *
 */
class Autoload {

    public static $loader;

    /**
     * Inicializa el autocargador
     * @return self
     */
    public static function init() {
        if (self::$loader == NULL) {
            self::$loader = new self();
        }
        return self::$loader;
    }

    private function __construct() {
        spl_autoload_register(array($this, 'run'));
    }

    /**
     * Arranca el autocargador
     * @param String $className nombre de la clase a cargar
     * @throws \Exception
     */
    public function run($className) {
        $c = str_replace('XWork' . DS, '', str_replace('\\', DS, $className));
        $a = explode(DS, $c);
        $b = array_pop($a);
        $class = strtolower(implode(DS, $a)) . DS . $b;
        $classDir = ROOT . $class . '.php';
        if (is_file($classDir)) {
            require_once $classDir;
        } else {
            throw new \Exception('Archivo: ' . $b . " (" . $classDir . '), NO ENCONTRADO', 404);
        }
    }

}
