<?php

namespace XWork\Core;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Request.php
 * @desc        Request & URL parser
 * @subpackage  XWork\Core
 *
 */
final class Request {

    private $_package;
    private $_controller;
    private $_method;
    private $_args;

    public function __construct() {
        if (Requests\Get::issetKey('request')) {
            $u = Requests\Get::get('request');
            $ur = explode('/', $u);
            $url = array_filter($ur);
            $this->_package = strtolower(array_shift($url));

            if (!$this->_package) {
                $this->_package = DEFAULT_PACKAGE;
            }

            $this->_controller = strtolower(array_shift($url));

            if (!$this->_controller) {
                $this->_controller = DEFAULT_CONTROLLER;
            }
            $this->_method = strtolower(array_shift($url));
            $this->_args = $url;
        }

        if (!$this->_package) {
            $this->_package = DEFAULT_PACKAGE;
        }

        if (!$this->_controller) {
            $this->_controller = DEFAULT_CONTROLLER;
        }

        if (!$this->_method) {
            $this->_method = 'index';
        }

        if (!isset($this->_args)) {
            $this->_args = array();
        }
    }

    /**
     * Devuelve el package
     * @return String
     */
    public function getPackage() {
        return $this->_package;
    }

    /**
     * Devuelve el controlador
     * @return String
     */
    public function getController() {
        return $this->_controller;
    }


    /**
     * Devuelve el metodo
     * @return String
     */
    public function getMethod() {
        return $this->_method;
    }
    
    /**
     * Devuelve el o los argumento(s)
     * @param int $index índice del argumento
     * @return mixed, si está definido el indice a devolver, y si este existe, devuelve su valor, si no existe devuelve falso y si no está definido devuelve todos los argumentos
     */
    public function getArgs($index = false) {
        if (is_int($index)) {
            if (isset($this->_args[$index])) {
                return $this->_args[$index];
            } else {
                return false;
            }
        } else {
            return $this->_args;
        }
    }
    
    /**
     * Serializa la URI
     * @param boolean $method define si se serializa el metodo o no
     * @return string
     */
    public function serializeDir($method = true) {
        if ($method) {
            return $this->_package . '/' . $this->_controller . '/' . $this->_method;
        }
        return $this->_package . '/' . $this->_controller;
    }

}
