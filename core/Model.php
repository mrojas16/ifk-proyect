<?php

namespace XWork\Core;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use XWork\Core\Exceptions\HelperException;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Model.php
 * @desc        Models Manager
 * @subpackage  XWork\Core
 *
 */
class Model {
    
    protected $database = null;
    
    public function __construct() {
        $this->database = new Database\DatabaseProvider();
    }
    
    /**
     * Permite cargar un helper
     * @param string $package direccion del package al que corresponde el helper, o en caso de ser $helper falso, el nombre del helper root a utilizar
     * @param string $helper direccion del helper a utilizar
     * @return mixed retorna la instancia del helper
     * @throws HelperException
     */
    protected function loadHelper($package, $helper = false){
        if (!$helper) {
            $ruta = HELPERS . str_replace('.', DS, $package) . '.helper.php';
            try {
                if (is_file($ruta)) {
                    if (is_readable($ruta)) {
                        $class = '\XWork\App\Helpers\\' . str_replace('.', '_', $package);
                        require_once $ruta;
                        if (class_exists($class,false)) {
                            return new $class;
                        } else {
                            throw new HelperException('El helper: <b>' . $package . '</b> no es accesible, la clase esta mal instanciada');
                        }
                    } else {
                        throw new HelperException('El helper: <b>' . $package . '</b> no es accesible!');
                    }
                } else {
                    throw new HelperException('El helper: <b>' . $package . '</b> no existe!');
                }
            } catch (HelperException $exc) {
                Error::exception($exc);
            } catch (Exception $exc) {
                Error::exception($exc);
            }
        } else {
            $ruta = MODELS . str_replace('.', DS, $package) . DS . $helper . '.model.php';
            try {
                if (is_file($ruta)) {
                    if (is_readable($ruta)) {
                        $class = '\XWork\App\Models\\' . str_replace('.', '_', $package) . '\\' . $helper;
                        require_once $ruta;
                        if (class_exists($class,false)) {
                            return new $class;
                        } else {
                            throw new HelperException('El helper: <b>' . $package . '/' . $helper . ' - ' . $class . '</b> no es accesible, la clase esta mal instanciada');
                        }
                    } else {
                        throw new HelperException('El helper: <b>' . $package . '/' . $helper . '</b> no es accesible!');
                    }
                } else {
                    throw new HelperException('El helper: <b>' . $package . '/' . $helper . '</b> no existe!');
                }
            } catch (HelperException $exc) {
                Error::exception($exc);
            } catch (Exception $exc) {
                Error::exception($exc);
            }
        }
        
    }
    
    /**
     * Devuelve la fecha-hora en formato Y-m-d h:i:s
     * @return string
     */
    public static function now($hour = true){
        if ($hour) {
            return date('Y-m-d h:i:s');
        } else {
            return date('Y-m-d');
        }
    }

}
