<?php

namespace XWork\Core;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Exceptions\ViewException;
use \XWork\index;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        View.php
 * @desc        Views Manager
 * @subpackage  XWork\Core
 *
 */
class View {

    private $_js = array();
    private $_css = array();
    private $_scripts = array();
    private $statusTitle = false;

    private $_staticResources;
    private $controlador;
    private $package;
    private $template;

    public function __construct(Request $q, $staticResources = FALSE) {
        $this->_staticResources = $staticResources;
        $this->controlador = $q->getController();
        $this->package = str_replace('-', '/', $q->getPackage());
    }

    /**
     * Carga la vista
     * @param string $template Nombre de la vista a cargar
     * @param boolean $sandwich Define si la construccion de la vista se va a hacaer cargando el header y el footer o no
     * @param string $layout Define el layout a utilizar
     * @param mixed $controller Si se envía un string, se hace pasar como que la construccion de la vista se generará a partir de otro controlador
     * @throws ViewException
     */
    public function load($template = 'index', $sandwich = TRUE, $layout = DEFAULT_LAYOUT, $controller = FALSE) {
        $header_sandwich = LAYOUTS . DS . $layout . DS . 'header.xview';
        $footer_sandwich = LAYOUTS . DS . $layout . DS . 'footer.xview';
        $package = $this->package;
        try {
            if (!$controller) {
                $controller = $this->controlador;
            }
            if (!$this->_staticResources) {
                $ruta = VIEWS . str_replace('.', DS, $package) . DS . $controller . DS . $template . '.xview';
            } else {
                $ruta = $template;
            }

            if (is_readable($ruta)) {
                if ($sandwich) {
                    if (file_exists($header_sandwich) and file_exists($footer_sandwich)) {
                        $this->build($header_sandwich, $footer_sandwich, $ruta);
                    } else {
                        throw new ViewException('Error de template (<b>No se han encontrado los archivos principales de las plantillas</b>)', 1);
                    }
                } else {
                    $this->build(FALSE, FALSE, $ruta);
                }
            } else {
                throw new ViewException('Vista No Encontrada("' . $ruta . '"), Imposible renderizar la Aplicacion', 2);
            }
        } catch (ViewException $exc) {
            Error::exception($exc);
        } catch (\Exception $exc) {
            Error::exception($exc);
        }
    }

    /**
     * Metodo abreviado de carga para no utlizar metodo sandwich
     * @param string $template Nombre de la vista a cargar
     * @param string $layout Define el layout a utilizar
     * @param mixed $controller Si se envía un string, se hace pasar como que la construccion de la vista se generará a partir de otro controlador
     */
    public function loadAjax($template = 'index', $layout = DEFAULT_LAYOUT, $controller = FALSE) {
        $this->load($template, false, $layout, $controller);
    }

    /**
     * Construye la vista
     * @param string $header_sandwich direccion del header
     * @param string $footer_sandwich direccion del footer
     * @param string $ruta direccion de la vista
     */
    private function build($header_sandwich, $footer_sandwich, $ruta) {
        $hs = '';
        $fs = '';
        if ($header_sandwich) {
            $hs = file_get_contents($header_sandwich);
        }
        if ($footer_sandwich) {
            $fs = file_get_contents($footer_sandwich);
        }
        $r = file_get_contents($ruta);
        $this->template = $hs . $r . $fs;
    }

    /**
     * Asigna valor a las variables
     * @param mixed $var si es un array asociativo multidimensional, itera sobre el y asigna para cada indice un valor, si es un string, se ocupa como indice
     * @param string $content en el caso de que $var sea un string, este será el valor asignado a esa variable
     */
    public function assign($var, $content = '') {
        if (is_array($var)) {
            foreach ($var as $key => $value) {
                if (!is_array($value)) {
                    if ($value != "") {
                        $this->template = preg_replace('^\[IF\[:' . $key . ':\]THEN\[\?(.*)\?\]OR\[\?.*\?\]\|\]^', '${1}', $this->template);
                        $this->template = str_replace("[:" . $key . ":]", $value, $this->template);
                    }
                }
            }
        } else {
            if ($content != "") {
                $this->template = preg_replace('^\[IF\[:' . $var . ':\]THEN\[\?(.*)\?\]OR\[\?(.*)\?\]ENDIF\]^', '${1}', $this->template);
                $this->template = str_replace("[:" . $var . ":]", $content, $this->template);
            }
        }
    }
    
    /**
     * Limpia las variables no asignadas
     */
    private function removeEmpty() {
        $this->template = preg_replace('^\[IF\[:.*:\]THEN\[\?.*\?\]OR\[\?(.*)\?\]ENDIF\]^', '${1}', $this->template);
        $this->template = preg_replace('^\[:.*:\]^', "", $this->template);
    }

    /**
     * Renderiza la vista
     * @param bool $bool si es verdadero, imprime la vista, si es falso, la devuelve como un string
     * @return mixed si bool es verdadero retorna un string con el contenido de la vista, de lo contrario no retorna nada;
     */
    public function renderizar($bool = true) {
        $this->loadScripts('_js', '__PLUGIN_JS');
        $this->loadScripts('_scripts', '__SCRIPTS_JS');
        $this->loadScripts('_css', '__STYLE_SHEETS');
        $this->loadConstants();
        $this->removeEmpty();
        if ($bool) {
            eval("?>" . $this->template . "<?");
            die();
        } else {
            return $this->template;
        }
    }

    /**
     * Imprime scripts
     * @param string $var nombre del argumento que contiene los scripts
     * @param string $to variable contenedora de los scripts
     */
    private function loadScripts($var, $to) {
        if (count($this->{$var})) {
            $r = '';
            for ($i = 0; $i < count($this->{$var}); $i++) {
                $r .= $this->{$var}[$i];
            }
            $this->template = str_replace("[:" . $to . ":]", $r, $this->template);
        }
    }

    /**
     * Permite setear en tiempo de ejecución scripts js
     * @param array $js listado de los scripts a cargar
     * @param type $layout layout para traer los scripts
     * @throws ViewExc
     */
    public function setJs(array $js, $layout = DEFAULT_LAYOUT) {
        try {
            if (is_array($js) && count($js)) {
                for ($i = 0; $i < count($js); $i++) {
                    array_push($this->_js, "<script type=\"text/javascript\" src=\"". BASE_URL . 'layout/' . $layout . '/js/' . $js[$i] . '.js"></script>'."\n" );
                }
            } else {
                throw new ViewExc('Error de js', 3);
            }
        } catch (ViewExc $exc) {
            $this->handle_exception($exc);
        }
    }

    /**
     * Permite setear en tiempo de ejecución scripts css
     * @param array $js listado de los scripts a cargar
     * @param type $layout layout para traer los scripts
     * @throws ViewExc
     */
    public function setCss(array $js, $layout = DEFAULT_LAYOUT) {
        try {
            if (is_array($js) && count($js)) {
                for ($i = 0; $i < count($js); $i++) {
                    array_push($this->_js, BASE_URL . 'layout/' . $layout . '/css/' . $js[$i] . '.js');
                }
            } else {
                throw new ViewExc('Error de js', 3);
            }
        } catch (ViewExc $exc) {
            $this->handle_exception($exc);
        }
    }

    /**
     * Asigna un titulo a la página
     * @param string $title Titulo de la página
     */
    public function assignTitle($title) {
        if (!$this->statusTitle) {
            Session::set('TITLE_A', APP_NAME_TITLE . ' ' . APP_NAME_TITLE_SEPARATOR . ' ' . $title);
            $this->assign('__MAIN_TITLE', Session::get('TITLE_A'));
            $this->statusTitle = TRUE;
        }
    }

    /**
     * Carga constantes de vista
     */
    private function loadConstants() {
        $t = BASE_URL . 'layout' . '/' . DEFAULT_LAYOUT . '/';
        if (!$this->statusTitle) {
            $this->assign('__MAIN_TITLE', APP_NAME_TITLE);
            $this->statusTitle = true;
        }
        $this->assign('__BASE_URL', BASE_URL);
        $this->assign('__POWEREDBY', index::powerby());
        $this->assign('__BENCHMARK', index::benchmark(TRUE));
        $this->assign('__TPL_JS', $t . 'js/');
        $this->assign('__TPL_CSS', $t . 'css/');
        $this->assign('__TPL_IMG', $t . 'img/');
        $this->assign('__TPL_PLUGINS', $t . 'plugins/');
        $this->assign('__TPL_SOUND', $t . 'sound/');
        $this->assign('__TPL_', $t);
        $this->assign('__APP_NAME', APP_NAME_TITLE);
        $this->assign('__APP_NAME_SEPARATOR', APP_NAME_TITLE_SEPARATOR);
        $this->assign('__LOADED_JS', implode('', $this->_js));
        $this->assign('__USERNAME', Session::get('nombre') . ' ' . Session::get('apellido'));
        $this->assign('__TODAY', date('d-m-Y'));
        // Personalized
        $this->getpersonalized();
    }
    
    /**
     * Obtiene las constantes personalizadas
     */
    private function getpersonalized() {
        Config\ExtraViewsConstant::getVars();
        $a = Config\ExtraViewsConstant::$personalized_views_vars;
        foreach ($a as $k => $v) {
            $this->assign($k, $v);
        }
    }
    
    /**
     * Renderiza un script de la aplicacion
     * @param string $dir direccion la cual se encuentra el script app
     * @param array $assigns array asociativo unidimensional para asignar en cada variable key el valor
     */
    public function loadSelfScript($dir, array $assigns = array()) {
        $fdr = SCRIPTS . str_replace('.', DS, $dir) . '.js';
        $v = new View(new Request(), TRUE, TRUE);
        $v->loadAjax($fdr, DEFAULT_LAYOUT, "staticResources");
        $v->assign($assigns);
        $n = $v->renderizar(false);
        $src = "<script type=\"text/javascript\" id=\"postrunnable\">" . $n . "</script>";
        $this->assign(MAGIC_CONTAINER_SCRIPT, $src);
    }

}
