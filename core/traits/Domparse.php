<?php

namespace XWork\Core\Traits;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Domparse.php
 * @desc        Parser para el DOM
 * @subpackage  XWork\Core\Traits
 *
 */
trait Domparse {

    public function __construct() {
        
    }

    /**
     * Obtiene las opciones tipo <b>OPTION</b> para un Select
     * @param mixed $obj objeto iterable
     * @param string $value valor para el option
     * @param string $name label para el option
     * @param string $placeholder placeholder para el select
     * @param boolean $special muestra el placeholder
     * @param mixed $defvalue valor por defecto
     * @param bool $nospace define si los valores (en el caso de ser un array) seran concatenados con un espacio
     * @return string listado de options agrupados
     */
    public function getSelectOptions($obj, $value, $name, $placeholder = 'Seleccione una opcion', $special = false, $defvalue = 0, $nospace = false) {
        if (($placeholder AND FALSE) OR $special) {
            $r = "<option value=\"$defvalue\">" . $placeholder . "</option>";
        } else {
            $r = "";
        }
        $selected = 0;
        for ($i = 0; $i < count($obj); $i++) {
            $ares = $obj[$i];
            $r .= '<option value="' . $ares->{$value} . '" ' . (($selected == 0) ? " selected=\"selected\" " : '') . '>';
            $selected++;
            if (is_array($name)) {
                foreach ($name as $key => $na) {
                    if (isset($ares->{$na})) {
                        $r .= $ares->{$na};
                    } else {
                        $r .= $na;
                    }
                    if (!$nospace) {
                        $r .= ' ';
                    }
                }
            } else {
                $r .= $ares->{$name};
            }
            $r .= '</option>';
        }
        return $r;
    }

    /**
     * Obtiene las opciones tipo <b>OPTGROUP</b> para un Select
     * @param mixed $obj objeto iterable
     * @param string $idlabel valor para el option
     * @param string $label label para el option
     * @param string $value valor para el optgroup
     * @param string $name label para el optgroup
     * @param string $placeholder placeholder para el select
     * @param boolean $special muestra el placeholder
     * @param mixed $defvalue valor por defecto
     * @param bool $nospace define si los valores (en el caso de ser un array) seran concatenados con un espacio
     * @return string listado de options agrupados
     */
    public function getSelectOptGroup($obj, $idlabel, $label, $value, $name, $placeholder = 'Seleccione una opcion', $special = false, $defvalue = 0, $nospace = false) {
        if (($placeholder AND FALSE) OR $special) {
            $r = "<optgroup label='" . $placeholder . "'><option value=\"$defvalue\">" . $placeholder . "</option>";
        } else {
            $r = "";
        }
        $varauxidlabel = 0;
        for ($i = 0; $i < count($obj); $i++) {
            $ares = $obj[$i];
            if ($ares->{$idlabel} != $varauxidlabel) {
                $varauxidlabel = $ares->{$idlabel};
                $r .= "</optgroup>";
                $r .= '<optgroup label="' . ($ares->{$label}) . '">';
                $r .= '<option value="' . $ares->{$value} . '">';
                if (is_array($name)) {
                    foreach ($name as $key => $na) {
                        if (isset($ares->{$na})) {
                            $r .= $ares->{$na};
                        } else {
                            $r .= $na;
                        }
                        if (!$nospace) {
                            $r .= ' ';
                        }
                    }
                } else {
                    $r .= $ares->{$name};
                }
                $r .= '</option>';
            } else {
                $r .= '<option value="' . $ares->{$value} . '">';
                if (is_array($name)) {
                    foreach ($name as $key => $na) {
                        if (isset($ares->{$na})) {
                            $r .= $ares->{$na};
                        } else {
                            $r .= $na;
                        }
                        if (!$nospace) {
                            $r .= ' ';
                        }
                    }
                } else {
                    $r .= $ares->{$name};
                }
                $r .= '</option>';
            }
        }
        return $r;
    }

}
