<?php

namespace XWork\Core\Traits;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Fecha.php
 * @desc        Parser para fechas
 * @subpackage  XWork\Core\Traits
 *
 */
trait Fecha {

    public function __construct() {
        
    }

    public function invertirFecha($di, $delimiter = "-") {
        $d = explode($delimiter, $di);
        return $d[2] . $delimiter . $d[1] . $delimiter . $d[0];
    }

    public function getMonthByNum($num) {
        switch ($num) {
            case 1: return 'Enero';
            case 2: return 'Febrero';
            case 3: return 'Marzo';
            case 4: return 'Abril';
            case 5: return 'Mayo';
            case 6: return 'Junio';
            case 7: return 'Julio';
            case 8: return 'Agosto';
            case 9: return 'Septiembre';
            case 10: return 'Octubre';
            case 11: return 'Noviembre';
            case 12: return 'Diciembre';
            default: return 'Mes';
        }
    }

}
