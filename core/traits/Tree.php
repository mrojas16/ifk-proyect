<?php

namespace XWork\Core\Traits;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}


/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Tree.php
 * @desc        Parser tipo Arbol de LI-UL
 * @subpackage  XWork\Core\Traits
 *
 */
trait Tree {

    private $_stm;

    public function __construct() {
        
    }

    /**
     * Obtine el menu en formato de arbol
     * @param mixed $obj objeto iterable que contiene el menu
     * @return string arbol con el menu
     */
    public function getMenu($obj) {
        $r = "<ul id=\"tree\">" . "\n";
        $this->_stm = $obj;
        for ($i = 0; $i < count($this->_stm); $i++) {
            $ares = $this->_stm[$i];
            if ($ares->idpadre == 0) {
                $r .= ' <li>' . "\n";
                if ($ares->hijos == 1) {
                    $r .='      <label for="treemenu_' . $ares->idmenu . '_gv"></label> <input type="checkbox" id="treemenu_' . $ares->idmenu . '_gv" value="' . $ares->idmenu . '"/><span>' . $ares->nombre . '</span>' . "\n";
                    $r .= '     <ul>' . "\n";
                    $r .= $this->_getHijos($ares->idmenu, $ares->href);
                    $r .= '     </ul>' . "\n";
                } else {
                    $r .='  <label for="treemenu_' . $ares->idmenu . '_gv"></label> <input type="checkbox" id="treemenu_' . $ares->idmenu . '_gv" value="' . $ares->idmenu . '"/><span>' . $ares->nombre . '</span>' . "\n";
                }
                $r .= ' </li>' . "\n";
            }
        }
        $r .= '<ul>' . "\n";
        return $r;
    }

    /**
     * Devuelve los hijos del menú
     * @param mixed $id id del menu padre
     * @param string $href direccion del menu padre
     * @return string arbol con los hijos
     */
    private function _getHijos($id, $href) {
        $r = '';
        for ($i = 0; $i < count($this->_stm); $i++) {
            $ares = $this->_stm[$i];
            if ($ares->idpadre == $id) {
                $r .= '     <li>' . "\n";
                if ($ares->hijos == 1) {
                    $r .='          <label for="treemenu_' . $ares->idmenu . '_gv"></label> <input type="checkbox" id="treemenu_' . $ares->idmenu . '_gv" value="' . $ares->idmenu . '"/><span>' . $ares->nombre . '</span>' . "\n";
                    $r .= '         <ul>' . "\n";
                    $r .= $this->_getHijos($ares->idmenu, $href . '-' . $ares->href);
                    $r .= '         </ul>' . "\n";
                } else {
                    $r .='          <label for="treemenu_' . $ares->idmenu . '_gv" ></label> <input type="checkbox" id="treemenu_' . $ares->idmenu . '_gv" value="' . $ares->idmenu . '"/><span>' . $ares->nombre . '</span>' . "\n";
                }
                $r .= '     </li>' . "\n";
            }
        }
        return $r;
    }

    /**
     * Obtiene un arbol para checktree
     * @param mixed $obj objeto iterable
     * @param mixede $pk valor para cada uno
     * @param string $label label para cada uno
     * @return string arbol del objeto
     */
    public function getTableChecks($obj,$pk,$label) {
        $r = '<ul class="list-group clear-list m-t">';
        for ($i = 0; $i < count($obj); $i++) {
            $ares = $obj[$i];
            $r .= '<li class="list-group-item fist-item listtree">
                        <div class="checkbox i-checks listtree">
                            <label><input type="checkbox" name="treehlp_' . $ares->{$label} . '_' . $ares->{$pk} . '" value="' . $ares->{$pk} . '">' . $ares->{$label} . '</label>
                        </div>
                    </li>';
        }
        $r .= '</ul>';
        return $r;
    }

}
