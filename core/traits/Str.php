<?php

namespace XWork\Core\Traits;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}


/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Str.php
 * @desc        Funciones para Strings
 * @subpackage  XWork\Core\Traits
 *
 */
trait Str {
    
    public function __construct() {
        
    }

    /**
     * Recorta Texto
     * @param string $texto texto a recortar
     * @param int $limite limite a recortar
     * @return string Texto Recortado
     */
    public function recortar_texto($texto, $limite = 100) {
        $texto = trim($texto);
        $texto = strip_tags($texto);
        $tamano = strlen($texto);
        $resultado = '';
        if ($tamano <= $limite) {
            return $texto;
        } else {
            $texto = substr($texto, 0, $limite);
            $palabras = explode(' ', $texto);
            $resultado = implode(' ', $palabras);
            $resultado .= '...';
        }
        return $resultado;
    }

    /**
     * Genera String Aleatorio
     * @param int $length largo del String
     * @param string $possible Caracteres con los cuales genera el String
     * @return string Texto aleatorio
     */
    public function generateString($length = 8, $possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ") {
        $string = "";
        $i = 0;
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);
            $string .= $char;
            $i++;
        }
        return $string;
    }

}
