<?php

namespace XWork\Core\Traits;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}


/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Mail.php
 * @desc        Funciones para envíos de Mails
 * @subpackage  XWork\Core\Traits
 *
 */
trait Mail {

    private $mailLib;

    public function __construct() {
        require_once LIBS.'phpmailer/class.phpmailer.php';
    }

    /**
     * Gestion de envío de mail simple
     * @param string $mensaje Mensaje a enviar
     * @param string $destinatario_mail destinatario del mensaje
     * @param string $destinatario_nombre nombre del destinatario
     * @param string $subject motivo del mensaje
     */
    public function sendSimpleMail($mensaje, $destinatario_mail, $destinatario_nombre, $subject) {
        $mail = new \PHPMailer(true);

        $mail->IsSMTP();
        $mail->Host = SMTP_STD_SERVER;
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "ssl";

        $mail->Port = SMTP_STD_SERVER_PORT;
        $mail->Username = SMTP_STD_MAIL;
        $mail->Password = SMTP_STD_MAIL_PASS;

        $mail->AddAddress($destinatario_mail, $destinatario_nombre);
        $mail->SetFrom(SMTP_STD_MAIL, SMTP_STD_MAIL_NAME);
        $mail->IsHTML(true);

        $mail->Subject = $subject;
        $mail->Body = $mensaje;
        $mail->AltBody = 'Para ver este mensaje, por favor use un visor de de email compatible con HTML!';
        $mail->Send();
    }

    /**
     * Gestion de envío de mail con adjunto
     * @param string $mensaje Mensaje a enviar
     * @param string $destinatario_mail destinatario del mensaje
     * @param string $destinatario_nombre nombre del destinatario
     * @param string $subject motivo del mensaje
     * @param string $adjunto direccion del archivo adjunto
     * @param string $nombreAdjunto nombre del archivo a adjuntar
     */
    public function sendAttachedMail($mensaje, $destinatario_mail, $destinatario_nombre, $subject, $adjunto, $nombreAdjunto) {
        $mail = new \PHPMailer(true);

        $mail->IsSMTP();
        $mail->Host = SMTP_STD_SERVER;
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "ssl";

        $mail->Port = SMTP_STD_SERVER_PORT;
        $mail->Username = SMTP_STD_MAIL;
        $mail->Password = SMTP_STD_MAIL_PASS;

        $mail->AddAddress($destinatario_mail, $destinatario_nombre);
        $mail->SetFrom(SMTP_STD_MAIL, SMTP_STD_MAIL_NAME);
        $mail->IsHTML(true);
        $archivo = $adjunto;
        $mail->addAttachment($archivo, $nombreAdjunto);

        $mail->Subject = $subject;
        $mail->Body = $mensaje;
        $mail->AltBody = 'Para ver este mensaje, por favor use un visor de de email compatible con HTML!';
        $mail->Send();
    }


}
