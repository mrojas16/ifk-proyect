<?php

namespace XWork\Core\Traits;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Hash;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Hash.php
 * @desc        Funciones de Hash
 * @subpackage  XWork\Core\Traits
 *
 */
trait Hash {

    private $hasher;

    public function __construct() {
        $this->hasher = new Hash();
    }

    /**
     * Verificael hash de un usuario
     * @param int $userID id del usuario
     * @return string verificacion del usuario
     */
    public function getNewUserHashVerify($userID) {
        return $this->hasher->HashID($userID);
    }

    /**
     * Verifica un usuario con un salto preestablecido
     * @param int $iduser id del usario a verificar
     * @param string $salt salto preestablecido
     * @return string verificacion del usuario
     */
    public function verifyHashVerification($iduser, $salt) {
        return $this->hasher->HashIDVerification($iduser, $salt);
    }

}
