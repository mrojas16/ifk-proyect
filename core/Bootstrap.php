<?php

namespace XWork\Core;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Exceptions\BootstrapException;
use \XWork\Core\Error;
use \Exception;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Bootstrap.php
 * @desc        Arranque de la app
 * @subpackage  XWork\Core
 *
 */
class Bootstrap {

    /**
     * Arranca el framwork
     * @param \XWork\Core\Request $req
     * @throws BootstrapException
     * @throws Exceptions\BootstrapException
     */
    public static function run(Request $req) {
        $package = $req->getPackage();
        $controller = $req->getController();
        $method = $req->getMethod();
        $args = $req->getArgs();

        $dirController = CONTROLLERS . str_replace('.', DS, $package) . DS . $controller . '.controller.php';
        $controllerName = '\XWork\App\Controllers\\' . str_replace('.', '\\', $package) . '\\' . $controller;

        if (is_readable($dirController)) {
            require_once $dirController;
            try {
                $controllerName;
                if (class_exists($controllerName, FALSE)) {
                    $ctrlr = new $controllerName;
                } else {
                    $aa = explode('\\', str_replace('\XWork\App\Controllers\\', '', $controllerName));
                    $emth = array_pop($aa);
                    throw new Exceptions\BootstrapException("La pagina que estaba buscando: <b>'" . implode('.', $aa) . '/' . $emth . "'</b>, no existe; porfavor intentelo mas tarde; o vuelva al <a href=\"" . BASE_URL . "#index/dashboard\">inicio</a>; si cree que esto pueda ser un error, contácte a su administrador.", 404);
                }


                if ($ctrlr->getId() != -1) {
                    if (!(in_array($ctrlr->getId(), Session::get(DEFAULT_USER_PERMISOS)))) {
                        $aa = explode('\\', str_replace('\XWork\App\Controllers\\', '', $controllerName));
                        $emth = array_pop($aa);
                        throw new BootstrapException("Permiso denegado para: <b>" . implode('.', $aa) . '/' . $emth . "</b>; usted, bajo su perfil de usuario, no posee privilegios suficientes para poder mostrar la página; si cree que esto pueda ser un error, contácte a su administrador.", 401);
                    }
                }

                if (method_exists($ctrlr, 'index')) {
                    if (is_callable(array($ctrlr, $method))) {
                        $metodo = $req->getMethod();
                    } else {
                        $metodo = 'index';
                    }

                    if (count($args)) {
                        call_user_func_array(array($ctrlr, $metodo), $args);
                    } else {
                        call_user_func(array($ctrlr, $metodo));
                    }
                } else {
                    $aa = explode('\\', str_replace('\XWork\App\Controllers\\', '', $controllerName));
                    $emth = array_pop($aa);
                    throw new BootstrapException("Error en la Aplicación, la página <b>'" . implode('.', $aa) . '/' . $emth . "'</b>posee un error interno; intentelo mas tarde o contacte a su administrador.", 500);
                }
            } catch (BootstrapException $exc) {
                Error::bootstrap($exc);
            } catch (Exception $exc) {
                Error::exception($exc);
            }
        } else {
            try {
                $aa = explode('\\', str_replace('\XWork\App\Controllers\\', '', $controllerName));
                $emth = array_pop($aa);
                throw new Exceptions\BootstrapException("La pagina que estaba buscando: <b>'" . implode('.', $aa) . '/' . $emth . "'</b>, no existe; porfavor intentelo mas tarde; o vuelva al <a href=\"" . BASE_URL . "#index/dashboard\">inicio</a>; si cree que esto pueda ser un error, contácte a su administrador.", 404);
            } catch (BootstrapException $exc) {
                Error::bootstrap($exc);
            }
        }
    }

}
