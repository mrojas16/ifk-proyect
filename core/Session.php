<?php

namespace XWork\Core;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Session.php
 * @desc        Session Manager
 * @subpackage  XWork\Core
 *
 */
class Session {

    private static $excLogin = false;

    /**
     * Inicializa las Sessiones
     */
    public static function init() {
        if (SESSION_DIE_ON_CLOSE_BROWSER) {
            session_set_cookie_params(0, "/", $_SERVER["HTTP_HOST"], 0);
        }
        session_start();
    }

    /**
     * Si clave está definida elimina ese índice de la session, de otra forma, elimina la sesion completa
     * @param mixed $clave
     */
    public static function destroy($clave = false) {
        if ($clave) {
            if (is_array($clave)) {
                for ($i = 0; $i < count($clave); $i++) {
                    if (isset($_SESSION[$clave[$i]])) {
                        unset($_SESSION[$clave[$i]]);
                    }
                }
            } else {
                if (isset($_SESSION[$clave])) {
                    unset($_SESSION[$clave]);
                }
            }
        } else {
            Log::session('LOGOUT', 'OK');
            unset($_SESSION['onlogin']);
            session_unset();
            session_destroy();
        }
    }

    /**
     * Retorna el valor de un indice de sesion
     * @param string $target
     * @return mixed si existe devuelve el valor, de otra forma, devuelve falso
     */
    public static function get($target) {
        if (isset($_SESSION[$target])) {
            return $_SESSION[$target];
        } else {
            return false;
        }
    }

    /**
     * Asigna un valor a un indice
     * @param String $target indice a asignar
     * @param mixed $value valor a asignar
     * @return mixed Valor asignado
     */
    public static function set($target, $value) {
        return $_SESSION[$target] = $value;
    }

    /**
     * Permite eliminar una session despues de un tiempo específico
     * @return null
     * @throws SessionException
     */
    public static function tiempo() {
        if (!self::get('tiempo') || !defined('SESSION_TIME')) {
            throw new SessionException('No se ha definido el tiempo de sesion');
        }
        
        if (SESSION_TIME == 0) {
            return;
        }
        
        $last = self::get('last_timestamp');
        $now = date("Y-n-j H:i:s"); 
        $transcurred = (strtotime($now)-strtotime($last));
        
        if($transcurred>=(SESSION_TIME*60)){
            log::session('LOGOUT', 'Sesion cerrada por tiempo inactiva');
            self::destroy();
            header('location:' . BASE_URL . DEFAULT_LOGIN);
        } else {
            self::set('last_timestamp', time());
        }
    }

    /**
     * Verifica si existe una session, y ademas si el usuario esta logueado
     */
    public static function onlogin() {
        if (!session::get('onlogin')) {
            if (!self::$excLogin) {
                $url = "http://" . $_SERVER['HTTP_HOST'] . ":" . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'];
                log::session('ACCESO_RESTRINGIDO', 'Usuario ha Intentado acceder sin estar logueado a: ' . $url);
                header('location:' . BASE_URL . DEFAULT_LOGIN);
                exit;
            }
        }
    }

}
