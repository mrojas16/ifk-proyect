<?php

namespace XWork\Core\Misc;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Hash;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        File.php
 * @desc        Gestion y manejo de archivos
 * @subpackage  XWork\Core\Misc
 *
 */
class Files {

    private function __construct() {
        
    }
    
    /**
     * Convierte un fichero a base64.
     * @param string $dir : direccion del fichero; puede ser utilizada con $_FILES o \XWork\Core\Requests\Files::get().
     * @param mixed $mime : Tipo mime para el fichero, en caso de estar definido, devuelve con tipo mime, si no lo esta, devuelve solo el string base64.
     * @return string retorna un string base64 válido.
     */
    public static function fileToBase64($dir, $mime = false) {
        $imgbinary = fread(fopen($dir, "r"), filesize($dir));
        if ($mime) {
            return 'data:' . self::mime_content_type($dir) . ';base64,' . base64_encode($imgbinary);
        } else {
            return base64_encode($imgbinary);
        }
    }
    
    /**
     * Devuelve tipo mime de un fichero
     * @param string $filename : direccion del fichero; puede ser utilizada con $_FILES o \XWork\Core\Requests\Files::get().
     * @param int $mode : 1: checkea solo extension | 0: checkea con herramientas del servidor
     * @return string retorna tipo mime de un fichero
     */
    public static function mime_content_type($filename, $mode = 0) {
        $mime_types = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',
            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $as = explode('.', $filename);
        $ext = strtolower($as[(count($as)-1)]);



        if (function_exists('mime_content_type') && $mode == 0) {
            $mimetype = mime_content_type($filename);
            return $mimetype;
        } elseif (function_exists('finfo_open') && $mode == 0) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        } elseif (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        } else {
            return 'application/octet-stream';
        }
    }

    /**
     * Recorta una imagen y la encuadra al centro
     * @param string $file : direccion del fichero; puede ser utilizada con $_FILES o \XWork\Core\Requests\Files::get().
     * @param boolean $tmp : verifica si debe retornar una direccion o el contenido del fichero
     * @return string direccion o contenido de la imagen
     */
    public static function recorteEncuadreIMG($file, $tmp = false) {
        $ti = explode('/', self::mime_content_type($file));
        $tipo = $ti[(count($ti) - 1)];

        $tamano = getimagesize($file);
        $orig_Ancho = $tamano[0];
        $orig_Alto = $tamano[1];
        $recorte_izq = 0;
        $recorte_sup = 0;
        $recorte_der = 0;
        $recorte_inf = 0;
        if ($orig_Alto > $orig_Ancho) {
            $recorte_sup = $recorte_inf = ($orig_Alto - $orig_Ancho) / 2;
        } else {
            $recorte_der = $recorte_izq = ($orig_Ancho - $orig_Alto) / 2;
        }

        $margen = 0;

        $Ancho_recortado = $orig_Ancho - $recorte_izq - $recorte_der;
        $Alto_recortado = $orig_Alto - $recorte_sup - $recorte_inf;

        $ampliacion_X = 1.3;
        $ampliacion_Y = 1.3;

        $papel_Ancho = $Ancho_recortado * $ampliacion_X + 2 * $margen;
        $papel_Alto = $Alto_recortado * $ampliacion_Y + 2 * $margen;
        $resultado_Ancho = $papel_Ancho - 2 * $margen;
        $resultado_Alto = $papel_Alto - 2 * $margen;
        switch ($tipo) {
            case "jpg":
                $copia = imagecreatefromjpeg($file);
                break;
            case "png":
                $copia = imagecreatefrompng($file);
                break;
            case "gif":
                $copia = imagecreatefromgif($file);
                break;
            default:
                $copia = imagecreatefromjpeg($file);
                break;
        }

        $ampliada = imagecreatetruecolor($papel_Ancho, $papel_Alto);

        if ($tipo == "gif" or $tipo == "png") {
            imagecolortransparent($ampliada, imagecolorallocatealpha($ampliada, 0, 0, 0, 127));
            imagealphablending($ampliada, false);
            imagesavealpha($ampliada, true);
        }
        $file_ret = ROOT . 'warehouse' . DS . 'tmp' . DS . 'tpmImg_' . Hash::getSimpleHash() . '.' . $tipo;

        $fondo = imagecolorAllocate($ampliada, 255, 0, 0);
        imagefill($ampliada, 0, 0, $fondo);
        imagecopyresampled($ampliada, $copia, $margen, $margen, $recorte_izq, $recorte_sup, $resultado_Ancho, $resultado_Alto, $Ancho_recortado, $Alto_recortado);
        switch ($tipo) {
            case 'gif': $r = imagegif($ampliada, $file_ret);
                break;
            case 'jpg': $r = imagejpeg($ampliada, $file_ret);
                break;
            case 'png': $r = imagepng($ampliada, $file_ret);
                break;
            default: $r = imagejpeg($ampliada, $file_ret);
                break;
        }
        imagedestroy($ampliada);
        if (!$tmp) {
            unlink($file_ret);
            return $r;
        } else {
            return $file_ret;
        }
    }

    /**
     * Obtiene la extensión de un nombre de archivo
     * @param string $nfile nombre del archivo
     */
    public static function getExtension($nfile) {
        $a = explode('.', $nfile);
        return array_pop($a);
    }

}
