<?php

namespace XWork\Core;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Hash.php
 * @desc        Hash Parsers
 * @subpackage  XWork\Core
 *
 */
class Hash {

    private $salt;

    private function __construct() {
        
    }

    /**
     * Verifica un hash
     * @param mixed $id
     * @param mixed $salt
     * @return string
     */
    public function HashIDVerification($id, $salt) {
        if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
            $hs = str_replace('/', '_', crypt($id, $salt));
        } else {
            $hs = str_replace('/', '_', crypt($id));
        }
        return $hs;
    }

    /**
     * Genera un hash mediante Blowfish
     * @param mixed $id
     * @param mixed $salt
     * @return string
     */
    public function blowfish($in, $digito = 7) {
        if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
            $set_salt = '.1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            $salt = sprintf('$2a$%02d$', $digito);
            for ($i = 0; $i < 22; $i++) {
                $salt .= $set_salt[mt_rand(0, 63)];
            }
            $this->salt = $salt;
            return str_replace('/', '_', crypt($in, $salt));
        } else {
            return str_replace('/', '_', crypt($in));
        }
    }

    /**
     * Genera un hash generico
     * @param int $length largo del hash a devolver
     * @return string
     */
    public static function getSimpleHash($length = 8) {
        $code = sha1(uniqid(rand(), true));
        if ($length != "") {
            return substr($code, 0, $length);
        } else {
            return $code;
        }
    }

}
