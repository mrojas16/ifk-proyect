<?php

namespace XWork\Core\Database;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\ORM\DatabaseTools\Functions;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        DatabaseProvider.php
 * @desc        Database Transactions 
 * @subpackage  XWork\Core
 *
 */
class DatabaseProvider extends Database {

    private $resource;
    private $obj = null;
    private $_autocommit;

    public function __construct() {
        parent::__construct();
        $this->resource = self::getInstance();
        $this->query("SET NAMES 'utf8'");
    }

    /**
     * Termina la conexion de la base de datos
     */
    public function disconnect() {
        $this->resource->close();
    }

    /**
     * Obtiene el código de error de SQLSTATE asociado con la última operación del gestor de sentencia
     * @return String
     */
    public function getErrorState() {
        $a = $this->resource->errorInfo();
        return $a[0];
    }

    /**
     * Obtiene el código de error específico del controlador asociado con la última operación del gestor de sentencia
     * @return int
     */
    public function getErrorNo() {
        $a = $this->resource->errorInfo();
        return $a[1];
    }

    /**
     * Obtiene el mensaje de error específico del controlador asociado con la última operación del gestor de sentencia
     * @return int
     */
    public function getError() {
        $a = $this->resource->errorInfo();
        return $a[2];
    }

    /**
     * Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto PDOStatement
     * @param String $q Query a ejecutar
     * @return PDOStatement
     */
    public function query($q) {
        return $this->obj = $this->resource->query($q);
    }

    /**
     * Devuelve el número de filas afectadas por la última sentencia SQL
     * @param PDOStatement $resource recurso a analizar
     * @return int cantidad de filas
     */
    public function numRows($resource) {
        $num_rows = 0;
        if ($resource) {
            $num_rows = $resource->num_rows;
        }
        return $num_rows;
    }

    /**
     * Averigua si actualmente existe una transaccion activa
     * @return boolean
     */
    public function inTransaction() {
        return $this->resource->inTransaction();
    }

    /**
     * Inicia una transacción
     * @return \XWork\Core\Database\DatabaseProvider
     */
    public function begin() {
        $this->_autocommit = TRUE;
        if (!$this->inTransaction()) {
            $this->resource->beginTransaction();
        } else {
            $this->rollback();
            $this->resource->beginTransaction();
        }
        return $this;
    }

    /**
     * Revierte una transacción
     * @return \XWork\Core\Database\DatabaseProvider
     */
    public function rollback() {
        $this->resource->rollback();
        return $this;
    }

    /**
     * Consigna una transacción
     * @return \XWork\Core\Database\DatabaseProvider
     */
    public function commit() {
        $this->resource->commit();
        return $this;
    }

    /**
     * Prepara una sentencia para su ejecución
     * @param type $q
     * @return \XWork\Core\Database\DatabaseProvider
     */
    public function prepare($q) {
        $this->obj = $this->resource->prepare($q);
        return $this;
    }

    /**
     * Ejecuta una sentencia SQL y realiza el bind a los valores
     * @param array $array Listado de valores asociativos a una consulta
     * @return \XWork\Core\Database\DatabaseProvider
     */
    public function execute(array $array = array()) {
        $this->obj->execute($array);
        return $this;
    }

    /**
     * Devuelve un array que contiene todas las filas del conjunto de resultados
     * @return array
     */
    public function fetchObject() {
        $r = array();
        while ($row = $this->obj->fetch(\PDO::FETCH_OBJ)) {
            $r[] = $row;
        }
        $this->obj = null;
        return $r;
    }

    /**
     * Verifica si existe una conexión activa
     * @return boolean
     */
    public function isConnected() {
        return !is_null($this->resource);
    }

    /**
     * Devuelve el ID de la última fila o secuencia insertada
     * @param string $name Nombre de la secuencia de objetos de la cual el ID debe ser devuelto.
     * @return int
     */
    public function getInsertedID($name = null) {
        return $this->resource->lastInsertId($name);
    }
    
    /**
     * Ejecuta una query para realizar un cambio de bases de datos
     * @param String $database nombre de la base de datos a la cual conectarse
     * @return PDOStatement
     */
    public function changeDB($database) {
        return $this->query('USE ' . $database);
    }

    
    /**
     * Ejecuta una query para realizar un cambio de charset de la bases de datos
     * @param String $charset nombre del charset base de datos
     * @return PDOStatement
     */
    public function setCharset($charset = 'utf8') {
        return $this->resource->exec("set names " . $charset);
    }

    /**
     * Devuelve el ID de la última fila o secuencia insertada
     * @param string $name Nombre de la secuencia de objetos de la cual el ID debe ser devuelto.
     * @return int
     */
    public function lastId() {
        return $this->lastId = $this->resource->lastInsertId();
    }

    /**
     * Obtiene una estructura extendida de luna tabla específica
     * @param String $tabla Nombre de la Tabla
     * @param String $schema Nombre del schema a conetarse por defecto DB_NAME
     * @return array Listado de la estructura de la tabla
     * @throws \XWork\Core\Exceptions\DatabaseException
     */
    public function describeTable($tabla, $schema = DB_NAME) {
        $result = array();
        if ($schema) {
            $sql = 'DESCRIBE ' . Functions::addQuotes("$schema.$tabla", true);
        } else {
            $sql = 'DESCRIBE ' . Functions::addQuotes($tabla, true);
        }

        if (($queryResult = $this->obj->query($sql))) {
            while ($row = $queryResult->fetch(\PDO::FETCH_ASSOC)) {
                $result[] = $row;
            }
            $queryResult->closeCursor();
        } else {
            throw new \XWork\Core\Exceptions\DatabaseException($this->obj->error, $this->obj->errno);
        }

        $desc = array();

        $row_defaults = array(
            'Length' => null,
            'Scale' => null,
            'Precision' => null,
            'Unsigned' => null,
            'Primary' => false,
            'PrimaryPosition' => null,
            'Identity' => false
        );
        $i = 1;
        $p = 1;
        foreach ($result as $key => $row) {
            $matches = null;
            $row = array_merge($row_defaults, $row);
            if (preg_match('/unsigned/', $row['Type'])) {
                $row['Unsigned'] = true;
            }
            if (preg_match('/^((?:var)?char)\((\d+)\)/', $row['Type'], $matches)) {
                $row['Type'] = $matches[1];
                $row['Length'] = $matches[2];
            } else if (preg_match('/^decimal\((\d+),(\d+)\)/', $row['Type'], $matches)) {
                $row['Type'] = 'decimal';
                $row['Precision'] = $matches[1];
                $row['Scale'] = $matches[2];
            } else if (preg_match('/^float\((\d+),(\d+)\)/', $row['Type'], $matches)) {
                $row['Type'] = 'float';
                $row['Precision'] = $matches[1];
                $row['Scale'] = $matches[2];
            } else if (preg_match('/^((?:big|medium|small|tiny)?int)\((\d+)\)/', $row['Type'], $matches)) {
                $row['Type'] = $matches[1];
            }
            if (strtoupper($row['Key']) == 'PRI') {
                $row['Primary'] = true;
                $row['PrimaryPosition'] = $p;
                if ($row['Extra'] == 'auto_increment') {
                    $row['Identity'] = true;
                } else {
                    $row['Identity'] = false;
                }
                ++$p;
            }
            $desc[($row['Field'])] = array(
                'SCHEMA_NAME' => $schema,
                'TABLE_NAME' => $tabla,
                'COLUMN_NAME' => $row['Field'],
                'COLUMN_POSITION' => $i,
                'DATA_TYPE' => $row['Type'],
                'DEFAULT' => $row['Default'],
                'NULLABLE' => (bool) ($row['Null'] == 'YES'),
                'LENGTH' => $row['Length'],
                'SCALE' => $row['Scale'],
                'PRECISION' => $row['Precision'],
                'UNSIGNED' => $row['Unsigned'],
                'PRIMARY' => $row['Primary'],
                'PRIMARY_POSITION' => $row['PrimaryPosition'],
                'IDENTITY' => $row['Identity']
            );
            ++$i;
        }
        return $desc;
    }

}
