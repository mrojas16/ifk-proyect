<?php

namespace XWork\Core\Database;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \PDO;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Database.php
 * @desc        Database Manager Singlenton Class
 * @subpackage  XWork\Core\Database
 *
 */
class Database {

    private static $instance;

    protected function __construct() {
        
    }

    /**
     * Genera una conexion a bases de datos tipo PDO
     * @return PDOInstance
     */
    public static function getInstance() {
        if (!defined('DB_HOST') || !defined('DB_NAME') || !defined('DB_PASS') || !defined('DB_USER') || !defined('DB_PORT')) {
            Error::exception(new Exceptions\DatabaseException('Configuracion de Conexión de Bases de Datos Incompleta o Inexistente', -12));
        }
        if (empty(self::$instance)) {
            switch (strtolower(DB_CONNECTOR)) {
                case "mssql":
                    self::$instance = new PDO("mssql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS, array(PDO::ATTR_PERSISTENT => true));
                    break;
                case "sqlsrv":
                    self::$instance = new PDO("sqlsrv:server=" . DB_HOST . ";database=" . DB_NAME, DB_USER, DB_PASS);
                    break;
                case "ibm":
                    self::$instance = new PDO("ibm:DRIVER={IBM DB2 ODBC DRIVER};DATABASE=" . DB_NAME . "; HOSTNAME=" . DB_HOST . ";PORT=" . DB_PORT . ";PROTOCOL=TCPIP;", DB_USER, DB_PASS);
                    break;
                case "dblib":
                    self::$instance = new PDO("dblib:host=" . DB_HOST . ":" . DB_PORT . ";dbname=" . DB_NAME, DB_USER, DB_PASS);
                    break;
                case "odbc":
                    self::$instance = new PDO("odbc:Driver={Microsoft Access Driver (*.mdb)};Dbq=C:\accounts.mdb;Uid=" . DB_USER);
                    break;
                case "oci":
                case "oracle":
                    self::$instance = new PDO("OCI:dbname=" . DB_NAME . ";charset=UTF-8", DB_USER, DB_PASS);
                    break;
                case "ifmx":
                case "informix":
                    self::$instance = new PDO("informix:DSN=InformixDB", DB_USER, DB_PASS);
                    break;
                case "fbd":
                case "firebird":
                    self::$instance = new PDO("firebird:dbname=" . DB_HOST . ":" . DB_NAME, DB_USER, DB_PASS);
                    break;
                case "mysql":
                    self::$instance = (is_numeric(DB_PORT)) ? new PDO("mysql:host=" . DB_HOST . ";port=" . DB_PORT . ";dbname=" . DB_NAME, DB_USER, DB_PASS) : new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS);
                    break;
                case "sqlite2":
                    self::$instance = new PDO("sqlite:" . DB_HOST);
                    break;
                case "sqlite":
                case "sqlite3":
                    self::$instance = new PDO("sqlite::memory");
                    break;
                case "pg":
                case "pgsql":
                    self::$instance = (is_numeric(DB_PORT)) ? new PDO("pgsql:dbname=" . DB_NAME . ";port=" . DB_PORT . ";host=" . DB_HOST, DB_USER, DB_PASS) : new PDO("pgsql:dbname=" . DB_NAME . ";host=" . DB_HOST, DB_USER, DB_PASS);
                    break;
                default:
                    self::$instance = null;
                    break;
            }

            self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$instance->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            self::$instance->query('SET NAMES utf8');
            self::$instance->query('SET CHARACTER SET utf8');
        }
        return self::$instance;
    }

}
