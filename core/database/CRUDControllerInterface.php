<?php

namespace XWork\Core\Database;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        CRUDControllerInterface.php
 * @desc        Instance of CRUD in Controllers
 * @subpackage  XWork\Core\Database
 *
 */
interface CRUDControllerInterface {

    public function getId();
    public function get_table();
    public function get();
    public function add();
    public function edit();
    public function delete();
}
