<?php

namespace XWork\Core\Config;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        defaults.php
 * @desc        Define las constantes de configuracion necesarias para el funcionamiento del sistema.
 * @subpackage  XWork\Core\Config
 * @category    Configs
 *
 */

/**
 * Define si se guarda los logs de incidentes
 */
define('PRINT_XLOGS',false);

/**
 * Define la zona horaria <https://php.net/manual/es/timezones.php>
 */
define('TIMEZONE','America/Santiago');

/**
 * No cambiar, arranque del benchmark
 */
define('START_TIME', \XWork\index::getmicrotime());

/**
 * Permite definir el Lugar donde se encuentran los Controladores de la Aplicacion
 */
define('CONTROLLERS', APP . 'controllers' . DS);

/**
 * Permite definir el Lugar donde se encuentran las vistas de la Aplicacion
 */
define('VIEWS', APP . 'views' . DS);

/**
 * Permite definir el Lugar donde se encuentran los modelos de la Aplicacion
 */
define('MODELS', APP . 'models' . DS);

/**
 * Permite definir el Lugar donde se encuentran los scripts JS de la Aplicacion
 */
define('SCRIPTS', APP . 'scripts' . DS);

/**
 * Permite definir el Lugar donde se encuentran los modulos de la Aplicacion
 */
define('MODULOS', ROOT . 'modules' . DS);

/**
 * Permite definir el Lugar donde se encuentran los helpers de la Aplicacion
 */
define('HELPERS', APP . 'helpers' . DS);

/**
 * Permite definir el Lugar donde se encuentran los objetos para ORM de la Aplicacion
 */
define('ORMOBJECTS', ROOT . 'dbobjects' . DS);

/**
 * Permite definir el Lugar donde se encuentran llas librerias de la Aplicacion
 */
define('LIBS', ROOT . 'libs' . DS);

/**
 * Permite definir el Lugar donde se encuentran el Layout de la Aplicacion
 */
define('LAYOUTS', ROOT . 'layout' );

/**
 * Define El tiempo predeterminado de Session en minutos
 */
define('SESSION_TIME',999);

/**
 * Define El tiempo predeterminado de Session en minutos
 */
define('SESSION_DIE_ON_CLOSE_BROWSER',TRUE);

/**
 * Define la llave para definir que existe una sesion activa
 */
define('SESSION_ONLOGIN','onlogin');
