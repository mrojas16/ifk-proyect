<?php

namespace XWork\Core\Config;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        defaults.php
 * @desc        Define las direcciones estandar del sistema
 * @subpackage  XWork\Core\Config
 * @category    Configs
 *
 */

/**
 * Define cual es la direccion donde se ejecutara el framework
 */
define('BASE_URL', 'http://dev.proyect.cod/');
