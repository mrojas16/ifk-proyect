<?php

namespace XWork\Core\Config;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        orm.php
 * @desc        Estructuras configurables de orm
 * @subpackage  XWork\Core\Config
 * @category    Configs
 *
 */

class ORMConfig {
    
    public static function getUnset(){
        return array(
            '_queryString',
            'fecha_creacion',
            'fecha_edicion',
            'fecha_eliminacion',
            'ideliminador',
            'idmodificador', 
            'idcreador',
            'activo'
        );
    }
}
