<?php

namespace XWork\Core\Config;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        database.php
 * @desc        Configuraciones de Bases de Datos
 * @subpackage  XWork\Core\Config
 * @category    Configs
 *
 */
/**
 * Define las bases de datos a conectar
 */
$conections = array(
    'DEV' => array(
        'name' => 'ifk_proyect',
        'user' => 'root',
        'pass' => 'rootsafe',
        'host' => '10.0.2.3',
        'port' => 3306
    ),
    'PROD' => array(
        'name' => 'ifk_proyect',
        'user' => 'root',
        'pass' => 'root',
        'host' => 'localhost',
        'port' => 3306
    ),
    'TEST' => array(
        'name' => 'ifk_proyect',
        'user' => 'root',
        'pass' => 'root',
        'host' => 'localhost',
        'port' => 3306
    )
);

if (DEVELOPMENT_ENVIRONMENT === true) {
    $key = 'DEV';
} elseif (DEVELOPMENT_ENVIRONMENT === false) {
    $key = 'PROD';
} else {
    if (array_key_exists(DEVELOPMENT_ENVIRONMENT, $conections)) {
        $key = DEVELOPMENT_ENVIRONMENT;
    } else {
        throw new \Exception('Configuracion de Bases de Datos no encontrada', -1);
    }
}

/**
 *        Define el nombre del esquema de la Base de Datos
 */
define('DB_NAME', $conections[$key]['name']);

/**
 *        Define el nombre del usuario de la Base de Datos
 */
define('DB_USER', $conections[$key]['user']);

/**
 *        Define la Contraseña del usuario de la Base de Datos
 */
define('DB_PASS', $conections[$key]['pass']);

/**
 *        Define el Host donde se encuentra la Base de Datos
 */
define('DB_HOST', $conections[$key]['host']);

/**
 *  Define el driver con el cual se conectara pdo.
 *  Los drivers disponibles varian segun la version de php y el sistema
 *  operativo utilizado; para mas informacion visite: <http://php.net//manual/es/pdo.drivers.php>, 
 *  o a travez de la funcion \PDO::getAvailableDrivers(). 
 *  Por defecto estos son los drivers soportados:
 *      - mysql
 *      - sqlite
 *      - firebird
 *      - mssql
 *      - sybase
 *      - dblib
 *      - ibm
 *      - informix
 *      - sqlsrv
 *      - oci
 *      - odbc
 *      - pgsql
 *      - 4D
 * 
 *  <b>ADVERTENCIA:</b> debido a la imposibilidad de alguunas pruebas
 *  al menos en esta version solo será probada para mysql, en el caso que lo amerite
 *  pronte se irá testeando para los demas drivers.
 */
define('DB_CONNECTOR', 'mysql');

/**
 *        Define el Puerto donde se encuentra la Base de Datos
 */
define('DB_PORT', $conections[$key]['port']);

/**
 *        Define un prefijo para las tablas, en el caso de que existiera
 */
define('DB_PRFX', '');

unset($key);
unset($conections);
