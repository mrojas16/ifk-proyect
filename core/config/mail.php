<?php

namespace XWork\Core\Config;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        defaults.php
 * @desc        Controles especificos para sistema de envio de correos
 * @subpackage  XWork\Core\Config
 * @category    Configs
 *
 */

/**
 * Define el servidor standard de smtp a usar
 */
define('SMTP_STD_SERVER',"SECURE.EMAILSRVR.COM");

/**
 * Define el puerto del servidor standard de smtp a usar
 */
define('SMTP_STD_SERVER_PORT',465);

/**
 * Define el correo smtp a usar
 */
define('SMTP_STD_MAIL',"contacto@ifk.cl");

/**
 * Define el correo smtp a usar
 */
define('SMTP_STD_MAIL_NAME','Sistemas IFK');

/**
 * Define el correo smtp a usar
 */
define('SMTP_STD_MAIL_PASS',"2015,T..");
