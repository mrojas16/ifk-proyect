<?php

namespace XWork\Core\Config;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Session;
/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        extra_views_constants.php
 * @desc        Permite generar nuevas constantes de vistas
 * @subpackage  XWork\Core\Config
 * @category    Configs
 *
 */
class ExtraViewsConstant {

    /**
     * Contiene un array unidimensional para definir las constantes personalizadas de vista
     * @var array 
     */
    static $personalized_views_vars = array();

    /**
     * Inicializa las constantes
     */
    public static function getVars() {
        self::$personalized_views_vars = array(
            '__USER_NAME' => Session::get('full_name'),
            '__USER_PROFILE' => Session::get('profile_name'),
            '__TODAY_FORMATED' => date('d-m-Y'),
            '__LOGO_MIN' => BASE_URL . 'layout/inspinia/img/d_logo_min.png',
            '__LOGO_BIG' => BASE_URL . 'layout/inspinia/img/d_logo_big.png',
            '__LOGO_MED' => BASE_URL . 'layout/inspinia/img/d_logo_med.png',
            '__APP_NAME_TITLE' => APP_NAME_TITLE
        );
    }

}
