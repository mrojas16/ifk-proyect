<?php

namespace XWork\Core\Config;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        defaults.php
 * @desc        Constantes para definir direcciones y nombres por defecto
 * @subpackage  XWork\Core\Config
 * @category    Configs
 *
 */

/**
 * Permite definir cual es el package por defecto
 */
define('DEFAULT_PACKAGE', 'index');

/**
 * Permite definir cual es el controlador por defecto
 */
define('DEFAULT_CONTROLLER', 'index');

/**
 * Permite definir cual es el controlador del Login
 */
 define('DEFAULT_LOGIN', 'index/login');

/**
 * Permite definir cual es el controlador del sistema de bloqueo de la app
 */
 define('DEFAULT_LOCKED', 'index/lock');
 
/**
 * Permite definir cual es el controlador del Logout
 */
define('DEFAULT_LOGOUT', 'logout');

/**
 * Permite definir cual es el controlador de errores, para los handlers
 */
define('DEFAULT_ERROR', 'index/error');

/**
 * Permite definir cual es el controlador de errores, para los handlers
 */
define('DEFAULT_VERIFY', 'index/verify');

/**
 * Permite definir cual es el controlador de errores, para los handlers
 */
define('DEFAULT_PROFILE_USER', 'index/user');

/**
 * Permite definir cual es el controlador de menu
 */
define('DEFAULT_MENU', 'index/menu');

/**
 * Permite definir cual es la variable de sesion donde se guardan los permisos del usuario, si es que aplica
 */
define('DEFAULT_USER_PERMISOS', 'permisos');

/**
 * Define el estado por defecto de la aplicacion en cuanto al comportamiento del controlador y las sessiones PHP; 
 * si es false tiene que tener si o si una session activa para poder renderizar; de otra forma no es requisito
 */
define('DEFAULT_SESSION_STATE', false);

/**
 * Permite definir cual es el prefijo para controladores de Ajax
 * @deprecated since version 3
 */
define('DEFAULT_AJAX_PREFIX', 'app');

/**
 * Permite definir cual es el layout Predeterminado
 */
define('DEFAULT_LAYOUT', 'inspinia');

/**
 * Permite definir cual es el handler de los modulos
 */
define('DEFAULT_MODULE_HANDLER', '__handler.php');

/**
 * Define el nombre por defecto de la aplicacion
 */
define('APP_NAME_TITLE','IFK Proyect');

/**
 * Define el separador del nombre de la aplicacion
 */
define('APP_NAME_TITLE_SEPARATOR','|');

/**
 * Define la variable de vista que va a contener el script del modulo/app
 */
define('MAGIC_CONTAINER_SCRIPT','EXPLICIT_SCRIPT');