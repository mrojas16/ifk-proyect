<?php

namespace XWork\Core;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \Exception;
use XWork\Core\Exceptions\BootstrapException;
use XWork\Core\View;
use XWork\Core\Request;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Error.php
 * @desc        Gestion de Errores
 * @subpackage  XWork\Core
 *
 */
class Error {

    /**
     * Controla exepciones no atrapadas
     * @param Exception $e
     */
    public static function exception(Exception $e) {
        self::isAjax();
        if($e instanceof Exceptions\BootstrapException){
            http_response_code($e->getCode());
        }
        echo "<h2>Algo ha salido Mal!</h2>";
        echo "<p>Buscaremos una pronta solución...</p>";
        echo '<div class="jumbotron" style="padding:10px">
                              <p>Trace:</p>
                              <div>
                                        ' . $e->getMessage() . '<br><br><pre>' . $e->getTraceAsString() . '</pre>
                              </div>
                              <div style="text-align: right;padding: 10px 60px;">
                                        <a class="btn btn-primary btn-xs" role="button">Informar...</a>
                              </div>
                    </div>';
        die();
    }

    /**
     * Controla errores estandard de PHP
     * @param int $errno umero del error
     * @param string $errstr contenido del error
     * @param string $errfile archivo donde esta el error
     * @param string $errline linea donde está el error
     * @return boolean
     */
    public static function trigger_error($errno, $errstr, $errfile, $errline) {
        if (!(error_reporting() & $errno) || !(DEVELOPMENT_ENVIRONMENT)) {
            return;
        }

        switch ($errno) {
            case 2:
                echo "<b>ERROR - Warning:</b> [$errno][L:$errline] $errstr [$errfile]<br />\n";
                break;

            case 8:
                echo "<b>ERROR - Notice: </b> [$errno][L:$errline]  $errstr [$errfile]<br />\n";
                break;

            case 4096:
                echo "<b>ERROR - Catchable Fatal Error: </b> [$errno][L:$errline]  $errstr [$errfile]<br />\n";
                break;

            default:
                echo "<b>ERROR - Unknown: </b> [$errno][L:$errline]  $errstr [$errfile]<br />\n";
                break;
        }
        return true;
    }
    
    public static function bootstrap(\Exception $e) {
        $v = new View(new Request(), TRUE);
        http_response_code($e->getCode());
        $v->load(VIEWS . 'error/index.xview' , FALSE);
        $v->assign('CODE',$e->getCode());
        $v->assign('TITLE',self::getTitleBootstrap($e->getCode()));
        $v->assign('MESSAGE',$e->getMessage());
        $v->renderizar();
    }
    
    private static function getTitleBootstrap($num){
        switch ($num) {
            case '401': return 'No posee Permiso para acceder a la página';
            case '403': return 'El acceso a esta página está prohibido';
            case '404': return 'Página no Encontrada';
            case '405': return 'El metodo no está disponible';
            case '500': return 'Error en la aplicación';
            case '503': return 'Servicio no Disponible';
            default: return 'Error Desconocido';
        }
    }
            
    /**
     * Verifica si la llamada se hace desde ajax
     */
    private static function isAjax() {
        $a = getallheaders();
        if (!empty($a) && isset($a['XJENGINE_XWORK'])) {
            return;
        } else {
            $r = new Request();
            $dir = BASE_URL . '#' . $r->serializeDir(false);
            header('Location:' . $dir);
            die();
        }
    }

}
