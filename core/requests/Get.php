<?php

namespace XWork\Core\Requests;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Get.php
 * @desc        Request type GET parser
 * @subpackage  XWork\Core\Request
 *
 */
class Get {

    private function __construct() {
        
    }

    /**
     * Obtiene valores de la superglobal $_GET
     * @param string $key Indice a devolver
     * @return string Si existe el indice retorna el valor, de otra forma retorna vacío
     */
    public static function get($key) {
        return (self::issetKey($key)) ? filter_input(INPUT_GET, $key) : '';
    }

    /**
     * Verifica la existencia de un indice en la superglobal $_GET
     * @param string $key Indice a Verificar
     * @return boolean Retorna verdadero si existe, falso si no
     */
    public static function issetKey($key) {
        return (filter_input(INPUT_GET, $key)) ? TRUE : FALSE;
    }
    
    /**
     * Retorna la Superglobal $_GET completa
     * @return array
     */
    public static function getAll() {
        return $_GET;
    }

}
