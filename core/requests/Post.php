<?php

namespace XWork\Core\Requests;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        POST.php
 * @desc        Request type POST parser
 * @subpackage  XWork\Core\Request
 *
 */
class Post {

    private function __construct() {
        
    }

    /**
     * Obtiene el valor de un indice de la Superglobal $_POST
     * @param string $key indice a buscar
     * @return mixed si existe el valor lo retorna, si no, retorna vacío
     */
    public static function get($key) {
        return (self::issetKey($key)) ? filter_input(INPUT_POST, $key) : '';
    }

    /**
     * Verifica la existencia de un indice
     * @param string $key indice a buscar
     * @return bool
     */
    public static function issetKey($key) {
        return (filter_input(INPUT_POST, $key)) ? TRUE : FALSE;
    }
    
    /**
     * Retorna la Superglobal $_POST completa
     * @return array
     */
    public static function getAll() {
        return $_POST;
    }

}
