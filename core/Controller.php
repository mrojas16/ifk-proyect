<?php

namespace XWork\Core;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Exceptions\ModelException;
use \XWork\Core\Exceptions\HelperException;
use \XWork\Core\Error;
use \Exception;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Controller.php
 * @desc        Controllers
 * @subpackage  XWork\Core
 *
 */
abstract class Controller {

    /**
     * Permite generar un excepcion de session, para renderizarse sin tener que tener una session PHP activa
     * @var boolean 
     */
    public static $_loginException = DEFAULT_SESSION_STATE;
    
    /**
     * Instancia de vista para los controladores
     * @var type \XWork\Core\View
     */
    protected $_view;
    
    public function __construct() {
        $req = new Request();
        if (!self::$_loginException and ($req->serializeDir(FALSE) != DEFAULT_LOGIN)) {
            if (!Session::get(SESSION_ONLOGIN)) {
                $this->redireccionar(DEFAULT_LOGIN);
            }
            if (Session::get('locked')  and (($req->serializeDir(FALSE) != DEFAULT_LOCKED) OR ($req->serializeDir(FALSE) != DEFAULT_LOGIN) )) {// OR ($req->serializeDir(FALSE) != DEFAULT_LOGIN))
                $this->redireccionar(DEFAULT_LOCKED);
            }
        } else {
//            Session::tiempo();
            if(Session::get(SESSION_ONLOGIN) && Session::get('locked') and ($req->serializeDir(FALSE) != DEFAULT_LOCKED)) {
                $this->redireccionar(DEFAULT_LOCKED);
            }
            Session::set('localtimestamp', time());
            Session::set('last_timestamp', time());
        }
        $this->_view = new View($req);
    }

    /**
     * Metodo abstracta de arranque de los controladores
     */
    public abstract function index();

    /**
     * Metodo abstracta que define a que retorna el id de menú correspondiente a cada controlador; si este no existiera se debe colocar como retorno -1
     */
    public abstract function getId();

    /**
     * Permite cargar un modelo
     * @param string $package direccion del package al que corresponde el modelo, o en caso de ser $model falso, el nombre del modelo root a utilizar
     * @param string $model direccion del modelo a utilizar
     * @return mixed retorna la instancia del modelo
     * @throws ModelException
     */
    protected function loadModel($package, $model = false) {
        if (!$model) {
            $ruta = MODELS . str_replace('.', DS, $package) . '.model.php';
            try {
                if (is_file($ruta)) {
                    if (is_readable($ruta)) {
                        $class = '\XWork\App\Models\\' . $package;
                        require_once $ruta;
                        if (class_exists($class,false)) {
                            return new $class;
                        } else {
                            throw new ModelException('El modelo: <b>' . $package . '</b> no es accesible, la clase esta mal instanciada');
                        }
                    } else {
                        throw new ModelException('El modelo: <b>' . $package . '</b> no es accesible!');
                    }
                } else {
                    throw new ModelException('El modelo: <b>' . $package . '</b> no existe!');
                }
            } catch (ModelException $exc) {
                Error::exception($exc);
            } catch (Exception $exc) {
                Error::exception($exc);
            }
        } else {
            $ruta = MODELS . str_replace('.', DS, $package) . DS . $model . '.model.php';
            try {
                if (is_file($ruta)) {
                    if (is_readable($ruta)) {
                        $class = '\XWork\App\Models\\' . str_replace('.', '\\', $package) . '\\' . $model;
                        require_once $ruta;
                        if (class_exists($class,false)) {
                            return new $class;
                        } else {
                            throw new ModelException('El modelo: <b>' . $package . '/' . $model . ' - ' . $class . '</b> no es accesible, la clase esta mal instanciada');
                        }
                    } else {
                        throw new ModelException('El modelo: <b>' . $package . '/' . $model . '</b> no es accesible!');
                    }
                } else {
                    throw new ModelException('El modelo: <b>' . $package . '/' . $model . '</b> no existe!');
                }
            } catch (ModelException $exc) {
                Error::exception($exc);
            } catch (Exception $exc) {
                Error::exception($exc);
            }
        }
    }
    

    /**
     * Permite cargar un helper
     * @param string $package direccion del package al que corresponde el helper, o en caso de ser $helper falso, el nombre del helper root a utilizar
     * @param string $helper direccion del helper a utilizar
     * @return mixed retorna la instancia del helper
     * @throws ModelException
     */
    protected function loadHelper($package, $helper = false){
        if (!$helper) {
            $ruta = HELPERS . str_replace('.', DS, $package) . '.helper.php';
            try {
                if (is_file($ruta)) {
                    if (is_readable($ruta)) {
                        $class = '\XWork\App\Helpers\\' . str_replace('.', '\\', $package);
                        require_once $ruta;
                        if (class_exists($class,false)) {
                            return new $class;
                        } else {
                            throw new HelperException('El helper: <b>' . $package . '</b> no es accesible, la clase esta mal instanciada');
                        }
                    } else {
                        throw new HelperException('El helper: <b>' . $package . '</b> no es accesible!');
                    }
                } else {
                    throw new HelperException('El helper: <b>' . $package . '</b> no existe!');
                }
            } catch (HelperException $exc) {
                Error::exception($exc);
            } catch (Exception $exc) {
                Error::exception($exc);
            }
        } else {
            $ruta = HELPERS . str_replace('.', DS, $package) . DS . $helper . '.helper.php';
            try {
                if (is_file($ruta)) {
                    if (is_readable($ruta)) {
                    $class = '\XWork\App\Helpers\\' . str_replace('.', '\\', $package) . '\\' . $helper;
                        require_once $ruta;
                        if (class_exists($class,false)) {
                            return new $class;
                        } else {
                            throw new HelperException('El helper: <b>' . $package . '/' . $helper . ' - ' . $class . '</b> no es accesible, la clase esta mal instanciada');
                        }
                    } else {
                        throw new HelperException('El helper: <b>' . $package . '/' . $helper . '</b> no es accesible!');
                    }
                } else {
                    throw new HelperException('El helper: <b>' . $package . '/' . $helper . '</b> no existe!');
                }
            } catch (HelperException $exc) {
                Error::exception($exc);
            } catch (Exception $exc) {
                Error::exception($exc);
            }
        }
        
    }
    
    /**
     * Constructor de la vista
     * @param \XWork\Core\Request $q Sistemna de request
     * @param type $staticResources Define si se van a utilizar recursos estáticos
     */
    protected function createNewInstanceOfView($handler = false) {
        return new View(new Request, $handler);
    }

    /**
     * Redirecciona la ejecucion
     * @param string $where lugar a redireccionar
     */
    protected function redireccionar($where = '') {
        header('Location:' . BASE_URL . $where);
        exit;
    }
    
    /**
     * Verifica si la llamada se hace desde ajax
     */
    protected function isAjax() {
        $a = getallheaders();
        if ((!empty($a) && isset($a['XJENGINE_XWORK']))||(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {
            return;
        } else {
            $r = new Request();
            $this->redireccionar('#'.$r->serializeDir(false));
        }
    }

}
