<?php

namespace XWork\Core;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Response.php
 * @desc        Respuestas JSON
 * @subpackage  XWork\Core
 *
 */
class Response {

    const JSONTRUE = '{"response":1}';
    const JSONFALSE = '{"response":0}';
    const JSONNULL = '{}';

    /**
     * Permite parsear una respuesta JSON
     * @param array $data array con los datos a parsear
     * @return String Cadena JSON parseada
     */
    public static function JSONDATA(array $data) {
        return '{"response":1,"data":' . json_encode($data) . '}';
    }

    /**
     * Permite parsear un error o excepcion para devolverla como una cadena JSON
     * @param mixed $traceMessage error o Excepcion a parsear
     * @param int $errno numero de error
     * @return String Cadena JSON parseada
     */
    public static function JSONTRACEERROR($traceMessage,$errno = 0) {
        if (!($traceMessage instanceof \Exception)) {
            return '{"response":0,"message":"' . (string) $traceMessage . '","errno":' . (int) $errno . '}';
        } else {
            return '{"response":0,"message":"' . (string) $traceMessage->getMessage() . '","trace":"' . (string) $traceMessage->getTraceAsString() . '","errno":' . (int) $traceMessage->getCode() . '}';
        }
    }
    
    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}
        

}
