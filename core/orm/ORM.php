<?php

namespace XWork\Core\ORM;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Exceptions\ORMException;
use \XWork\Core\Error;
use \XWork\Core\ORM\ORMTable;
use \XWork\Core\ORM\ORMView;
use \XWork\Core\ORM\ORMTemporaryObject;
use \XWork\Core\ORM\ORMModifiable;
use \XWork\Core\ORM\Transaction;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        ORM.php
 * @desc        Class para definir y entablar transacciones entre los objetos ORM y las Bases de Datos
 * @subpackage  XWork\Core\ORM
 *
 */
abstract class ORM {

    /**
     * Query a Utilizar
     * @var String
     */
    public $_queryString;
    protected static $_classDataSource = null;
    protected static $_name = null;
    private $_data;
    private $_isNew;
    private $_dataSource;
    private static $_md5 = false;

    public function __construct($data = array()) {
        $this->_data = $data;
        $this->_isNew = true;
        $this->_dataSource = new Transaction();
        self::$_classDataSource = new Transaction();
    }

    /**
     * Modifica o agrega datos del array asociativo columna-valor
     * @param array $data 
     */
    public function populateFromRow($data) {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    /**
     * Asigna un valor a una columna
     * @param String $key columna a asignar
     * @param Mixed $value valor a asignar
     */
    public function set($key, $value) {
        if (isset($this->{$key})) {
            $this->{$key} = $value;
        }
    }

    /**
     * Retorna el valor de una columna
     * @param String $key nombre de la columna
     * @return Mixed retorna, en el caso de que exista la columna, el valor de esta, de otra forma retorna falso
     */
    public function get($key) {
        if (isset($this->{$key})) {
            return $this->{$key};
        }
        return false;
    }
    
    /**
     * Asigna el estado de autocommit
     * @param boolean $status
     */
    public function autoCommit($status = true) {
        $this->getDataSource()->autoCommit($status);
    }
    
    /**
     * Cosigna una transaccion ORM
     */
    public function commit() {
        $this->getDataSource()->commit();
    }

    /**
     * Setea un objeto ORM como una nueva fila
     * @param boolean $isNew
     */
    protected function _setNew($isNew) {
        $this->_isNew = $isNew;
    }

    /**
     * Verifica si la fila del objeto es una nueva
     * @return boolean
     */
    public function isNew() {
        return $this->_isNew;
    }

    /**
     * Genera un nuevo objeto de transaccion
     * @return Transaction
     */
    protected function _getDataSource() {
        return new Transaction();
    }

    /**
     * Valida si un objeto es modificable
     * @throws ORMException
     */
    private function _validateModifiable() {
        if (!($this->getDataSource())) {
            throw new ORMException("Objeto de solo lectura.");
        }
        if ($this instanceof ORMView && !($this instanceof ORMTemporaryObject)){
            throw new ORMException("Objeto de vista.");
        } else if (!($this instanceof ORMTable) && !($this instanceof ORMTemporaryObject) && !($this instanceof ORMModifiable)){
            throw new ORMException("Objeto no válido.");
        }
    }

    /**
     * Asigna si los valores serán comparados como sentencias MD5
     * @deprecated since version 1.0.1
     * @param boolean $bool
     */
    public static function isMd5($bool) {
        self::$_md5 = $bool;
    }

    /**
     * Devuelve el valor de una columna cuando es un objeto
     * @param String $property nombre de la columna
     * @return mixed si existe la columna retorna su valor, de otra forma retorna null
     */
    public function __get($property) {
        if (isset($this->_data[$property])) {
            return $this->_data[$property];
        } else {
            return NULL;
        }
    }

    /**
     * Verifica si existe la columna cuando es un objeto
     * @param String $property nombre de la columna
     * @return boolean
     */
    public function __isset($property) {
        return isset($this->_data[$property]);
    }

    /**
     * Asigna el valor a una columna cuando es un objeto
     * @param String $property Columna a modificar
     * @param Mixed $value valor a asignar
     */
    public function __set($property, $value) {
        $this->_data[$property] = $value;
    }

    /**
     * Nullea una columna cuando es un objeto
     * @param String $property Columna a modificar
     */
    public function __unset($property) {
        $this->_data[$property] = null;
    }

    /**
     * Guarda los cambios del objeto ORM
     */
    public function save() {
        $this->_validateModifiable();

        if ($this->isNew()) {
            $this->_getDataSource()->add(static::_getName(), $this, static::$_primaryKey);
        } else {
            $this->_getDataSource()->update(static::_getName(), $this, static::$_primaryKey);
        }
    }

    /**
     * Devuelve el valor de la tabla del objeto ORM
     * @return String
     */
    protected static function _getName() {
        return isset(static::$_name) ? static::$_name : strtolower(get_called_class());
    }

    /**
     * Verifica si el objeto es una Tabla temporal
     * @return boolean
     */
    protected static function _getStatus() {
        return isset(static::$___status___) ? (static::$___status___ == 'TEMPORARY_TABLE')?true: FALSE : false;
    }

    /**
     * Genera un nuevo objeto de transaccion
     * @return Transaction
     */
    public static function getDataSource() {
        return new Transaction();
    }

    /**
     * Devuelve la primera fila de una consulta
     * @param array $criteria criterio de búsqueda ORM
     * @param String $order cadena de orden de la consulta
     * @return mixed si encuentra un objeto, lo devuelve, de otra forma, retorna null
     */
    public static function findFirst($criteria = array(), $order = null) {
        $objects = static::find($criteria, $order, 1, 0);
        return count($objects)>0 ? $objects[0] : null;
    }

    /**
     * Devuelve el listado de objetos que concuerden
     * @param array $criteria criterio de búsqueda
     * @param String $order cadena de orden de la consulta
     * @param int $limit limite de la consulta
     * @param int $offset Inicio de la consulta
     * @return array Listado de filas encontradas
     */
    public static function find($criteria = array(), $order = null, $limit = null, $offset = 0, $extra_params = "") {
        $query = "";
        $result = static::getDataSource()->find(static::_getName(), $criteria, $order, $limit, $offset, $extra_params, $query, '=', self::$_md5,static::_getStatus());
        $objects = array();

        foreach ($result as $data) {
            $object = new static($data);
            $object->_setNew(false);
            if (DEVELOPMENT_ENVIRONMENT) {
                $object->_queryString = $query;
            } else {
                unset($object->_queryString);
            }
            $objects[] = $object;
        }

        return $objects;
    }

    /**
     * Devuelve el listado de objetos que concuerden ademas de parsear columnas
     * @param array $criteria criterio de búsqueda
     * @param String $order cadena de orden de la consulta
     * @param int $limit limite de la consulta
     * @param int $offset Inicio de la consulta
     * @return array Listado de filas encontradas
     */
    public static function findCols($columns = "*", $criteria = array(), $order = null, $limit = null, $offset = 0, $extra_params = "") {
        $query = "";
        $result = static::getDataSource()->findCols(static::_getName(), $columns, $criteria, $order, $limit, $offset, $extra_params, $query, '=', self::$_md5,static::_getStatus());
        $objects = array();

        foreach ($result as $data) {
            $object = new static($data);
            $object->_setNew(false);
            if (DEVELOPMENT_ENVIRONMENT) {
                $object->_queryString = $query;
            } else {
                unset($object->_queryString);
            }
            $objects[] = $object;
        }

        return $objects;
    }

    /**
     * Devuelve el listado de objetos que concuerden, cruzándolos con otras tablas
     * @param array $joins Array asociativo donde se establecen todas las reglas para join
     * @param array $cols listao de columnas a devolver
     * @param array $criteria criterio de búsqueda
     * @param String $order cadena de orden de la consulta
     * @param int $limit limite de la consulta
     * @param int $offset Inicio de la consulta
     * @return array Listado de filas encontradas
     */
    public static function findJoin($joins = array(), $cols = array(), $criteria = array(), $order = null, $limit = null, $offset = 0) {
        $query = "";
        $result = static::getDataSource()->findJoin(static::_getName(),$joins, $criteria, $order, $limit, $offset, $query, '=', self::$_md5, $cols,static::_getStatus());
        $objects = array();

        foreach ($result as $data) {
            $object = new static($data);
            $object->_setNew(false);
            if (DEVELOPMENT_ENVIRONMENT) {
                $object->_queryString = $query;
            } else {
                unset($object->_queryString);
            }
            $objects[] = $object;
        }

        return $objects;
    }

    /**
     * Elimina los registros cunado se cumplan los criterios
     * @param array $criteria criterio de búsqueda
     * @param int $limit limite de la consulta
     * @param int $offset Inicio de la consulta
     * @return boolean
     */
    public static function deleteWhere($criteria = array(), $limit = null, $offset = 0) {
        $query = "";
        try{
            $result = static::getDataSource()->deleteWhere(static::_getName(), $criteria, $limit, $offset, $query, '=', self::$_md5);
            return true;
        } catch (ORMException $e){
            Error::exception($e);
            return false;
        }
    }

    /**
     * Devuelve el listado de objetos que concuerden, compara por matrices de valores
     * @param array $criteria criterio de búsqueda
     * @param String $order cadena de orden de la consulta
     * @param int $limit limite de la consulta
     * @param int $offset Inicio de la consulta
     * @param int $extra_params Parámetros extras para a consulta
     * @return array Listado de filas encontradas
     */
    public static function findIn($criteria = array(), $order = null, $limit = null, $offset = 0, $extra_params="") {
        $query = "";

        $result = static::getDataSource()->find(static::_getName(), $criteria, $order, $limit, $offset, $extra_params, $query, 'IN');
        $objects = array();

        foreach ($result as $data) {
            $object = new static($data);
            $object->_setNew(false);
            if (DEVELOPMENT_ENVIRONMENT) {
                $object->_queryString = $query;
            } else {
                unset($object->_queryString);
            }
            $objects[] = $object;
        }

        return $objects;
    }

    /**
     * Cuenta el numero de filas de acuerdo a el criterio
     * @param array $criteria criterio de búsqueda
     * @return type
     */
    public static function count($criteria = array()) {
        return static::getDataSource()->count(static::_getName(), $criteria);
    }

    /**
     * Elimina la fila del objeto específico
     * @return boolean
     */
    public function delete() {
        return static::getDataSource()->delete(static::_getName(), $this, static::$_primaryKey);
    }
    
    public static function transaction($callback) {
        if (!(static::getDataSource() instanceof Transaction)) {
            call_user_func($callback);
            return;
        }

        try {
            static::getDataSource()->beginTransaction();
            call_user_func($callback);
            static::getDataSource()->commit();
        } catch (ORMException $ex) {
            static::getDataSource()->rollBack();
            throw $ex;
        }
    }

    /**
     * Permite ejecutar una query auxiliar
     * @param String $q query a ejecutar
     * @return boolean
     */
    protected static function auxQuery($q){
        return static::getDataSource()->auxQuery($q);
    }

    /**
     * Permite parsear llamadas estaticas a objetos ORM
     * @param String $method
     * @param Mixed $params
     * @return type
     * @throws ORMException
     */
    public static function __callStatic($method, $params) {
        $matches = null;
        if (!preg_match('/^(find|findFirst|count)(By|In)(\w+)$/', $method, $matches)) {
            throw new ORMException("Llamado a metodo desconocido " , $method);
        }
        $cK = explode('_And_', preg_replace('/([a-z0-9])([A-Z])/', '$1_$2', $matches[3]));
        $criteriaKeys = array_map('strtolower', $cK);
        $criteriaValues = array_slice($params, 0, count($criteriaKeys));
        $criteria = array_combine($criteriaKeys, $criteriaValues);

        $method = $matches[1];
        if (strtolower($matches[2]) == 'in') {
            $method = $matches[1] . $matches[2];
        }

        return static::$method($criteria);
    }
    
    /**
     * Crea un objeto temporal a partir de una query
     * @param String $q query a ejecutar
     * @return DatabaseTools\TemporaryTable
     */
    public static function createTempObject($q){
        return static::getDataSource()->createTempObject($q);
    }
}
