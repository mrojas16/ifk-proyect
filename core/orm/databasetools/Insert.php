<?php

namespace XWork\Core\ORM\DatabaseTools;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Exceptions\DatabaseException;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Insert.php
 * @desc        ORM Database Tools: Insert Parser
 * @subpackage  XWork\Core\ORM\DatabaseTools
 *
 */
class Insert {

    protected $parts = array();    
    public $pk = null;
    private $values = array();

    public function __construct($method = NULL) {
        if ($method === NULL) {
            $this->parts['METHOD'] = 'INSERT INTO';
        }
    }

    /**
     * Asigna la tabla sobre la cual va a insertar
     * @param String $into nombre de la clase del objeto de la tabla
     * @return \XWork\Core\ORM\DatabaseTools\Update
     */
    public function into($into) {
        $name = '`' . str_replace('\\', '`.`', str_replace('xwork\ormconnectors\\', '', $into)) . '`';
        $this->parts['INTO'] = Functions::addQuotes($name);
        return $this;
    }

    /**
     * Asigna valores a la consulta
     * @param array $values Valores a asignar
     * @param String $pk primary key de la tabla
     * @throws DatabaseException
     */
    public function values($values) {
        if (is_array($values)) {
            $this->parts['VALUES'] = $values;
        } else {
            throw new DatabaseException('Tipo de datos no soportados', -989);
        }
    }

    /**
     * Devuelve query parseada
     * @return String
     * @throws DatabaseException
     */
    public function getQuery() {
        $q = NULL;
        if (isset($this->parts['METHOD'])) {
            $q = $this->parts['METHOD'] . " ";
        } else {
            throw new DatabaseException("Metodo de query no encontrado");
        }
        if (isset($this->parts['INTO'])) {
            $q .= $this->parts['INTO'] . " ";
        } else {
            throw new DatabaseException("Tabla de query no encontrado");
        }
        if (isset($this->parts['VALUES'])) {
            $q .= $this->parseValues($this->parts['VALUES'], $this->parts['INTO']) . " ";
        } else {
            throw new DatabaseException("Datos de query no encontrados");
        }
        return array('QUERY'=>$q,'VALUES'=>$this->values);
    }

    /**
     * Parsea valores de un array asociativo
     * @param array $values valores a parsear
     * @param String $table_name nombre de la tabla
     * @return String
     * @throws DatabaseException
     */
    private function parseValues(array $values, $table_name) {
        $cols = " ";
        $vals = " ";
        if (empty($values)) {
            throw new DatabaseException("Datos de query no encontrados");
        } else {
            foreach ($values as $key => $value) {
                if ($key != '_queryString') {
                    $cols .= Functions::addQuotes(Functions::removeQuotes($table_name) . '.' . Functions::removeQuotes($key)) . ',';
                    $this->values[] = $value;
                    $vals .= '?,';
                    
                }
            }
        }
        return "(" . trim($cols, ',') . ") VALUES (" . trim($vals, ',') . ")";
    }

}
