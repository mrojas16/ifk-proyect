<?php

namespace XWork\Core\ORM\DatabaseTools;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Exceptions\DatabaseException;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Functions.php
 * @desc        ORM Database Tools: Functions
 * @subpackage  XWork\Core\ORM\DatabaseTools
 *
 */
class Functions {

    /**
     * Retorna la query parseada
     * @param mixed $object objeto(s) para obtener la query
     * @return string
     * @throws DatabaseException
     */
    public static function getQuery($object = NULL) {
        if (!(is_object($object) || is_array($object)) || $object === NULL) {
            throw new DatabaseException("Imposible Obtener query, es necesario llamarla con un objeto");
        }

        if (is_object($object)) {
            return $object->_queryString;
        } else {
            foreach ($object as $key) {
                return $key->_queryString;
            }
        }
    }

    /**
     * Agrega quotes al nombre de la tabla
     * @param string $tabla
     * @param bool $bool
     * @return string
     */
    public static function addQuotes($tabla, $bool = true) {
        if ($bool) {
            return '`' . str_replace('.', '`.`', self::removeQuotes(self::removeNamespace($tabla))) . '`';
        } else {
            return "'" . $tabla . "'";
        }
    }

    /**
     * Quita el namespace del nombre del objeto que contiene la tabla
     * @param string $tabla
     * @return type
     */
    public static function removeNamespace($tabla) {
        $n = explode('\\', $tabla);
        return $n[(count($n) - 1)];
    }

    /**
     * elimina las quotes del nombre de una tabla
     * @param string $tabla
     * @return string
     */
    public static function removeQuotes($tabla) {
        return (str_replace('`.`', '.', str_replace("'.'", ".", trim(trim($tabla, '`'), "'"))));
    }

    /**
     * Obtiene las columnas de un objeto ORM
     * @param mixed $object
     * @return mixed
     */
    public static function getCols($object) {
        $multi = array();
        if (is_object($object)) {
            $a = get_object_vars($object);
            unset($a['_queryString']);
            return $a;
        } else if (is_array($object)) {
            foreach ($object as $key => $value) {
                if (is_object($value)) {
                    $a = get_object_vars($value);
                    unset($a['_queryString']);
                    $multi[] = $a;
                } else if (is_array($value)) {
                    $multi[] = self::getCols($value);
                } else {
                    $multi[] = null;
                }
            }
            return $multi;
        } else {
            return null;
        }
    }
    
}
