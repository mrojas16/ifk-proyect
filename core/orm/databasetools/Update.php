<?php

namespace XWork\Core\ORM\DatabaseTools;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Exceptions\DatabaseException;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Update.php
 * @desc        ORM Database Tools: Update Parser
 * @subpackage  XWork\Core\ORM\DatabaseTools
 *
 */
class Update {

    const SPACE = " ";

    protected $parts = array();
    
    public $pk = NULL;
    private $values;

    public function __construct($method = NULL) {
        if ($method === NULL) {
            $this->parts['METHOD'] = 'UPDATE';
        }
    }

    /**
     * Asigna la tabla sobre la cual va a modificar
     * @param String $into nombre de la clase del objeto de la tabla
     * @return \XWork\Core\ORM\DatabaseTools\Update
     */
    public function table($into) {        
        $name = '`' . str_replace('\\', '`.`', str_replace('xwork\ormconnectors\\', '', $into)) . '`';
        $this->parts['TABLE'] = $name;
        return $this;
    }

    /**
     * Asigna valores a la consulta
     * @param array $values Valores a asignar
     * @param String $pk primary key de la tabla
     * @throws DatabaseException
     */
    public function values($values, $pk) {
        $this->pk = $pk;
        if (is_array($values)) {
            $this->parts['VALUES'] = $values;
        } else {
            throw new DatabaseException('Tipo de datos no soportados', -989);
        }
    }

    /**
     * Devuelve query parseada
     * @return String
     * @throws DatabaseException
     */
    public function getQuery() {
        $q = NULL;
        if (isset($this->parts['METHOD'])) {
            $q = $this->parts['METHOD'] . self::SPACE;
        } else {
            throw new DatabaseException("Metodo de query no encontrado");
        }
        if (isset($this->parts['TABLE'])) {
            $q .= $this->parts['TABLE'] . self::SPACE;
        } else {
            throw new DatabaseException("Tabla de query no encontrado");
        }
        if (isset($this->parts['VALUES'])) {
            $q .= $this->parseValues($this->parts['VALUES'], $this->parts['TABLE']) . self::SPACE;
        } else {
            throw new DatabaseException("Datos de query no encontrados");
        }
        if ($this->pk !== NULL) {
            $q .= 'WHERE ' . Functions::addQuotes(Functions::removeQuotes($this->parts['TABLE']) . '.' . Functions::removeQuotes($this->pk)) .
                    " = " . Functions::addQuotes($this->parts['VALUES'][$this->pk], FALSE);
        } else {
            throw new DatabaseException("Condicion de query no encontrada");
        }
        
        return array('QUERY'=>$q,'VALUES'=>$this->values);
    }

    /**
     * Parsea valores de un array asociativo
     * @param array $values valores a parsear
     * @param String $table_name nombre de la tabla
     * @return String
     * @throws DatabaseException
     */
    private function parseValues(array $values, $table_name) {
        $data = self::SPACE;
        if (empty($values)) {
            throw new DatabaseException("Datos de query no encontrados");
        } else {
            foreach ($values as $key => $value) {
                if ($key != $this->pk && $key[0] != '_') {
                    $data .= Functions::addQuotes(Functions::removeQuotes($table_name) . '.' . Functions::removeQuotes($key)) . '= ?,';
                    $this->values[] = $value;
                }
            }
        }
        return 'SET ' . trim($data, ',');
    }

}
