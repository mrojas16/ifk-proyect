<?php

namespace XWork\Core\ORM\DatabaseTools;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Exceptions\ORMException;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        TemporaryTable.php
 * @desc        ORM Database Tools: TemporaryTable
 * @subpackage  XWork\Core\ORM\DatabaseTools
 *
 */
class TemporaryTable extends \XWork\Core\ORM\ORM implements \XWork\Core\ORM\ORMTemporaryObject {

    public function __construct($data = array()) {
        parent::__construct();
        if ($data && count($data)) {
            $this->populateFromRow($data);
        }
    }

    public static function describe() {
        
    }

    /**
     * Permite setear valores a las columnas
     * @param String $property columna
     * @param Mixed $value valor
     */
    public function __set($property, $value) {
        $this->{$property} = $value;
    }

    /**
     * Ingresa valores a la tabla
     * @param Array $describe valores
     * @throws ORMException
     */
    public function populateTemporary($describe) {
        if ($describe && count($describe)) {
            $this->_primaryKey = NULL;
            for ($i = 0; $i < count($describe); $i++) {
                $ares = $describe[$i];
                $this->{$ares->Field} = ($ares->Default!="")?$ares->Default:"";
            }
        } else {
            throw new ORMException("Describe Incorrecto");
        }
    }
    
    /**
     * Elimina la tala temporal
     * @return boolean
     */
    public static function dieTable() {
        return self::auxQuery("DROP TABLE ".self::$_name);
    }
    
    /**
     *Define que el objeto corresponde a una tabla temporal
     * @var String 
     */
    public static $___status___ = 'TEMPORARY_TABLE';
    
    /**
     * Nombre de la tabla temporal
     * @var String 
     */
    public static $_name = 'TEMPORARY_TABLE';
    
    public static $_primaryKey = null;

}
