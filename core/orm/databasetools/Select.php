<?php

namespace XWork\Core\ORM\DatabaseTools;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Exceptions\DatabaseException;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Select.php
 * @desc        ORM Database Tools: Select Parser
 * @subpackage  XWork\Core\ORM\DatabaseTools
 *
 */
class Select {

    const SPACE = " ";

    protected static $joinTypes = array(
        'inner join',
        'left join',
        'right join',
        'full join',
        'cross join',
        'natural join',
        'join'
    );
    protected $parts = array();
    private $values = array(1);

    public function __construct() {
        $this->parts['METHOD'] = 'SELECT';
        $this->parts['WHERE'] = 'WHERE 1=?';
    }

    /**
     * Define la tabla sobre la cual se va a realizar la búsqueda
     * @param string $name nombre de la tabla
     * @param boolean $temporary define si es una tabla temporal o no
     * @param array $colums columnas a devolver
     * @param String $schema schema sobre el cual trabajar
     * @return self
     */
    public function from($name, $temporary, $colums = array("*"), $schema = null) {
        if (!$temporary) {
            $name = '`' . str_replace('\\', '`.`', str_replace('xwork\ormconnectors\\', '', $name)) . '`';
        }
        $this->parts['FROMS'][] = $name;
        $this->parts['COLS'] = $colums;
        return $this->join(array($name => array(
                        'METHOD' => 'from',
                        'CONDITION' => NULL
                    )), $colums, $schema);
    }

    /**
     * Aplica Joins a otras tablas
     * @param array $joins reglas de joins
     * @param string $schema shema sobre el cual trabajar
     * @throws DatabaseException
     */
    public function jointable($joins, $schema = null) {
        $join = '';
        if (empty($joins)) {
            throw new DatabaseException("El nombre de la tabla no puede estar vacio");
        } else if (is_array($joins)) {
            foreach ($joins as $name => $args) {
                $this->parts['FROMS'][] = $name;
                if (is_array($args)) {
                    if (isset($args['METHOD']) && in_array(strtolower($args['METHOD']), self::$joinTypes)) {
                        $join .= " " . $args['METHOD'] . " ";
                    } else {
                        $join .= " " . self::$joinTypes[0] . " ";
                        $args['METHOD'] = self::$joinTypes[0];
                    }
                    $join .= $name . " ";
                    if (isset($args['CONDITION'])) {
                        $join .= 'ON ' . $args['CONDITION'] . " ";
                    }
                } else {
                    throw new DatabaseException("El listado de tablas debe estar correctamente argumentado", 100);
                }
            }
        } else {
            throw new DatabaseException("El listado de tablas debe estar correctamente argumentado", 100);
        }
        $this->parts['JOINS'][] = $join;
    }

    /**
     * Aplica Joins a otras tablas mediante reglas
     * @param array $joins reglas de joins
     * @param string $schema shema sobre el cual trabajar
     * @throws DatabaseException
     */
    protected function join($joins, $cols, $schema = null) {
        if (empty($joins)) {
            throw new DatabaseException("El nombre de la tabla no puede estar vacio");
        } else if (is_array($joins)) {
            foreach ($joins as $name => $args) {
                if (is_array($args)) {
                    if (!isset($args['SCHEMA'])) {
                        if ((strpos($name, '.') !== false)) {
                            list($sch, $tName) = explode('.', $name);
                            $joins[$name]['SCHEMA'] = $sch;
                            $a = $joins[$name];
                            $joins[$tName] = $a;
                            unset($joins[$name]);
                        } else {
                            $joins[$name]['SCHEMA'] = $schema;
                        }
                    }
                } else {
                    throw new DatabaseException("El listado de tablas debe estar correctamente argumentado", 100);
                }
            }
        } else {
            throw new DatabaseException("El listado de tablas debe estar correctamente argumentado", 100);
        }

        $from = FALSE;
        $join = "";
        foreach ($joins as $name => $args) {
            if (!isset($args['METHOD'])) {
                $args['METHOD'] = 'from';
            }
            if ($args['METHOD'] == 'from') {
                if (!$from) {
                    $aux = ($args['SCHEMA']) ? $args['SCHEMA'] . '.' . $name : $name;
                    $join = ' FROM ' . $aux . ' ' . $join;
                    $from = TRUE;
                } else {
                    throw new DatabaseException("Solo puede haber una tabla con metodo from");
                }
            } else {
                $aux = ($args['SCHEMA']) ? $args['SCHEMA'] . '.' . $name : $name;
                $join = $join . ' ' . $args['METHOD'] . ' ' . $aux . ' ';
                if ($args['CONDITION']) {
                    $join = $join . ' ON ' . $args['CONDITION'];
                }
            }
        }

        $this->parts['JOIN'] = $join;
        $this->parts['COLS'] = $cols;

        return $this;
    }

    /**
     * Asigna un orden a la consulta
     * @param type $order
     * @return \XWork\Core\ORM\DatabaseTools\Select
     */
    public function order($order) {
        $this->parts['ORDER'] = 'ORDER BY ' . $order;
        return $this;
    }

    /**
     * Asigna las condiciones a la consulta
     * @param string $condition
     * @param mixed $value
     * @param bool $bool
     * @param string $operator @deprecated
     * @param bool $md5 @deprecated
     * @return \XWork\Core\ORM\DatabaseTools\Select
     */
    public function where($condition = NULL, $value = NULL, $bool = true, $operator = '=', $md5 = false) {
        $cond = " ";
        if ($value !== NULL) {
            $this->values[] = $value;
            if (is_array($value)) {
                if (preg_match('/^between\((.*)\)$/i', $condition)) {
                    $cond .= $this->parseOperator($condition, $value);
                } else {
                    $cond .= $this->parseOperator($condition, $value) . '(' . str_repeat('?,', count($value) - 1) . "?) ";
                }
            } else {
                $cond .= $this->parseOperator($condition, $value) . "? ";
            }
        } else {
            $cond .= $this->parseOperator($condition, $value) . " ";
        }

        $this->parts['WHERE'] .= $cond;
        return $this;
    }

    /**
     * Obtiene el operador, evaluando una funcion
     * la evalucion de funcion no es case sensitive
     *  FUNCIONES:
     *      <ul>
     *          <li><b>NOT: </b> devuelve como si comparara distinto de <i>(<>)</i></li>
     *          <li><b>ISNULL: </b> devuelve como si comparara un valor nulo <i>(<=>)</i></li>
     *          <li><b>MINOR||SMALLER: </b> devuelve como si comparara menor de <i>(<)</i></li>
     *          <li><b>MAJOR||BIGGER: </b> devuelve como si comparara mayor de <i>(>)</i></li>
     *          <li><b>MINOREQUAL||SMALLEREQUAL: </b> devuelve como si comparara menor o igual de <i>(<=)</i></li>
     *          <li><b>MAJOREQUAL||BIGGEREQUAL: </b> devuelve como si comparara mayor o igual de <i>(>=)</i></li>
     *          <li><b>MD5: </b> parsea a funcion md5 <i>(<=>)</i></li>
     *      </ul>
     * @param mixed $condition condicion a evaluar
     * @param mixed $value valor de la condicion
     * @param boolean $concat debe o no verificar su concatenacion
     * @param terminacion $lastconcat ultimo caracter a concatenar
     * @return string funcion parseada
     */
    private function parseOperator($condition, $value, $concat = true, $lastconcat = " = ") {
        if (is_array($value)) {
            $r = '';
            $matches = null;
            if (preg_match('/^between\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if (!isset($value[0]) || !isset($value[1])) {
                    throw new DatabaseException('Faltan paramatros para la funcion BETWEEN; porfavor revíselos');
                }
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= $matches[1] . " BETWEEN ? AND ? ";
                return $r;
            }
            if ($concat) {
                $r .= $this->getConcatenation($condition);
            }
            $r .= '`' . $condition . '`' . " IN ";
            return $r;
        } else {
            $matches = null;
            if (preg_match('/^not\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= $this->parseOperator($matches[1], $value, false, '') . " <> ";
                return $r;
            }
            if (preg_match('/^minor\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= $this->parseOperator($matches[1], $value, false, '') . " < ";
                return $r;
            }
            if (preg_match('/^minorequal\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= $this->parseOperator($matches[1], $value, false, '') . " <= ";
                return $r;
            }
            if (preg_match('/^smaller\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= $this->parseOperator($matches[1], $value, false, '') . " < ";
                return $r;
            }
            if (preg_match('/^smallerequal\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= $this->parseOperator($matches[1], $value, false, '') . " <= ";
                return $r;
            }
            if (preg_match('/^major\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= $this->parseOperator($matches[1], $value, false, '') . " > ";
                return $r;
            }
            if (preg_match('/^majorequal\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= $this->parseOperator($matches[1], $value, false, '') . " >= ";
                return $r;
            }
            if (preg_match('/^bigger\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= $this->parseOperator($matches[1], $value, false, '') . " > ";
                return $r;
            }
            if (preg_match('/^biggerequal\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= $this->parseOperator($matches[1], $value, false, '') . " >= ";
                return $r;
            }
            if (preg_match('/^isnull\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= "ISNULL(" . $this->parseOperator($matches[1], $value, false, ")") . $lastconcat;
                return $r;
            }
            if (preg_match('/^or\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= $this->parseOperator($matches[1], $value, false, " = ");
                return $r;
            }
            if (preg_match('/^md5\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= "md5(" . $this->parseOperator($matches[1], $value, false, ")") . $lastconcat;
                return $r;
            }
            if (preg_match('/^crc32\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= "crc32(" . $this->parseOperator($matches[1], $value, false, ")") . $lastconcat;
                return $r;
            }
            if (preg_match('/^datediff\((.*)\)$/i', $condition, $matches)) {
                $r = '';
                if ($concat) {
                    $r .= $this->getConcatenation($condition);
                }
                $r .= "datediff(" . $matches[1] . ")" . $lastconcat;
                return $r;
            }

            $r = '';
            if ($concat) {
                $r .= $this->getConcatenation($condition);
            }
            $r .= '`' . $condition . '`' . $lastconcat;
            return $r;
        }
    }

    /**
     * Obtiene la funcion de Concatenacion de la query (AND|OR)
     * @param mixed $condition condicion a evaluar
     * @return string retorna la concatenacion evaluada
     */
    private function getConcatenation($condition) {
        $matches = null;
        if (preg_match('/^and\((.*)\)$/i', $condition, $matches)) {
            return "AND ";
        }
        if (preg_match('/^or\((.*)\)$/i', $condition, $matches)) {
            return "OR ";
        }
        return "AND ";
    }

    /**
     * Asigna un limite a l consulta
     * @param int $count limite de la consulta
     * @param int $offset inicio de la consulta
     * @return \XWork\Core\ORM\DatabaseTools\Select
     */
    public function limit($count = NULL, $offset = NULL) {
        if ($count) {
            $this->parts['LIMIT'] = ' LIMIT ' . $count . ' ';
            if ($offset) {
                $this->parts['LIMIT'] = ' ,' . $offset . ' ';
            }
        } else {
            $this->parts['LIMIT'] = NULL;
        }
        return $this;
    }

    /**
     * Permite añadir parámetros extras a la consulta
     * @param string $extra_params parámetros a añadir
     * @return \XWork\Core\ORM\DatabaseTools\Select
     */
    public function addExtraParams($extra_params) {
        $this->parts['EXTRA_PARAMS'] = $extra_params;
        return $this;
    }

    /**
     * Retorna la query parseada
     * @return String
     * @throws DatabaseException
     */
    public function getQuery() {
        $q = NULL;
        if (isset($this->parts['METHOD'])) {
            $q = $this->parts['METHOD'] . " ";
        } else {
            throw new DatabaseException("Metodo de query no encontrado");
        }
        $q .= (isset($this->parts['COLS'])) ? implode(',', $this->parts['COLS']) . " " : '*' . " ";

        if (isset($this->parts['JOIN'])) {
            $q .= $this->parts['JOIN'] . " ";
        } else {
            throw new DatabaseException("Tabla de query no encontrado");
        }
        if (isset($this->parts['JOINS'])) {
            $q .= implode(" ", $this->parts['JOINS']) . " ";
        }
        $q .= $this->parts['WHERE'];
        $q .= (isset($this->parts['EXTRA_PARAMS'])) ? $this->parts['EXTRA_PARAMS'] . " " : '';
        $q .= (isset($this->parts['GROUPBY'])) ? $this->parts['GROUPBY'] . " " : '';
        $q .= (isset($this->parts['HAVING'])) ? $this->parts['HAVING'] . " " : '';
        $q .= (isset($this->parts['ORDER'])) ? $this->parts['ORDER'] . " " : '';
        $q .= (isset($this->parts['LIMIT'])) ? $this->parts['LIMIT'] . " " : '';
        return array('QUERY' => $q, 'VALUES' => $this->values);
    }

    public function parseCols($cols) {
        if (is_array($cols)) {
            return implode(',', $cols);
        } else {
            return $cols;
        }
    }

}
