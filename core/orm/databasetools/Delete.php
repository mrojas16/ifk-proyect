<?php

namespace XWork\Core\ORM\DatabaseTools;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Delete.php
 * @desc        ORM Database Tools: Delete Parser
 * @subpackage  XWork\Core\ORM\DatabaseTools
 *
 */
class Delete {

    protected $parts = array();
    private $pk = NULL;
    private $pkValue = NULL;
    private $bypk = true;
    private $guidelines = array();
    private $values = array(1);

    public function __construct($method = NULL) {
        if ($method === NULL) {
            $this->parts['METHOD'] = 'DELETE FROM';
        }
    }


    /**
     * Asigna la tabla sobre la cual va a eliminar
     * @param String $into nombre de la clase del objeto de la tabla
     * @return \XWork\Core\ORM\DatabaseTools\Update
     */
    public function from($into) {
        $this->parts['TABLE'] = Functions::addQuotes($into);
        return $this;
    }

    /**
     * Asigna que la query de eliminacion NO se haga mediante la Primary Key
     */
    public function noPrimaryKey() {
        $this->bypk = false;
    }

    /**
     * Asigna criterio de eliminacion
     * @param arrai $values
     */
    public function setGuidelines($values) {
        if (isset($values['_queryString'])) {
            unset($values['_queryString']);
        }
        $this->guidelines = $values;
    }

    /**
     * Asigna un valor a una primary key
     * @param string $pk
     * @param mixed $value
     */
    public function setPrimaryKey($pk, $value) {
        $this->pk = $pk;
        $this->pkValue = $value;
    }

    /**
     * Asigna un limite a l consulta
     * @param int $count limite de la consulta
     * @param int $offset inicio de la consulta
     * @return \XWork\Core\ORM\DatabaseTools\Select
     */
    public function limit($count = NULL, $offset = NULL) {
        if ($count) {
            $this->parts['LIMIT'] = 'LIMIT ' . $count . ' ';
            if ($offset) {
                $this->parts['LIMIT'] = ' ,' . $offset . ' ';
            }
        } else {
            $this->parts['LIMIT'] = NULL;
        }
        return $this;
    }

    /**
     * Devuelve query parseada
     * @return String
     * @throws DatabaseException
     */
    public function getQuery() {
        $q = NULL;
        if (isset($this->parts['METHOD'])) {
            $q = $this->parts['METHOD'] . " ";
        } else {
            throw new \XWork\Excepciones\DatabaseException("Metodo de query no encontrado");
        }
        if (isset($this->parts['TABLE'])) {
            $q .= $this->parts['TABLE'] . " ";
        } else {
            throw new \XWork\Excepciones\DatabaseException("Tabla de query no encontrado");
        }
        if ($this->bypk) {
            if ($this->pk !== NULL) {
                $q .= 'WHERE ' . Functions::addQuotes(Functions::removeQuotes($this->parts['TABLE']) . '.' . Functions::removeQuotes($this->pk)) .
                        " = ?";
                $this->values[] = $this->pkValue;
            } else {
                throw new \XWork\Excepciones\DatabaseException("Condicion de query no encontrada");
            }
        } else {
            $q .= ' WHERE 1=? ';
            if (count($this->guidelines)) {
                foreach ($this->guidelines as $key => $value) {
                    $q .= 'AND ' . Functions::addQuotes(Functions::removeQuotes($this->parts['TABLE']) . '.' . Functions::removeQuotes($key)) .
                            " = ?";
                    $this->values[] = $value;
                }
            }
        }
        $q .= (isset($this->parts['LIMIT'])) ? $this->parts['LIMIT'] . " " : '';
        return array('QUERY' => $q, 'VALUES' => $this->values);
    }

}
