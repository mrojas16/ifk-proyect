<?php

namespace XWork\Core\ORM;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use XWork\Core\ORM\ORM;
use XWork\Core\Exceptions\ORMException;
use XWork\Core\Config\ORMConfig;
use \stdClass;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        ORMTools.php
 * @desc        Sistema de Herramientas para objetos ORM
 * @subpackage  XWork\Core\ORM
 *
 */
class ORMTools {
    
    public static $query = '';
    public static $values = array();

    public static function clean(&$obj, $primary = TRUE) {
        $e = ORMConfig::getUnset();
        if ($obj instanceof ORM) {
            foreach ($e as $ee) {
                unset($obj->{$ee});
            }
            if ($primary) {
                unset($obj->{$obj::$_primaryKey});
            }
        } elseif (is_array($obj)) {
            foreach ($obj as $cobj) {
                if ($cobj instanceof ORM) {
                    foreach ($e as $ee) {
                        unset($cobj->{$ee});
                    }
                    if ($primary) {
                        unset($cobj->{$cobj::$_primaryKey});
                    }
                } else {
                    throw new ORMException('El Objeto no corresponde a un objeto ORM', -322);
                }
            }
        } else {
            throw new ORMException('El Objeto no corresponde a un objeto ORM', -321);
        }
        return $obj;
    }
    
    public static function serializeLastFind() {
        $a = new stdClass();
        $a->query = self::$query;
        $a->values = self::$values;
        return $a;
    }

}
