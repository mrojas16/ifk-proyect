<?php

namespace XWork\Core\ORM;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Exceptions\ORMException;
use \XWork\Core\Error;
use \XWork\Core\Database\DatabaseProvider;
use \XWork\Core\ORM\DatabaseTools\Select;
use \XWork\Core\ORM\DatabaseTools\Insert;
use \XWork\Core\ORM\DatabaseTools\Update;
use \XWork\Core\ORM\DatabaseTools\Delete;
use \XWork\Core\ORM\DatabaseTools\TemporaryTable;
use \XWork\Core\ORM\DatabaseTools\Functions;
use XWork\Core\ORM\ORMTools;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Transaction.php
 * @desc        Class para Construir las transacciones a las bases de datos desde ORM
 * @subpackage  XWork\Core\ORM
 *
 */
class Transaction {

    private $_query;
    private $database;
    public static $autocommit = false;

    public function __construct() {
        $this->database = new DatabaseProvider();
    }

    private function __select() {
        return new Select();
    }

    private function __insert() {
        return new Insert();
    }

    private function __update() {
        return new Update();
    }

    private function __delete() {
        return new Delete();
    }

    /**
     * Cambia el status del autocommit
     * @param boolean $status setea el autocommit
     */
    public static function autoCommit($status) {
        self::$autocommit = $status;
    }

    private function _getWhere($criteria) {
        $result = array();
        foreach ($criteria as $column => $value) {
            $result["$column"] = $value;
        }

        return $result;
    }

    private function _getPrimaryKeyColumn($object) {
        $columns = $this->database->describeTable($object);

        foreach ($columns as $column) {
            if ($column['PRIMARY']) {
                return $column['COLUMN_NAME'];
            }
        }

        return null;
    }

    /**
     * Busca filas segun parametros
     * @param String $object nombre de la tabla
     * @param array $criteria criterio de búsqueda
     * @param String $order Cadena de orden de resultado
     * @param int $limit limite de la consulta
     * @param int $offset inicio de la consulta
     * @param int $extra_params Parametros extras para la consulta
     * @param String $query query resultante
     * @param String $operator operador de busqueda
     * @param boolean $md5 define si la busqueda se va a realizar por md5
     * @param boolean $temporary define si apunta a una tabla temporal
     * @return stdClass fila parseada como objeto
     */
    public function find($object, array $criteria = array(), $order = null, $limit = null, $offset = 0, $extra_params = "", &$query = NULL, $operator = '=', $md5 = false, $temporary = false) {
        try {
            $select = $this->__select()->from($object, $temporary);

            foreach ($this->_getWhere($criteria) as $where => $value) {
                $select->where($where, $value, true, $operator, $md5);
            }

            if ($order != null) {
                $select->order($order);
            }

            if ($limit !== null) {
                $select->limit($limit, $offset);
            }
            $select->addExtraParams($extra_params);
            $all = $select->getQuery();
            $query = $all['QUERY'];
            ORMTools::$query = $query;
            $values = array();
            $this->getOnlyValues($values, $all['VALUES']);
            ORMTools::$values = $values;
            
            $this->database->begin()->prepare($query)->execute($values);
            $b = $this->database->fetchObject();

            return $b;
        } catch (ORMException $e) {
            die(print_r($e));
            $this->database->rollback();
            Error::exception($e);
        }
    }

    /**
     * Busca filas segun parametros
     * @param String $object nombre de la tabla
     * @param array $criteria criterio de búsqueda
     * @param String $order Cadena de orden de resultado
     * @param int $limit limite de la consulta
     * @param int $offset inicio de la consulta
     * @param int $extra_params Parametros extras para la consulta
     * @param String $query query resultante
     * @param String $operator operador de busqueda
     * @param boolean $md5 define si la busqueda se va a realizar por md5
     * @param boolean $temporary define si apunta a una tabla temporal
     * @return stdClass fila parseada como objeto
     */
    public function findCols($object, $cols="*", array $criteria = array(), $order = null, $limit = null, $offset = 0, $extra_params = "", &$query = NULL, $operator = '=', $md5 = false, $temporary = false) {
        try {
            $select = $this->__select()->from($object, $temporary,$cols);

            foreach ($this->_getWhere($criteria) as $where => $value) {
                $select->where($where, $value, true, $operator, $md5);
            }

            if ($order != null) {
                $select->order($order);
            }

            if ($limit !== null) {
                $select->limit($limit, $offset);
            }
            $select->addExtraParams($extra_params);
            $all = $select->getQuery();
            $query = $all['QUERY'];
            $values = array();
            $this->getOnlyValues($values, $all['VALUES']);
            
            $this->database->begin()->prepare($query)->execute($values);
            $b = $this->database->fetchObject();

            return $b;
        } catch (ORMException $e) {
            die(print_r($e));
            $this->database->rollback();
            Error::exception($e);
        }
    }

    /**
     * Busca filas segun parametros y cruzandola con otras
     * @param String $object nombre de la tabla
     * @param array $joins criterio de JOINS
     * @param array $criteria criterio de búsqueda
     * @param String $order Cadena de orden de resultado
     * @param int $limit limite de la consulta
     * @param int $offset inicio de la consulta
     * @param String $query query resultante
     * @param String $operator operador de busqueda
     * @param boolean $md5 define si la busqueda se va a realizar por md5
     * @param boolean $temporary define si apunta a una tabla temporal
     * @return stdClass fila parseada como objeto
     */
    public function findJoin($object, array $joins, array $criteria = array(), $order = null, $limit = null, $offset = 0, &$query = NULL, $operator = '=', $md5 = false, $cols = array(), $temporary = false) {
        try {
            $select = $this->__select()->from($object, $temporary, $cols);

            $select->jointable($joins);

            foreach ($this->_getWhere($criteria) as $where => $value) {
                if (is_array($value)) {
                    $select->where($where, $value, true, 'IN', $md5);
                } else {
                    $select->where($where, $value, true, '=', $md5);
                }
            }

            if ($order != null) {
                $select->order($order);
            }

            if ($limit !== null) {
                $select->limit($limit, $offset);
            }
            $all = $select->getQuery();
            $query = $all['QUERY'];
            $values = array();
            $this->getOnlyValues($values, $all['VALUES']);

            $this->database->begin()->prepare($query)->execute($values); //->commit();
            $b = $this->database->fetchObject();

            return $b;
        } catch (ORMException $e) {
            $this->database->rollback();
            Error::exception($e);
        }
    }

    /**
     * Elimina los registros cunado se cumplan los criterios
     * @param String $object nombre de la tabla
     * @param array $criteria criterio de búsqueda
     * @param int $limit limite de la consulta
     * @param int $offset Inicio de la consulta
     * @param String $query query resultante
     * @param String $operator operador de busqueda
     * @param boolean $md5 define si la busqueda se va a realizar por md5
     * @return boolean
     */
    public function deleteWhere($object, array $criteria = array(), $limit = null, $offset = 0, &$query = NULL, $operator = '=', $md5 = false) {
        try {
            $delete = $this->__delete()->from($object);
            $delete->noPrimaryKey();

            $delete->setGuidelines($criteria);

            if ($limit !== null) {
                $delete->limit($limit, $offset);
            }
            $all = $delete->getQuery();
            $query = $all['QUERY'];
            $values = array();
            $this->getOnlyValues($values, $all['VALUES']);

            $this->database->begin()->prepare($query)->execute($values);
            if (self::$autocommit) {
                $this->database->commit();
            }

            return true;
        } catch (ORMException $e) {
            $this->database->rollback();
            Error::exception($e);
        }
    }

    /**
     * Cuenta el numero de filas de acuerdo a el criterio
     * @param String $object nombre de la tabla
     * @param array $criteria criterio de búsqueda
     * @return type
     */
    public function count($object, array $criteria = array()) {
        $select = $this->__select()->from($object, 'COUNT(*) AS count');

        foreach ($this->_getWhere($criteria) as $where => $value) {
            $select->where($where, $value);
        }

        $q = $select->getQuery();
        $num = $this->database->query($q);
        $t = $num->fetch_object();

        return $t->count;
    }

    /**
     * Elimina la fila del objeto específico
     * @param String $object nombre de la tabla
     * @param ORM $data objeto ORM a eliminar
     * @param String $primaryKeyColumn PK del objeto
     */
    public function delete($object, &$data, $primaryKeyColumn) {
        $values = (get_object_vars($data));
        $delete = $this->__delete();
        $delete->from(Functions::removeNamespace($object));
        if (isset($primaryKeyColumn)) {
            $delete->setPrimaryKey($primaryKeyColumn, $values[$primaryKeyColumn]);
        } else {
            $delete->noPrimaryKey();
            $delete->setGuidelines($values);
        }

        $all = $delete->getQuery();
        $query = $all['QUERY'];
        $valuesi = array();
        $this->getOnlyValues($valuesi, $all['VALUES']);
        $this->database->begin()->prepare($query)->execute($valuesi);
        if (self::$autocommit) {
            $this->database->commit();
        }
    }

    /**
     * Inserta la fila del objeto específico
     * @param String $object nombre de la tabla
     * @param ORM $data objeto ORM a agregar
     * @param String $primaryKeyColumn PK del objeto
     */
    public function add($object, &$data, $primaryKeyColumn) {
        $insert = $this->__insert();
        $insert->into($object);
        $insert->pk = $primaryKeyColumn;
        $values = get_object_vars($data);
        $insert->values($values);

        $all = $insert->getQuery();
        $query = $all['QUERY'];
        $valuesi = array();
        $this->getOnlyValues($valuesi, $all['VALUES']);

        $this->database->begin()->prepare($query)->execute($valuesi);
        $lastID = $this->database->lastId();
        if (self::$autocommit) {
            $this->database->commit();
        }

        $data->_queryString = $query;
        $data->{$primaryKeyColumn} = $lastID;
    }

    /**
     * Actualiza la fila del objeto específico
     * @param String $object nombre de la tabla
     * @param ORM $data objeto ORM a actualizar
     * @param String $primaryKeyColumn PK del objeto
     */
    public function update($object, &$data, $primaryKeyColumn) {

        $update = $this->__update();
        $update->pk = $primaryKeyColumn;
        $update->table(($object));
        $values = (get_object_vars($data));
        $update->values($values, $primaryKeyColumn);

        $all = $update->getQuery();
        $query = $all['QUERY'];
        $valuesi = array();
        $this->getOnlyValues($valuesi, $all['VALUES']);

        $this->database->begin()->prepare($query)->execute($valuesi);
        if (self::$autocommit) {
            $this->database->commit();
        }

        $data->_queryString = $query;
    }

    private function getOnlyValues(array &$r, $a) {
        foreach ($a as $k => $v) {
            if (is_array($v)) {
                $this->getOnlyValues($r, $v);
            } else {
                $r[] = $v;
            }
        }
    }

    /**
     * Elimina un objeto específico
     * @param String $object nombre de la tabla
     * @param array $data criterio de busqueda
     */
    public function remove($object, array $data) {
        $primaryKeyColumn = $this->_getPrimaryKeyColumn($object);
        $criteria = array($primaryKeyColumn => $data[$primaryKeyColumn]);
        $where = $this->_getWhere($criteria);
        $this->database->delete($object, $where);
    }

    /**
     * Comienza una Transaccion
     */
    public function beginTransaction() {
        $this->database->beginTransaction();
    }

    /**
     * Consigna una transaccion
     */
    public function commit() {
        $this->database->commit();
    }

    /**
     * Revierte una transacción
     */
    public function rollBack() {
        $this->database->rollBack();
    }

    /**
     * Obtiene la query resultante
     * @return String
     */
    public function getQuery() {
        return $this->_query;
    }

    /**
     * Crea una tabla temporal y devuelve el objeto de la misma
     * @param String $q query para crear la tabla
     * @return TemporaryTable
     * @throws ORMException
     */
    public function createTempObject($q) {
        $coincidencias = array();
        $pattern = '/^select /';
        preg_match($pattern, strtolower($q), $coincidencias, PREG_OFFSET_CAPTURE);
        if (count($coincidencias) > 0) {
            $tablename = "x" . \XWork\Core\Hash::getSimpleHash(7);

            $query = "CREATE TEMPORARY TABLE " . $tablename . " " . $q;

            $this->database->begin();
            $this->database->query($query);
            $this->database->query("DESCRIBE " . $tablename);
            $b = $this->database->fetchObject();

            $this->database->commit();
            TemporaryTable::$_name = $tablename;
            $r = new TemporaryTable();
            $r->populateTemporary($b);
            return $r;
        } else {
            throw new ORMException("Error en la query; para obtener un objeto válido, esta siempre debe ser un SELECT");
        }
    }

    /**
     * Permite ejecutar una query auxiliar
     * @param String $q query a ejecutar
     * @return boolean
     */
    public function auxQuery($q) {
        try {
            $this->database->begin();
            $this->database->query($q);
            return true;
        } catch (ORMException $e) {
            $this->database->rollback();
            Error::exception($e);
            return false;
        }
    }

}
