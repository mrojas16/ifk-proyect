<?php

namespace XWork\Core\ORM;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        OrmModifiable.php
 * @desc        OrmModifiable Interface from XWORK
 * @package     XWork\Core\ORM
 *
 */
interface ORMModifiable {
    public function add($object, array &$data);
    public function update($object, array &$data);
    public function remove($object, array $data);
}
