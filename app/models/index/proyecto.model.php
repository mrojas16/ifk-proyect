<?php

namespace XWork\App\Models\index;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Model;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        index.controller.php
 * @desc        Gestion de Usuarios con Database
 * @subpackage  XWork\App\Models\index
 *
 */
class proyecto extends Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function getVariablePrueba() {
        return 'Variable Prueba!!';
    }
    
    public function getVariableVieja() {
        return 'Sprint Prueba!!';
        
    }
    public function getVariableLink() {
        return 'Proyecto Prueba!!';
        
    }
    public function getVariableSprint() {
        return 'Sprint Prueba!!';
        
    }
    public function getVariableAdmin_Proyect() {
        return 'Sprint Prueba!!';
        
    }
    public function getCreateProyect() {
        return 'Crear Prueba!!';
        
    }
    
    
    

}
