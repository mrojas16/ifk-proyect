<?php

namespace XWork\App\Models\index\administracion;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Model;
use \XWork\Core\Database\CRUDModelInterface;
use \XWork\Core\Response;
use \XWork\Core\Session;
use \XWork\Core\ORM\ORMTools;
use \Exception;
use \XWork\ORMConnectors\ifk_proyect\ifk_botella;
use \XWork\Core\Traits\Domparse;
use \XWork\ORMConnectors\ifk_proyect\ifk_empresa;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        botella.controller.php
 * @desc        Gestion de botella con database
 * @subpackage  XWork\App\Models\index\administracion
 *
 */
class botella extends Model implements CRUDModelInterface{
    use Domparse;

    public function __construct() {
        parent::__construct();
    }

    public function getIfk_proyectIfk_botellaPkAll(){
        $obj = ifk_botella::find(array('activo'=>1));
        return $this->getSelectOptions($obj, ifk_empresa::$_primaryKey, ifk_empresa::$_primaryKey);
    }

    public function add($p) {
        try {
            $p['idcreador'] = Session::get('idusuario');
            $p['fecha_creacion'] = $this->now();
            $p['activo'] = 1;
            $new_ifk_botella = new ifk_botella($p);
            $new_ifk_botella->save();
            $new_ifk_botella->commit();
            return Response::JSONTRUE;
        } catch (Exception $exc) {
            return Response::JSONFALSE;
        }
    }

    public function delete($id) {
        try {
            $ifk_botellaClass = new ifk_botella();
            $ifk_botella = $ifk_botellaClass->findFirst(array('MD5('.ifk_botella::$_primaryKey.')' => $id));
            $ifk_botella->fecha_eliminacion = $this->now();
            $ifk_botella->ideliminador = Session::get('idusuario');
            $ifk_botella->activo = 0;
            $ifk_botella->save();
            $ifk_botella->commit();
            return Response::JSONTRUE;
        } catch (Exception $exc) {
            return Response::JSONFALSE;
        }
    }

    public function edit($p) {
        try {
            $id = $p[ifk_botella::$_primaryKey];
            $ifk_botellaClass = new ifk_botella();
            $ifk_botella = $ifk_botellaClass->findFirst(array('MD5('.ifk_botella::$_primaryKey.')' => $id));
            $ifk_botella->idempresa = $p['idempresa'];
            $ifk_botella->tamano = $p['tamano'];
            $ifk_botella->color = $p['color'];
            $ifk_botella->forma = $p['forma'];
            $ifk_botella->fecha_modificacion = $p['fecha_modificacion'];
            $ifk_botella->fecha_edicion = $this->now();
            $ifk_botella->idmodificador = Session::get('idusuario');
            $ifk_botella->save();
            $ifk_botella->commit();
            return Response::JSONTRUE;
        } catch (Exception $exc) {
            return Response::JSONFALSE;
        }
    }

    public function get($id) {
        try {
            $ifk_botellaClass = new ifk_botella();
            $data = $ifk_botellaClass->findFirst(array('MD5('.ifk_botella::$_primaryKey.')' => $id));
            ORMTools::clean($data);            return Response::JSONDATA(array('data' => $data));
        } catch (Exception $exc) {
            return Response::JSONFALSE;
        }
    }

    public function getTable() {
        $aColumns = array(ifk_botella::$_primaryKey,'idempresa','tamano','color','forma','fecha_modificacion');
        $sIndexColumn = ifk_botella::$_primaryKey;
        $sTable = "ifk_botella";
        $this->database->query("SELECT COUNT(" . $sIndexColumn . ") as num FROM $sTable WHERE activo=1");
        $resTot = $this->database->fetchObject();
        $iTotal1 = $resTot[0];
        $iTotal = $iTotal1->num;

        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . addslashes($_GET['iDisplayStart']) . ", " . addslashes($_GET['iDisplayLength']);
        }

        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . addslashes($_GET['sSortDir_' . $i]) . ", ";
                }
            }
            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }

        $sWhere = "";
        if ($_GET['sSearch'] != "") {
            $sWhere = "WHERE (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . " LIKE '%" . addslashes($_GET['sSearch']) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ") AND activo=1";
        } else {
            $sWhere = " WHERE activo=1";
        }

        for ($i = 0; $i < count($aColumns); $i++) {
            if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . addslashes($_GET['sSearch_' . $i]) . "%' ";
            }
        }

        $q = "SELECT SQL_CALC_FOUND_ROWS " . str_replace(" , ", " ", implode(", ", $aColumns)) . " FROM $sTable $sWhere $sOrder $sLimit";
        $t = $this->database->query($q);

        $l = "SELECT COUNT($sIndexColumn) AS num FROM $sTable $sWhere $sOrder";
        $n = $this->database->query($l);
        $m = $n->fetch(\PDO::FETCH_OBJ);

        $iFilteredTotal = $m->num;

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        while ($aRow = $t->fetch(\PDO::FETCH_BOTH)) {
            $row = array();
            for ($i = 1; $i < (count($aColumns)); $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }
            $row[] = '<div class="btn-group" data-dataid="' . md5($aRow[$aColumns[0]]) . '">
                        <button class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-gear"></i>  <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu fixed-dropdown-menu1">
                            <li>
                                <a class="txt-color-green" href="#" onclick="App.editar(\'' . md5($aRow[$aColumns[0]]) . '\');return false;"><i class="fa fa-edit"></i> Editar</a>
                            </li>

                            <li>
                                <a class="txt-color-red" href="#" onclick="App.eliminar(\'' . md5($aRow[$aColumns[0]]) . '\');return false;"><i class="fa fa-trash-o"></i> Eliminar</a>
                            </li>
                        </ul>
                    </div>';
            $output['aaData'][] = $row;
        }
        return json_encode($output);
    }

}
