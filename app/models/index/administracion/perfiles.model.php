<?php

namespace XWork\App\Models\index\administracion;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Model;
use \XWork\Core\Database\CRUDModelInterface;
use XWork\ORMConnectors\ifk_proyect\ifk_perfil;
use XWork\ORMConnectors\ifk_proyect\ifk_menu;
use XWork\ORMConnectors\ifk_proyect\ifk_permiso;
use \XWork\Core\Response;
use XWork\Core\ORM\ORMTools;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        perfiles.controller.php
 * @desc        Gestion de Periles con Database
 * @subpackage  XWork\App\Models\index
 *
 */
class perfiles extends Model implements CRUDModelInterface {

    public function __construct() {
        parent::__construct();
    }

    public function getPermisos($id) {
        try {
            $permisosClass = new ifk_permiso();
            $r = '';
            $permisos = $permisosClass->find(array(
                'MD5(idperfil)' => $id
            ));
            for ($i = 0; $i < count($permisos); $i++) {
                $ares = $permisos[$i];
                $r .= $ares->idmenu . ',';
            }
            $data = array('all' => trim($r, ','));
            return Response::JSONDATA($data);
        } catch (\Exception $e) {
            return Response::JSONFALSE;
        }
    }

    public function add($p) {
        try {
            $perm = $p['permisos'];
            $p['activo'] = 1;
            $newPerfil = new ifk_perfil($p);
            $newPerfil->save();
            $newPerfil->commit();
            return $this->savePermisos($newPerfil->idperfil, $perm);            
        } catch (\Exception $e) {
            return Response::JSONFALSE;
        }
    }

    public function delete($id) {
        try {
            $perfilesClass = new ifk_perfil();
            $perfil = $perfilesClass->findFirst(array(
                'MD5(idperfil)' => $id
            ));
            $perfil->activo = 0;
            $perfil->save();
            $perfil->commit();
            return Response::JSONTRUE;
        } catch (\Exception $e) {
            return Response::JSONFALSE;
        }
    }

    public function edit($p) {
        try {
            $perm = $p['permisos'];
            $perfilesClass = new ifk_perfil();
            $perfil = $perfilesClass->findFirst(array(
                'MD5(idperfil)' => $p['idperfil']
            ));
            $perfil->nombre = $p['nombre'];
            $perfil->save();
            $perfil->commit();
            return $this->savePermisos($perfil->idperfil, $perm);
        } catch (\Exception $e) {
            return Response::JSONFALSE;
        }
    }

    public function get($id) {
        try {
            $perfilesClass = new ifk_perfil();
            $data = $perfilesClass->findFirst(array(
                'MD5(idperfil)' => $id
            ));
            ORMTools::clean($data);
            return Response::JSONDATA(array('data' => $data));
        } catch (\Exception $e) {
            return Response::JSONFALSE;
        }
    }

    public function getMenu() {
        return $menu = ifk_menu::findByActivo(1, 'posicion');
    }

    public function savePermisos($perfil, $permisos) {
        try {
            $perfiles = new ifk_permiso();
            $all = $perfiles->deleteWhere(array('idperfil' => $perfil));
            $perfiles->commit();
            $p = explode(',', $permisos);
            for ($j = 0; $j < count($p); $j++) {
                $n = $p[$j];
                $data = array('idperfil' => $perfil, 'idmenu' => $n);
                $pen = new ifk_permiso($data);
                $pen->save();
                $pen->commit();
            }
            return Response::JSONTRUE;
        } catch (Exception $e) {
            return Response::JSONFALSE;
        }
    }

    public function getTable() {
        $aColumns = array('idperfil', 'nombre');
        $sIndexColumn = "idperfil";
        $sTable = "ifk_perfil";
        $this->database->query("SELECT COUNT(" . $sIndexColumn . ") as num FROM $sTable WHERE activo=1");
        $resTot = $this->database->fetchObject();
        $iTotal1 = $resTot[0];
        $iTotal = $iTotal1->num;

        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . addslashes($_GET['iDisplayStart']) . ", " .
                    addslashes($_GET['iDisplayLength']);
        }

        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
	      			" . addslashes($_GET['sSortDir_' . $i]) . ", ";
                }
            }
            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }

        $sWhere = "";
        if ($_GET['sSearch'] != "") {
            $sWhere = "WHERE (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . " LIKE '%" . addslashes($_GET['sSearch']) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ") AND activo=1";
        } else {
            $sWhere = " WHERE activo=1";
        }

        for ($i = 0; $i < count($aColumns); $i++) {
            if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . addslashes($_GET['sSearch_' . $i]) . "%' ";
            }
        }

        $q = "
            SELECT SQL_CALC_FOUND_ROWS " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
            FROM   $sTable
            $sWhere 
            $sOrder
            $sLimit
            ";
        $t = $this->database->query($q);

        $l = "SELECT COUNT($sIndexColumn) AS num FROM   $sTable
		      	$sWhere 
		      	$sOrder";
        $n = $this->database->query($l);
        $m = $n->fetch(\PDO::FETCH_OBJ);

        $iFilteredTotal = $m->num;

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        while ($aRow = $t->fetch(\PDO::FETCH_BOTH)) {

            $row = array();
            for ($i = 1; $i < (count($aColumns)); $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }

            $row[] = '<div class="btn-group" data-dataid="' . md5($aRow[$aColumns[0]]) . '">
                        <button class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-gear"></i>  <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu fixed-dropdown-menu1">
                            <li>
                                <a class="txt-color-green" href="#" onclick="App.editar(\'' . md5($aRow[$aColumns[0]]) . '\');return false;"><i class="fa fa-edit"></i> Editar</a>
                            </li>

                            <li>
                                <a class="txt-color-red" href="#" onclick="App.eliminar(\'' . md5($aRow[$aColumns[0]]) . '\');return false;"><i class="fa fa-trash-o"></i> Eliminar</a>
                            </li>
                        </ul>
                      </div>';
            $output['aaData'][] = $row;
        }


        return json_encode($output);
    }

}
