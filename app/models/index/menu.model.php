<?php

namespace XWork\App\Models\index;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use XWork\ORMConnectors\ifk_proyect\ifk_menu;
use \XWork\Core\Session;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        menu.model.php
 * @desc        Modelo para Menu
 * @subpackage  XWork\App\Models\index
 *
 */
class menu extends \XWork\Core\Model{
    
    public function __construct() {
        parent::__construct();
    }

    public function getAllMenu() {
        $menu = ifk_menu::findIn(array('idmenu' => Session::get('permisos'),'activo' => 1), 'posicion ASC, idmenu ASC');
        return $menu;
    }
}
