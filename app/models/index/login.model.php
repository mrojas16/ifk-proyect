<?php

namespace XWork\App\Models\index;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

//use XWork\ORMConnectors\fbr_acopio\fbr_usuario;
use XWork\ORMConnectors\ifk_proyect\ifk_permiso;
use XWork\ORMConnectors\ifk_proyect\ifk_perfil;

use \XWork\Core\Model;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        login.model.php
 * @desc        Modelo para login
 * @subpackage  XWork\App\Models\index
 *
 */
class login extends Model {

    public $usuario;
    public $new_pass;

    public function __construct() {
        parent::__construct();
    }

    public function getLogin($mail, $pass) {
        $pass = md5($pass);
        $login = \XWork\ORMConnectors\ifk_proyect\ifk_usuario::findFirst(array('email' => $mail, 'pass' => $pass, 'activo' => 1));
        if (is_object($login)) {
            $this->usuario = $login;
            $this->usuario->profile_name = $this->getProfileName($this->usuario->idperfil);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getAllPermisos($idusuario) {
        $r = array();
        $permisos = ifk_permiso::find(array('idperfil' => $idusuario));
        for ($i = 0; $i < count($permisos); $i++) {
            $r[] = $permisos[$i]->idmenu;
        }
        return $r;
    }

    public function getAllAcopios($idusuario) {
//        $p = array();
//        $r = array();
//        $perm = fbr_usuario_acopio::find(array('idusuario' => $idusuario));
//        for ($i = 0; $i < count($perm); $i++) {
//            $p[] = $perm[$i]->idacopio;
//        }
//        $permisos = fbr_acopio::find(array('idacopio'=>$p));
//        for ($i = 0; $i < count($permisos); $i++) {
//            $r[] = array('id'=>$permisos[$i]->idacopio,'desc'=>$permisos[$i]->descripcion);
//        }
//        return $r;
    }

    public function getAllTemporadas() {
//        $r = array();
//        $permisos = fbr_temporada::find(array('activo' => 1, 'vigente'=>1));
//        for ($i = 0; $i < count($permisos); $i++) {
//            $r[] = array('id'=>$permisos[$i]->idtemporada,'desc'=>$permisos[$i]->descripcion);
//        }
//        return $r;
    }

    public function recover($mail) {
        try {
            $u = \XWork\ORMConnectors\ifk_proyect\ifk_usuario::findFirstByEmailAndActivo($mail, 1);
            if (is_object($u)) {
                $str = $this->loadHelper('str');
                $new_pass = $str->generateString();
                $this->new_pass = $new_pass;
                $u->pass = md5($new_pass);
                try {
                    $u->save();
                    return TRUE;
                } catch (\Exception $e) {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } catch (\Exception $e) {
            print_r($e);
            return false;
        }
    }

    public function getProfileName($id) {
        return ifk_perfil::findFirst(array('idperfil' => $id))->nombre;
    }

}
