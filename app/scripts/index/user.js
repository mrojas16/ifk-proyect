App.xDom.init();
$("#primal-form").validate({
    rules: {
        email: {
            required: true,
            email: true,
            noRepeat: true
        },
        pass: {
            required: function () {
                return parseInt($("#idusuario").val()) === 0;
            }
        },
        rpass: {
            required: function () {
                return parseInt($("#idusuario").val()) === 0;
            },
            equalTo: "[name=pass]"
        },
        nombre: {
            required: true
        },
        apellido: {
            required: true
        },
        rut: {
            required: true,
            rut: true,
            noRepeat: true
        }
    },
    messages: {
        email: {
            required: "Debe ingresar un email",
            email: "Debe ingresar un email válido",
            noRepeat: "El email ingresado ya está registrado"
        },
        pass: {
            required: "Debe ingresar una contraseña"
        },
        rpass: {
            required: "Debe repetir la contraseña",
            equalTo: "Las contraseñas no coinciden"
        },
        nombre: {
            required: "Debe ingresar un nombre"
        },
        apellido: {
            required: "Debe ingresar un apellido"
        },
        rut: {
            required: "Debe ingresar un RUT",
            rut: "Debe ingresar un RUT válido",
            noRepeat: "El RUT ingresado ya está registrado"
        }

    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (form) {
        var dataForm = $(form).serialize();
        var idpk = parseInt($(".pk_form").val());
        if (idpk === 0) {
            $.gritter.add({
                title: "Error",
                text: "<i class='fa fa-clock-o'></i> <i>No se ha podido editar correctamente sus datos; intentelo mas tarde</i>",
                class_name: 'growl-danger',
                sticky: false,
                time: 2000
            });
        } else {
            blackout.show("Guardando...", function () {
                $.ajax({
                    type: "POST",
                    url: "[:__BASE_URL:]index/user/edit_self",
                    data: dataForm
                }).done(function (data) {
                    try {
                        var r = JSON.parse(data);
                        setTimeout(function () {
                            if (r.response === JSONTRUE) {
                                $(".user_name_i").text(r.data.nombre);
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "OK!",
                                        text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                        class_name: 'growl-success',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        }, 1);
                    } catch (e) {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                }).error(function () {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                });
            });
        }
    }
});