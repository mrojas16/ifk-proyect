App.xDom.init({
    "onOpenFormTitleAdd": "Agregar Empresa"
});

App.xDom.dataTable('[:__BASE_URL:]index.administracion/empresas/get_table');

$("#primal-form").validate({
    rules: {
        rut: {
            required: true,
            rut: true,
            noRepeat: true
        },
        codigo: {
            required: true
        },
        nombre: {
            required: true
        },
        razon_social: {
            required: true
        },
        representante_legal: {
            required: true
        },
        giro: {
            required: true
        },
        direccion: {
            required: true
        },
        telefono: {
            required: true
        }
    },
    messages: {
        rut: {
            required: "Debe ingresar el Rut de la Empresa",
            rut: "Debe ingresar un Rut de Empresa válido",
            noRepeat: "El Rut de Empresa ingresado ya está registrado"
        },
        codigo: {
            required: "Debe ingresar un Codigo"
        },
        nombre_fantasia: {
            required: "Debe ingresar el Nombre de la Empresa"
        },
        razon_social: {
            required: "Debe ingresar una Razón Social"
        },
        representante_legal: {
            required: "Debe ingresar un Representante Legal"
        },
        giro: {
            required: "Debe ingresar un Giro"
        },
        direccion: {
            required: "Debe ingresar una Direccion"
        },
        telefono: {
            required: "Debe ingresar un Telefono"
        }
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (form) {
        var dataForm = $(form).serialize();
        var idpk = parseInt($(".pk_form").val());
        if (idpk === 0) {
            blackout.show("Guardando...", function () {
                $.ajax({
                    type: "POST",
                    url: "[:__BASE_URL:]index.administracion/empresas/add",
                    data: dataForm
                }).done(function (data) {
                    setTimeout(function () {
                        try {
                            var r = JSON.parse(data);
                            if (r.response === JSONTRUE) {
                                App.oTable.fnDraw();
                                blackout.hide(function () {
                                    App.xDom.closeForm();
                                    $.gritter.add({
                                        title: "OK!",
                                        text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                        class_name: 'growl-success',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        } catch (e) {
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "Error",
                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                    class_name: 'growl-danger',
                                    sticky: false,
                                    time: 2000
                                });
                            });
                        }
                    }, 1);
                }).error(function () {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                });
            });
        } else {
            blackout.show("Guardando...", function () {
                $.ajax({
                    type: "POST",
                    url: "[:__BASE_URL:]index.administracion/empresas/edit",
                    data: dataForm
                }).done(function (data) {
                    try {
                        var r = JSON.parse(data);
                        setTimeout(function () {
                            if (r.response === JSONTRUE) {
                                blackout.hide(function () {
                                    App.oTable.fnDraw();
                                    App.xDom.closeForm();
                                    $.gritter.add({
                                        title: "OK!",
                                        text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                        class_name: 'growl-success',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                                $(".pk_form").val(0);
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        }, 1);
                    } catch (e) {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                }).error(function () {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                });
            });
        }
    }
});

App.editar = function (id) {
    App.xDom.closeForm(function () {
        blackout.show('Espere...', function () {
            $.ajax({
                type: "POST",
                url: "[:__BASE_URL:]index.administracion/empresas/get",
                data: "id=" + id
            }).done(function (data) {
                try {
                    var arr_data = JSON.parse(data);
                    var all = arr_data.data.data;
                    for (var i in all) {
                        $("[name=" + i + "]").val(all[i]).trigger("chosen:updated");
                    }
                    $(".only_on_edit").show();
                    $('.form-title').html("Editar Empresa");
                    $("#idempresa").val(id);
                    blackout.hide(function () {
                        $("[data-maincontainerform]").slideDown();
                    });
                } catch (e) {
                    console.log(e);
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    });
                }
            }).error(function () {
                blackout.hide(function () {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 4000
                    });
                });
            });
        });
    });
};

App.eliminar = function (id) {
    App.xDom.closeForm(function () {
        xmodal.show(null, "¿Esta seguro que desea eliminar esta Empresa?", function () {
            $.ajax({
                type: "POST",
                url: "[:__BASE_URL:]index.administracion/empresas/delete",
                data: "id=" + id
            }).done(function (data) {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        App.oTable.fnDraw();
                        $.gritter.add({
                            title: "OK!",
                            text: "<i class='fa fa-clock-o'></i> <i>Se han eliminado correctamente la empresa</i>",
                            class_name: 'growl-success',
                            sticky: false,
                            time: 2000
                        });
                    } else {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando la empresa</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    }
                    xmodal.hide();
                } catch (e) {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando la empresa</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                    xmodal.hide();
                }
            }).error(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando la empresa</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
                xmodal.hide();
            });
        });
    });
};

