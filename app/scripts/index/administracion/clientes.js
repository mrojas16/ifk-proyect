App.xDom.init({
    "onOpenFormTitleAdd": "Agregar Cliente"
});
App.xDom.dataTable("[:__BASE_URL:]index.administracion/clientes/get_table");

$("#primal-form").validate({
    rules: {
        email: {
            required: true,
            email: true,
            noRepeat: true
        },
        nombre: {
            required: true
        },
        apellido: {
            required: true
        },
        rut_empresa: {
            required: true,
            rut: true,
            noRepeat: true
        }

    },
    messages: {
        email: {
            required: "Debe ingresar un email",
            email: "Debe ingresar un email válido",
            noRepeat: "El email ingresado ya está registrado"
        },
        nombre: {
            required: "Debe ingresar un nombre"
        },
        apellido: {
            required: "Debe ingresar un apellido"
        },
        rut_empresa: {
            required: "Debe ingresar un RUT de la Empresa",
            rut: "Debe ingresar un RUT de Empresa válido",
            noRepeat: "El RUT de Empresa ingresado ya está registrado"
        }
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (form) {
        var dataForm = $(form).serialize();
        var idpk = parseInt($(".pk_form").val());
        if (idpk === 0) {
            blackout.show("Guardando...", function () {
                $.ajax({
                    type: "POST",
                    url: "[:__BASE_URL:]index.administracion/clientes/add",
                    data: dataForm
                }).done(function (data) {
                    setTimeout(function () {
                        try {
                            var r = JSON.parse(data);
                            if (r.response === JSONTRUE) {
                                App.oTable.fnDraw();
                                blackout.hide(function () {
                                    App.xDom.closeForm();
                                    $.gritter.add({
                                        title: "OK!",
                                        text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                        class_name: 'growl-success',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        } catch (e) {
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "Error",
                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                    class_name: 'growl-danger',
                                    sticky: false,
                                    time: 2000
                                });
                            });
                        }
                    }, 1);
                }).error(function () {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                });
            });
        } else {
            blackout.show("Guardando...", function () {
                $.ajax({
                    type: "POST",
                    url: "[:__BASE_URL:]index.administracion/clientes/edit",
                    data: dataForm
                }).done(function (data) {
                    try {
                        var r = JSON.parse(data);
                        setTimeout(function () {
                            if (r.response === JSONTRUE) {
                                blackout.hide(function () {
                                    App.oTable.fnDraw();
                                    App.xDom.closeForm();
                                    $.gritter.add({
                                        title: "OK!",
                                        text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                        class_name: 'growl-success',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                                $(".pk_form").val(0);
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        }, 1);
                    } catch (e) {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                }).error(function () {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                });
            });
        }
    }
});


App.editar = function (id) {
    App.xDom.closeForm(function () {
        blackout.show('Espere...', function () {
            $.ajax({
                type: "POST",
                url: "[:__BASE_URL:]index.administracion/clientes/get",
                data: "id=" + id
            }).done(function (data) {
                try {
                    var arr_data = JSON.parse(data);
                    var all = arr_data.data.data;
                    for (var i in all) {
                        $("[name=" + i + "]").val(all[i]).trigger("chosen:updated");
                    }
                    $(".only_on_edit").show();
                    $('.form-title').html("Editar Cliente");
                    $("#idcliente").val(id);
                    blackout.hide(function () {
                        $("[data-maincontainerform]").slideDown();
                    });
                } catch (e) {
                    console.log(e);
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    });
                }
            }).error(function () {
                blackout.hide(function () {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 4000
                    });
                });
            });
        });
    });
};

App.eliminar = function (id) {
    App.xDom.closeForm(function () {
        xmodal.show(null, "¿Esta seguro que desea eliminar este Cliente?", function () {
            $.ajax({
                type: "POST",
                url: "[:__BASE_URL:]index.administracion/clientes/delete",
                data: "id=" + id
            }).done(function (data) {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        App.oTable.fnDraw();
                        $.gritter.add({
                            title: "OK!",
                            text: "<i class='fa fa-clock-o'></i> <i>Se han eliminado correctamente el cliente</i>",
                            class_name: 'growl-success',
                            sticky: false,
                            time: 2000
                        });
                    } else {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando el cliente</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    }
                    xmodal.hide();
                } catch (e) {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando el cliente</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                    xmodal.hide();
                }
            }).error(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando el valor</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
                xmodal.hide();
            });
        });
    });
};
