App.xDom.init({
    "onOpenFormTitleAdd": "Agregar Perfil"
});

$('#tree').checktree();
App.xDom.dataTable("[:__BASE_URL:]index.administracion/perfiles/get_table");

$("#primal-form").validate({
    rules: {
        nombre: {
            required: true,
            noRepeat: true
        }
    },
    messages: {
        nombre: {
            required: "Debe ingresar un nombre",
            noRepeat: "El nombre ingresado ya está registrado"
        }
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (form) {
        $("#permisos").val(tree.get());
        var dataForm = $(form).serialize();
        var idpk = parseInt($(".pk_form").val());
        if (idpk === 0) {
            blackout.show("Guardando...", function () {
                $.ajax({
                    type: "POST",
                    url: "[:__BASE_URL:]index.administracion/perfiles/add",
                    data: dataForm
                }).done(function (data) {
                    setTimeout(function () {
                        try {
                            var r = JSON.parse(data);
                            if (r.response === JSONTRUE) {
                                App.oTable.fnDraw();
                                blackout.hide(function () {
                                    App.xDom.closeForm();
                                    $.gritter.add({
                                        title: "OK!",
                                        text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                        class_name: 'growl-success',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        } catch (e) {
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "Error",
                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                    class_name: 'growl-danger',
                                    sticky: false,
                                    time: 2000
                                });
                            });
                        }
                    }, 1);
                }).error(function () {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                });
            });
        } else {
            blackout.show("Guardando...", function () {
                $.ajax({
                    type: "POST",
                    url: "[:__BASE_URL:]index.administracion/perfiles/edit",
                    data: dataForm
                }).done(function (data) {
                    try {
                        var r = JSON.parse(data);
                        setTimeout(function () {
                            if (r.response === JSONTRUE) {
                                blackout.hide(function () {
                                    App.oTable.fnDraw();
                                    App.xDom.closeForm();
                                    $.gritter.add({
                                        title: "OK!",
                                        text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                        class_name: 'growl-success',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                                $("#idperfil").val(0);
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        }, 1);
                    } catch (e) {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                }).error(function () {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                });
            });
        }
    }
});


App.editar = function (id) {
    App.xDom.closeForm(function () {
        tree.clean();
        blackout.show('Espere...', function () {
            $.ajax({
                type: "POST",
                url: "[:__BASE_URL:]index.administracion/perfiles/get",
                data: "id=" + id
            }).done(function (data) {
                try {
                    var arr_data = JSON.parse(data);
                    var all = arr_data.data.data;
                    for (var i in all) {
                        $("[name=" + i + "]").val(all[i]).trigger("chosen:updated");
                    }
                    $('.form-title').html("Editar Perfil");
                    $(".pk_form").val(id);

                    $.ajax({
                        type: "POST",
                        url: "[:__BASE_URL:]index.administracion/perfiles/get_permisos/" + id,
                        data: "t=" + id
                    }).done(function (data) {
                        try {
                            var arr_data = JSON.parse(data);
                            tree.checkall(arr_data.data.all);
                            blackout.hide(function () {
                                $("[data-maincontainerform]").slideDown();
                            });
                        } catch (e) {
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "Error",
                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                                    class_name: 'growl-danger',
                                    sticky: false,
                                    time: 2000
                                });
                            });
                        }
                    }).error(function () {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 4000
                            });
                        });
                    });
                } catch (e) {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    });
                }
            }).error(function () {
                blackout.hide(function () {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 4000
                    });
                });
            });
        });
    });
};

App.eliminar = function (id) {
    App.xDom.closeForm(function () {
        xmodal.show(null, "¿Esta seguro que desea eliminar este Perfil?", function () {
            $.ajax({
                type: "POST",
                url: "[:__BASE_URL:]index.administracion/perfiles/delete",
                data: "id=" + id
            }).done(function (data) {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        App.oTable.fnDraw();
                        $.gritter.add({
                            title: "OK!",
                            text: "<i class='fa fa-clock-o'></i> <i>Se han eliminado correctamente el Perfil</i>",
                            class_name: 'growl-success',
                            sticky: false,
                            time: 2000
                        });
                    } else {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando el Perfil</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    }
                    xmodal.hide();
                } catch (e) {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando el Perfil</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                    xmodal.hide();
                }
            }).error(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando el Perfil</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
                xmodal.hide();
            });
        });
    });
};

var tree = function () {
    var init = function (action, id) {
        id || (id = 0);
        var $tree = $("#tree");
        if (action === 'clean') {
            $tree.find('input[type=checkbox]').prop('checked', false);
        } else if (action === 'check') {
            $tree.find('input[type=checkbox][value="' + id + '"]').prop('checked', true);
        } else if (action === 'get') {
            var a = "";
            $tree.find('input[type=checkbox]').each(function (i, e) {
                if ($(this).is(':checked')) {
                    a += $(this).val() + ',';
                }
            });
            return a.substring(0, a.length - 1);
        } else if (action === 'checkall') {
            if (typeof id === "string" && id !== '') {
                var all = id.split(',');
                for (var i = 0; i < all.length; i++) {
                    $tree.find('input[type=checkbox][value="' + all[i] + '"]').prop('checked', true);
                }
            }
        }
    };
    return {
        clean: function () {
            init('clean');
        },
        check: function (id) {
            id || (id = 0);
            init('check', id);
        },
        get: function () {
            return init('get');
        },
        checkall: function (a) {
            a || (a = '')
            init('checkall', a);
        }
    };
}();