/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Botellas.js
 * @desc        Gestion de Botellas, formularios, tablas y animaciones
 *
 */

App.xDom.init({
    "onOpenFormTitleAdd": "Agregar Botellas",
    "onOpenFormTitleEdit": "Editar Botellas",
    "prompDeleteMessage": "¿Esta seguro que desea eliminar este Botellas?",
    "deleteMessageSuccess": "Se han eliminado correctamente el Botellas",
    "deleteMessageFail": "Ha Ocurrido un error eliminando el Botellas"

});
App.xDom.dataTable("[:__BASE_URL:]index.administracion/botellas/get_table");

$("#primal-form").validate({
    rules: {
        idempresa: {
            required: true
        },
        tamano: {
            required: true
        },
        color: {
            required: true
        },
        forma: {
            required: true
        },
        fecha_modificacion: {
            required: true
        },
    },
    messages: {
        idempresa: {
            required: "Debe ingresar un(a) idempresa"
        },
        tamano: {
            required: "Debe ingresar un(a) tamano"
        },
        color: {
            required: "Debe ingresar un(a) color"
        },
        forma: {
            required: "Debe ingresar un(a) forma"
        },
        fecha_modificacion: {
            required: "Debe ingresar un(a) fecha_modificacion"
        },
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (form) {
        var dataForm = $(form).serialize();
        var idpk = parseInt($(".pk_form").val());
        if (idpk === 0) {
            blackout.show("Guardando...", function () {
                $.ajax({
                    type: "POST",
                    url: "[:__BASE_URL:]index.administracion/botellas/add",
                    data: dataForm
                }).done(function (data) {
                    setTimeout(function () {
                        try {
                            var r = JSON.parse(data);
                            if (r.response === JSONTRUE) {
                                App.oTable.fnDraw();
                                blackout.hide(function () {
                                    App.xDom.closeForm();
                                    $.gritter.add({
                                        title: "OK!",
                                        text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                        class_name: 'growl-success',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        } catch (e) {
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "Error",
                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                    class_name: 'growl-danger',
                                    sticky: false,
                                    time: 2000
                                });
                            });
                        }
                    }, 1);
                }).error(function () {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                });
            });
        } else {
            blackout.show("Guardando...", function () {
                $.ajax({
                    type: "POST",
                    url: "[:__BASE_URL:]index.administracion/botellas/edit",
                    data: dataForm
                }).done(function (data) {
                    try {
                        var r = JSON.parse(data);
                        setTimeout(function () {
                            if (r.response === JSONTRUE) {
                                blackout.hide(function () {
                                    App.oTable.fnDraw();
                                    App.xDom.closeForm();
                                    $.gritter.add({
                                        title: "OK!",
                                        text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                        class_name: 'growl-success',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        }, 1);
                        $(".pk_form").val(0);
                    } catch (e) {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                }).error(function () {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                });
            });
        }
    }
});

App.editar = function (idpk) {
    App.xDom.editStandard({
        id: idpk,
        url: "[:__BASE_URL:]index.administracion/botellas/get"
    });
};

App.eliminar = function (idpk) {
    App.xDom.deleteStandard({
        id: idpk,
        url: "[:__BASE_URL:]index.administracion/botellas/delete"
    });
};

