$(window).load(function() {
    $('#status').fadeOut();
    $('#preloader').delay(350).fadeOut(function() {
        $('body').delay(350).css({'overflow': 'visible'});
    });
});

$("#login").bind('submit', function(e) {
    e.preventDefault();
    var dataForm = "email=" + document.getElementsByName('email')[0].value + "&password=" + document.getElementsByName('password')[0].value;
    blackout.show('Espere...', function() {
        $.ajax({
            type: "POST",
            url: "[:__BASE_URL:]index/login/login",
            data: dataForm
        }).done(function(response) {
            try {
                var r = JSON.parse(response);
                if (r.response === 1) {
                    var h = (window.location.hash==="")?'#index/dashboard':window.location.hash;
                    document.location = '[:__BASE_URL:]' + h;
                } else {
                    blackout.hide(function() {
                        loginMSG.show("Error Iniciando sesion <b>Verifique su Email y Contraseña</b>.");
                    });
                }
            } catch (e) {
                blackout.hide(function() {
                    loginMSG.show("Error Iniciando sesion.");
                });
            }
        });
    });
});

$("#recover").bind('submit', function(e) {
    e.preventDefault();
    var dataForm = "email=" + document.getElementsByName('emailr')[0].value;
    blackout.show('Espere...', function() {
        $.ajax({
            type: "POST",
            url: "[:__BASE_URL:]index/login/recover",
            data: dataForm
        }).done(function(response) {
            try {
                var r = JSON.parse(response);
                if (r.response === 1) {
                    blackout.hide(function() {
                        $(".mainrecoverContainer").slideToggle(function() {
                            $(".mainloginContainer").slideToggle(function() {
                                loginMSG.show("Se ha Enviado un correo con instrucciones para recuperar su contraseña!",3000,"alert-success");
                                document.getElementsByName('emailr')[0].value = '';
                            });
                        });
                    });
                } else {
                    blackout.hide(function() {
                        loginMSG.show("Error Recuperando su contraseña <b>No sa ha encontrado su email</b>.");
                    });
                }
            } catch (e) {
                blackout.hide(function() {
                    loginMSG.show("Error Recuperando su contraseña.");
                });
            }
        });
    });
});


$(".recoverPass").bind('click', function(e) {
    e.preventDefault();
    $(".mainloginContainer").slideUp(function() {
        $(".mainrecoverContainer").slideDown();
    });
});

$(".backLogin").bind('click', function(e) {
    e.preventDefault();
    $(".mainrecoverContainer").slideUp(function() {
        $(".mainloginContainer").slideDown();
    });
});

var loginMSG = function() {
    return {
        show: function(mensaje, delay, class_i, callback) {
            mensaje || (mensaje = 'Error desconocido');
            if (typeof delay !== "number") {
                delay = 2000;
            }
            if (typeof callback !== "function") {
                callback = function() {
                };
            }
            class_i || (class_i = 'alert-danger');
            if (typeof class_i !== "string") {
                class_i = "alert-danger";
            }
            var $t = $('.mainErrorLogin');
            if (typeof mensaje === "string") {
                $t.html(mensaje).addClass(class_i).fadeIn(function() {
                    callback();
                    setTimeout(function() {
                        $t.fadeOut();
                    }, delay);
                });
            } else if (typeof mensaje === "function") {
                $t.html("Error desconocido").addClass(class_i).fadeIn(function() {
                    mensaje();
                    setTimeout(function() {
                        $t.fadeOut();
                    }, delay);
                });
            } else {
                $t.html("Error desconocido").addClass(class_i).fadeIn(function() {
                    setTimeout(function() {
                        $t.fadeOut();
                    }, delay);
                });
            }
        },
        hide: function(callback) {
            if (typeof callback !== "function") {
                callback = function() {
                };
            }
            var $t = $('.mainErrorLogin');
            if (typeof callback === "function") {
                $t.fadeOut(function() {
                    callback();
                });
            } else {
                $t.fadeOut();
            }
        }
    };
}();