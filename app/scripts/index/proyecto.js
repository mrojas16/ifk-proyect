
App.proyectEngine = function () {
    this.mainContainer = $('#proyectContainerMenu');
    this.mainTarget = $('#proyectContainer');
    this.proyectLink = this.mainContainer.find('[data-action="proyect_link"]');
    this.proyectAll = this.mainContainer.find('[data-action="proyect_all"]');
    this.adminProyect = this.mainContainer.find('[data-action="admin_proyect"]');
    this.sprintLink = this.mainContainer.find('[data-action="sprint_link"]');
    this.sprintAll = this.mainContainer.find('[data-action="sprint_all"]');
    this.adminSprint = this.mainContainer.find('[data-action="admin_sprint"]');
    this.allTypes = this.mainContainer.find('[data-action]');
};

App.proyectEngine.prototype.run = function () {
    this.allTypes.bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        App.PE.runAction(this, $(this).attr('data-action'));
    });
};

App.proyectEngine.prototype.runAction = function (obj, action) {
    switch (action) {
        case 'proyect_all':
            this.getAllProyects(obj);
            break;
        case 'create_proyect':
            this.getCreateProyect(obj);
            break;
        case 'proyect_link':
            this.getAllProyectlinks(obj);
            break;
        case 'admin_proyect':
            if ($(obj).attr('href') === "admin_proyect") {
                this.adminAllProyects(obj);
            } else if ($(obj).attr('href') === "create_proyect") {
                this.createProyect(obj);
            }
            break;
        case 'sprint_link':
            this.getAllSprintlinks(obj, $(obj).attr('href'));
            break;
        case 'sprint_all':
            this.getAllSprints(obj);
            break;
        case 'admin_sprint':
            if ($(obj).attr('href') === "admin_sprint") {
                this.adminAllSprints(obj);
            } else if ($(obj).attr('href') === "create_sprint") {
                this.createSprint(obj);
            }
            break;
    }
    return false;
};

App.proyectEngine.prototype.getCreateProyect = function (obj) {
    obj.preventExtensions();
    $('#myModal4').show();
//    alert(x);
//    setTimeout(function () {
//        $.ajax({
//            type: "POST",
//            url: "[:__BASE_URL:]index/proyecto/proyect_all",
//            data: "_y=true"
//        }).done(function (data) {
//            setTimeout(function () {
//                try {
//                    var r = JSON.parse(data);
//                    if (r.response === JSONTRUE) {
//                        App.PE.mainTarget.html(r.data.body);
//                        var $o = $(obj);
//                        $o.closest('.dropdown').find('.dropdown-toggle').click();
//                        $('[name="f_entrega"]').datepicker({
//                            dateFormat: 'dd-mm-yy',
//                            prevText: '<i class="fa fa-chevron-left"></i>',
//                            nextText: '<i class="fa fa-chevron-right"></i>'
//                        });
//                    } else {
//                        App.PE.closeLoading(function () {
//                            $.gritter.add({
//                                title: "Error",
//                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
//                                class_name: 'growl-danger',
//                                sticky: false,
//                                time: 2000
//                            });
//                        });
//                    }
//                } catch (e) {
//                    App.PE.closeLoading(function () {
//                        $.gritter.add({
//                            title: "Error",
//                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
//                            class_name: 'growl-danger',
//                            sticky: false,
//                            time: 2000
//                        });
//                    });
//                }
//            }, 1);
//        }).error(function () {
//            App.PE.closeLoading(function () {
//                $.gritter.add({
//                    title: "Error",
//                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
//                    class_name: 'growl-danger',
//                    sticky: false,
//                    time: 4000
//                });
//            });
//        });
//    }, 1);
};
App.proyectEngine.prototype.getAllProyects = function (obj) {
    setTimeout(function () {
        $.ajax({
            type: "POST",
            url: "[:__BASE_URL:]index/proyecto/proyect_all",
            data: "_y=true"
        }).done(function (data) {
            setTimeout(function () {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        App.PE.mainTarget.html(r.data.body);
                        var $o = $(obj);
                        $o.closest('.dropdown').find('.dropdown-toggle').click();
                        $('[name="f_entrega"]').datepicker({
                            dateFormat: 'dd-mm-yy',
                            prevText: '<i class="fa fa-chevron-left"></i>',
                            nextText: '<i class="fa fa-chevron-right"></i>'
                        });
                    } else {
                        App.PE.closeLoading(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                } catch (e) {
                    App.PE.closeLoading(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    });
                }
            }, 1);
        }).error(function () {
            App.PE.closeLoading(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 4000
                });
            });
        });
    }, 1);
};

App.proyectEngine.prototype.getAllSprints = function (obj) {
    setTimeout(function () {
        $.ajax({
            type: "POST",
            url: "[:__BASE_URL:]index/proyecto/sprint_all",
            data: "_y=true"
        }).done(function (data) {
            setTimeout(function () {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        App.PE.mainTarget.html(r.data.body);
                        var $o = $(obj);
                        $o.closest('.dropdown').find('.dropdown-toggle').click();
                        $('[name=f_entrega]').datepicker({
                            dateFormat: 'dd-mm-yy',
                            prevText: '<i class="fa fa-chevron-left"></i>',
                            nextText: '<i class="fa fa-chevron-right"></i>'
                        });
                    } else {
                        App.PE.closeLoading(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                } catch (e) {
                    App.PE.closeLoading(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    });
                }
            }, 1);
        }).error(function () {
            App.PE.closeLoading(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 4000
                });
            });
        });
    }, 1);
};

App.proyectEngine.prototype.getAllProyectlinks = function (obj) {
    setTimeout(function () {
        $.ajax({
            type: "POST",
            url: "[:__BASE_URL:]index/proyecto/proyect_link",
            data: "_y=true"
        }).done(function (data) {
            setTimeout(function () {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        App.PE.mainTarget.html(r.data.body);
                        var $o = $(obj);
                        $o.closest('.dropdown').find('.dropdown-toggle').click();
                        $('#project_name').append($o.text());
                    } else {
                        App.PE.closeLoading(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                } catch (e) {
                    App.PE.closeLoading(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    });
                }
            }, 1);
        }).error(function () {
            App.PE.closeLoading(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 4000
                });
            });
        });
    }, 1);
};

App.proyectEngine.prototype.getAllSprintlinks = function (obj) {
    setTimeout(function () {
        $.ajax({
            type: "POST",
            url: "[:__BASE_URL:]index/proyecto/sprint_link",
            data: "_y=true"
        }).done(function (data) {
            setTimeout(function () {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        App.PE.mainTarget.html(r.data.body);
                        var $o = $(obj);
                        $o.closest('.dropdown').find('.dropdown-toggle').click();
                        $('#sprint_name').append($o.text());
                    } else {
                        App.PE.closeLoading(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                } catch (e) {
                    App.PE.closeLoading(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    });
                }
            }, 1);
        }).error(function () {
            App.PE.closeLoading(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido cargando, intentelo mas tarde</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 4000
                });
            });
        });
    }, 1);
};

App.proyectEngine.prototype.closeLoading = function (callback) {
    callback();
};


App.PE = new App.proyectEngine();
App.PE.run();