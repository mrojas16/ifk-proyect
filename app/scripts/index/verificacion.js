/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Verificacion.js
 * @desc        verificacion de folios
 *
 */

App.xDom.init({
    "onOpenFormTitleAdd": "Verificar Folio"
});

$("#primal-form").validate({
    rules: {
        folio: {
            required: true,
            folioLenght: true
        }
    },
    messages: {
        folio: {
            required: "Debe ingresar un folio",
            folioLenght: "El Largo No es Válido"
        }
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (form) {
        var dataForm = $(form).serialize();
        blackout.show("Cargando...", function () {
            $.ajax({
                type: "POST",
                url: "[:__BASE_URL:]index/verificacion/get",
                data: dataForm
            }).done(function (data) {
                setTimeout(function () {
                    try {
                        var r = JSON.parse(data);
                        if (r.response === JSONTRUE) {
                            $('[data-iframecontainer] object').attr('data', r.data.URL);
                            $("[data-documentbody]").slideDown();
                            blackout.hide();
                            App.xDom.closeForm();
                            $.gritter.add({
                                title: "OK!",
                                text: "<i class='fa fa-clock-o'></i> <i>Se ha detectado correctamente el folio y es válido</i>",
                                class_name: 'growl-success',
                                sticky: false,
                                time: 2000
                            });
                        } else {
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "Error",
                                    text: "<i class='fa fa-clock-o'></i> <i>El Folio <b>NO</b> existe, verifíquelo</i>",
                                    class_name: 'growl-danger',
                                    sticky: false,
                                    time: 2000
                                });
                            });
                        }
                    } catch (e) {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>No se ha podido recuparar la información</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                }, 1);
            }).error(function () {
                blackout.hide(function () {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 4000
                    });
                });
            });
        });
    }
});


$.validator.addMethod("folioLenght", function (value, element) {
    if (value.length === 10) {
        return true;
    } else {
        return false;
    }
}, "El largo no es válido");

$('[name=folio]').keydown(function(e){
    if(e.wich === 17 || e.wich===74 || e.keyCode === 17 || e.keyCode===74){
        return false;
    }
    if(e.wich === 13 || e.keyCode === 13){
        e.preventDefault();
        setTimeout(function(){
            $("#primal-form").submit();
        },12);
        return false;
    }
});
