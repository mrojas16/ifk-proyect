$(window).load(function () {
    $('#status').fadeOut();
    $('#preloader').delay(350).fadeOut(function () {
        $('body').delay(350).css({'overflow': 'visible'});
    });
});
var COUNTER = 0;

$("#unlock").bind('submit', function (e) {
    COUNTER++;
    e.preventDefault();
    var dataForm = "password=" + document.getElementsByName('password')[0].value;
    blackout.show('Espere...', function () {
        $.ajax({
            type: "POST",
            url: "[:__BASE_URL:]index/lock/unlock",
            data: dataForm
        }).done(function (response) {
            try {
                var r = JSON.parse(response);
                if (r.response === 1) {
                    var h = (window.location.hash==="")?'#index/dashboard':window.location.hash;
                    document.location = '[:__BASE_URL:]' + h;
                } else {
                    if (COUNTER < 3) {
                        blackout.hide(function () {
                            loginMSG.show("Error Desbloqueando <b>Verifíque su Contraseña</b>.");
                        });
                    } else {
                        window.location = '[:__BASE_URL:]index/lock/logout';
                    }
                }
            } catch (e) {
                if (COUNTER < 3) {
                    blackout.hide(function () {
                        loginMSG.show("Error Desbloqueando.");
                    });
                } else {
                    window.location = '[:__BASE_URL:]index/lock/logout';
                }
            }
        });
    });
});

var loginMSG = function () {
    return {
        show: function (mensaje, delay, class_i, callback) {
            mensaje || (mensaje = 'Error desconocido');
            if (typeof delay !== "number") {
                delay = 2000;
            }
            if (typeof callback !== "function") {
                callback = function () {
                };
            }
            class_i || (class_i = 'alert-danger');
            if (typeof class_i !== "string") {
                class_i = "alert-danger";
            }
            var $t = $('.mainErrorLogin');
            if (typeof mensaje === "string") {
                $t.html(mensaje).addClass(class_i).fadeIn(function () {
                    callback();
                    setTimeout(function () {
                        $t.fadeOut();
                    }, delay);
                });
            } else if (typeof mensaje === "function") {
                $t.html("Error desconocido").addClass(class_i).fadeIn(function () {
                    mensaje();
                    setTimeout(function () {
                        $t.fadeOut();
                    }, delay);
                });
            } else {
                $t.html("Error desconocido").addClass(class_i).fadeIn(function () {
                    setTimeout(function () {
                        $t.fadeOut();
                    }, delay);
                });
            }
        },
        hide: function (callback) {
            if (typeof callback !== "function") {
                callback = function () {
                };
            }
            var $t = $('.mainErrorLogin');
            if (typeof callback === "function") {
                $t.fadeOut(function () {
                    callback();
                });
            } else {
                $t.fadeOut();
            }
        }
    };
}();