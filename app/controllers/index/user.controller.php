<?php

namespace XWork\App\Controllers\index;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use XWork\Core\Controller;
use XWork\Core\Session;
use XWork\Core\Requests\Post;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        user.controller.php
 * @desc        Gestión de Perfiles
 * @subpackage  XWork\App\Controller\index
 *
 */
class user extends Controller {

    private $usuariosModel;

    public function __construct() {
        parent::__construct();
        $this->isAjax();
        $this->usuariosModel = $this->loadModel('index.administracion', 'usuarios');
    }
    
    public function index() {
        $this->profile();
    }

    public function getId() {
        return -1;
    }

    public function profile() {
        $this->_view->load('index', false);
        $this->_view->loadSelfScript('index/user');
        $this->_view->assign('USER_NAME',  Session::get('nombre'));
        $this->_view->assign('USER_APELLIDO',  Session::get('apellido'));
        $this->_view->assign('USER_RUT',  Session::get('rut'));
        $this->_view->assign('USER_EMAIL',  Session::get('email'));
        $this->_view->assign('USER_ID',  md5(Session::get('idusuario')));
                
        $this->_view->renderizar();
    }

    public function edit_self() {
        echo $this->usuariosModel->edit_self(Post::getAll());
    }

}
