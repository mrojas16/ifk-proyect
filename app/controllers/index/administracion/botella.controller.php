<?php

namespace XWork\App\Controllers\index\administracion;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Controller;
use \XWork\Core\Database\CRUDControllerInterface;
use \XWork\Core\Requests\Post;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        botella.controller.php
 * @desc        Gestion de botella
 * @subpackage  XWork\App\Controller\index\administracion
 *
 */
class botella extends Controller implements CRUDControllerInterface{
    
    private $botellaModel;

    public function __construct() {
        parent::__construct();
        $this->isAjax();
        $this->botellaModel = $this->loadModel('index.administracion', 'botella');
    }

    public function getId() {
        return 8;
    }

    public function index() {
        $this->_view->load('index', false);
        $this->_view->loadSelfScript('index/administracion/botella');
        $this->_view->assign('IFK_PROYECTIFK_EMPRESA_PKALL', $this->botellaModel->getIfk_proyectIfk_empresaPkAll());
        $this->_view->renderizar();
    }

    public function add() {
        echo $this->botellaModel->add(Post::getAll());
    }

    public function delete() {
        echo $this->botellaModel->delete(Post::get('id'));
    }

    public function edit() {
        echo $this->botellaModel->edit(Post::getAll());
    }

    public function get() {
        echo $this->botellaModel->get(Post::get('id'));
    }

    public function get_table() {
        echo $this->botellaModel->getTable();
    }

}
