<?php

namespace XWork\App\Controllers\index\administracion;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Controller;
use \XWork\Core\Database\CRUDControllerInterface;
use \XWork\Core\Requests\Post;
use \XWork\Core\Traits\Domparse;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        usuarios.controller.php
 * @desc        Gestion de Usuarios
 * @subpackage  XWork\App\Controller\index
 *
 */
class usuarios extends Controller implements CRUDControllerInterface{
    use Domparse;
    
    private $usuariosModel;

    public function __construct() {
        parent::__construct();
        $this->isAjax();
        $this->usuariosModel = $this->loadModel('index.administracion', 'usuarios');
    }

    public function getId() {
        return 3;
    }

    public function index() {
        $this->_view->load('index', false);
        $this->_view->loadSelfScript('index/administracion/usuarios');
        $obj = $this->usuariosModel->getCd_perfilIdperfil_pkall();
        $this->_view->assign('CD_PERFILIDPERFIL_PKALL',  $this->getSelectOptions($obj, 'idperfil', 'nombre', 'Seleccione un Perfil'));
        $this->_view->renderizar();
    }

    public function add() {
        echo $this->usuariosModel->add(Post::getAll());
    }

    public function delete() {
        echo $this->usuariosModel->delete(Post::get('id'));        
    }

    public function edit() {
        echo $this->usuariosModel->edit(Post::getAll());
    }

    public function get() {
        echo $this->usuariosModel->get(Post::get('id'));
    }

    public function get_table() {
        echo $this->usuariosModel->getTable();
    }

}
