<?php

namespace XWork\App\Controllers\index\administracion;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Controller;
use \XWork\Core\Database\CRUDControllerInterface;
use \XWork\Core\Requests\Post;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        botellas.controller.php
 * @desc        Gestion de botellas
 * @subpackage  XWork\App\Controller\index\administracion
 *
 */
class botellas extends Controller implements CRUDControllerInterface{
    
    private $botellasModel;

    public function __construct() {
        parent::__construct();
        $this->isAjax();
        $this->botellasModel = $this->loadModel('index.administracion', 'botellas');
    }

    public function getId() {
        return 8;
    }

    public function index() {
        $this->_view->load('index', false);
        $this->_view->loadSelfScript('index/administracion/botellas');
        $this->_view->assign('IFK_PROYECTIFK_EMPRESA_PKALL', $this->botellasModel->getIfk_proyectIfk_empresaPkAll());
        $this->_view->renderizar();
    }

    public function add() {
        echo $this->botellasModel->add(Post::getAll());
    }

    public function delete() {
        echo $this->botellasModel->delete(Post::get('id'));
    }

    public function edit() {
        echo $this->botellasModel->edit(Post::getAll());
    }

    public function get() {
        echo $this->botellasModel->get(Post::get('id'));
    }

    public function get_table() {
        echo $this->botellasModel->getTable();
    }

}
