<?php


namespace XWork\App\Controllers\index\administracion;

use XWork\Core\Controller;
use XWork\Core\Database\CRUDControllerInterface;
use XWork\Core\Requests\Post;


class empresas extends Controller implements CRUDControllerInterface{
    
    private $empresasModel;
    
    public function __construct() {
        parent::__construct();
        $this->empresasModel = $this->loadModel('index.administracion', 'empresas');
    }
    
    public function getId() {
        return 7;
    }

    public function index() {
        $this->_view->load('index',false);
        $this->_view->loadSelfScript('index/administracion/empresas');
        $this->_view->renderizar();
        
    }

    public function add() {
        echo $this->empresasModel->add(Post::getAll());
    }

    public function delete() {
        echo $this->empresasModel->delete(Post::get('idempresa'));
        
    }

    public function edit() {
       echo $this->empresasModel->edit(Post::getAll());
        
    }

    public function get() {
        echo $this->empresasModel->get(Post::get('idempresa'));
        
    }

    public function get_table() {
        echo $this->empresasModel->getTable();
        
    }

}
