<?php

namespace XWork\App\Controllers\index;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use XWork\Core\Controller;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        menu.controller.php
 * @desc        Controlador de Menu
 * @subpackage  XWork\App\Controller\index
 *
 */
class menu extends Controller{
    
    public function __construct() {
        parent::$_loginException = true;
        parent::__construct();
        $this->menuModel = $this->loadModel('index','menu');
    }
    
    public function getId() {
        return -1;
    }
   
    public function index() {
        $this->_stm = $this->menuModel->getAllMenu();
        $_view = new \XWork\Core\View(new \XWork\Core\Request(), 'MenuHandler',TRUE);
        $_view->load(VIEWS.'index'.DS.'menu'.DS.'index.xview', FALSE, DEFAULT_LAYOUT, 'menu');
        $x = $this->getBody();
        $_view->assign('BODY_ALL_MENU', $x);
        return $_view->renderizar(false);
    }
    //TODO: SELECTED MENUS
    private function getBody() {
        $r = '';
        for ($i = 0; $i < count($this->_stm); $i++) {
            $ares = $this->_stm[$i];
            if ($ares->idpadre == 0) {
                $r .= '<li';
                if ($ares->hijos == 1) {
                    $r .= '><a href="#" title="' . $ares->nombre . '"  ><i class="fa fa-' . $ares->clase . '"></i> <span class="nav-label">' . $ares->nombre . '</span></a>';
                    $r .= '<ul class="nav nav-second-level">';
                    $r .= $this->getHijos($ares->idmenu, $ares->href);
                    $r .= '</ul>';
                } else {
                    $r .='><a href="' . BASE_URL . '#index/' . $ares->href . '" title="' . $ares->nombre . '" aria-id="' . $ares->idmenu . '" data-isdeadendlink>'
                            . '<i class="fa fa-' . $ares->clase . '"></i> <span class="menu-item-parent">' . $ares->nombre . '</span>'
                            . '</a>';
                }
                $r .= '</li>';
            }
        }
        return $r;
    }

    private function getHijos($id, $href) {
        $r = '';
        for ($i = 0; $i < count($this->_stm); $i++) {
            $ares = $this->_stm[$i];
            if ($ares->idpadre == $id) {
                $r .= '<li>';
                if ($ares->hijos == 1) {
                    $r .= '<a href="#" title="' . $ares->nombre . '"  aria-id="' . $ares->idmenu . '">' . $ares->nombre . '</a>';
                    $r .= '<ul class="nav nav nav-third-level">';
                    $r .= $this->getHijos($ares->idmenu, $href . '.' . $ares->href);
                    $r .= '</ul>';
                } else {
                    $r .= '<a href="' . BASE_URL . '#index.' . $href . '/' . $ares->href . '" title="' . $ares->nombre . '"  aria-id="' . $ares->idmenu . '" data-isdeadendlink>' . $ares->nombre . '</a>';
                }
                $r .= '</li>';
            }
        }
        return $r;
    }

}
