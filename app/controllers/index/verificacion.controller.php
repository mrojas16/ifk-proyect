<?php

namespace XWork\App\Controllers\index;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use XWork\Core\Controller;
use XWork\Core\Session;
use XWork\Core\Requests\Post;
use XWork\Core\Misc\ReporteController;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        verficacion.controller.php
 * @desc        Modulo de verificacion de folios
 * @subpackage  XWork\App\Controller\index
 *
 */
class verificacion extends Controller implements ReporteController {

    private $verificacionModel;

    public function __construct() {
        parent::__construct();
        $this->verificacionModel = $this->loadModel('index', 'verificacion');
    }

    public function getId() {
        return 13;
    }

    public function index() {
        $this->isAjax();
        $this->_view->load('index', false);
        $this->_view->loadSelfScript('index/verificacion');
        $this->_view->renderizar();
    }

    public function get() {
        $this->isAjax();
        echo $this->verificacionModel->get(Post::get('folio'));
    }
    
    public function getpdf($id=0) {
        $this->reportesModel = $this->loadModel('index.reportes', 'certificados');
        $tipo = $this->reportesModel->getTipoDoc($id);
        if($id===0){
            die('<center><b>No se ha podido cargar el PDF.</b></center>');
        } else {
            $datos = $this->reportesModel->getDatos($id, $tipo);
            if ($datos) {
                if ($tipo == 1) {
                    $pdf = $this->loadHelper('index.mantencion', 'emision');
                    $pdf->get($datos,true);
                } else if ($tipo == 2) {
                    $pdf = $this->loadHelper('index.mantencion', 'competencia');
                    $pdf->get($datos,true);
                } else if ($tipo == 3) {
                    $pdf = $this->loadHelper('index.mantencion', 'psicosensotecnico');
                    $pdf->get($datos,true);
                } else {
                    die('<center><b>No se ha podido cargar el PDF.</b></center>');
                }
            } else {
                die('<center><b>No se ha podido cargar el PDF.</b></center>');
            }
        }
    }

}
