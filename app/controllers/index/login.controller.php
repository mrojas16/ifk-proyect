<?php

namespace XWork\App\Controllers\index;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use XWork\Core\Request;
use XWork\Core\Requests\Post;
use XWork\Core\Session;
use XWork\Core\Response;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        menu.controller.php
 * @desc        Controlador de Login
 * @subpackage  XWork\App\Controller\index
 *
 */
class login extends \XWork\Core\Controller {

    private $loginModel;

    public function __construct() {
        parent::$_loginException = TRUE;
        parent::__construct();
        $this->loginModel = $this->loadModel('index', 'login');
    }

    public function getId() {
        return -1;
    }

    public function index() {
        if (Session::get(SESSION_ONLOGIN)) {
            $this->redireccionar('index/dashboard');
        } else {
            $this->_view->load('index', false);
            $this->_view->assign('EXPLICIT_SCRIPT', $this->_view->loadSelfScript('index/login'));
            $this->_view->renderizar();
        }
    }

    public function login() {
        try {
            if ($this->loginModel->getLogin(Post::get('email'), Post::get('password'))) {
                $info = $this->loginModel->usuario;

                Session::set('idusuario', $info->idusuario);
                Session::set('admin_acopio', (($info->admin_acopio==1)?true:false));
                Session::set('email', $info->email);
                Session::set('nombre', $info->nombre);
                Session::set('apellido', $info->apellido);
                Session::set('full_name', $info->nombre . ' ' . $info->apellido);
                Session::set('rut', $info->rut);
                Session::set('fecha_creacion', $info->fecha_creacion);
                Session::set('fecha_edicion', $info->fecha_edicion);
                Session::set('fecha_eliminacion', $info->fecha_eliminacion);
                Session::set('idperfil', $info->idperfil);
                Session::set('profile_name', $info->profile_name);
                Session::set('permisos', $this->loginModel->getAllPermisos($info->idperfil));
                Session::set(SESSION_ONLOGIN, 1);
                Session::set('locked', false);
                echo Response::JSONTRUE;
                die();
            } else {
                echo Response::JSONFALSE;
                die();
            }
        } catch (\Exception $e) {
            print_r($e);
            echo Response::JSONFALSE;
            die();
        }
    }

    public function logout() {
        $r = new Request();
        $a = $r->getArgs();
        $hash = "";
        if (count($a)) {
            $hash = "#" . implode('/', $a);
        }
        Session::destroy();
        $this->redireccionar('index/login' . $hash);
    }

    public function recover() {
        $mail = Post::get('email');
        if ($this->loginModel->recover($mail)) {
            try {
                $mailHelper = $this->loadHelper('mail');
                $destinatario_mail = $mail;
                $subject = utf8_decode(APP_NAME_TITLE . " | Recuperación de Contraseñas");
                $mensaje = '
                            <html>
                            <head>
                            <title>Recuperaci&oacute; de Contrase&ntilde;as - ' . APP_NAME_TITLE . '</title>
                            </head>
                            <body>    
                            <p>Saludos:</p>
                            <p>Se ha generado una solicitud para recuperar su contrase&ntilde;a<br>
                            Debe acceder al sistema desde <a href="' . BASE_URL . '">aqui</a>, luego <br />
                           Ingrese su actual correo y esta contrase&ntilde;a <br><br></p>
                            <p><center><b>' . $this->loginModel->new_pass . '</b></center>
                            <br><br></p>
                            </body>
                            </html>';
                $mailHelper->sendSimpleMail($mensaje, $destinatario_mail, "Usuario", $subject);
                echo Response::JSONTRUE;
            } catch (\Exception $ex) {
                echo Response::JSONTRACEERROR('Se ha Producido un error Enviando el correo', 1);
            }
        } else {
            echo Response::JSONFALSE;
        }
    }

}
