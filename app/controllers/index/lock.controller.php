<?php

namespace XWork\App\Controllers\index;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use XWork\Core\Session;
use XWork\Core\Requests\Post;
use XWork\Core\Response;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        menu.controller.php
 * @desc        Controlador de Login
 * @subpackage  XWork\App\Controller\index
 *
 */
class lock extends \XWork\Core\Controller {

    private $loginModel;

    public function __construct() {
        parent::$_loginException = TRUE;
        parent::__construct();
        $this->loginModel = $this->loadModel('index', 'login');
    }

    public function getId() {
        return -1;
    }

    public function index() {
        Session::set('locked', true);
        $this->_view->load('index', false);
        $this->_view->assign('EXPLICIT_SCRIPT', $this->_view->loadSelfScript('index/lock'));
        $this->_view->renderizar();
    }
    
    public function unlock() {
        $pass = Post::get('password');
        if ($this->loginModel->getLogin(Session::get('email'), $pass)) {
            Session::set('locked', false);
            echo Response::JSONTRUE;
        } else {
            echo Response::JSONFALSE;
        }
    }
    
    public function logout() {
        \XWork\Core\Session::destroy();
        $this->redireccionar('index/login');
    }

}
