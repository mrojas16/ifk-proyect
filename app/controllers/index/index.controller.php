<?php

namespace XWork\App\Controllers\index;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use XWork\Core\Controller;
use XWork\Core\Session;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        index.controller.php
 * @desc        Punto de Arranque de la Aplicacion
 * @subpackage  XWork\App\Controller\index
 *
 */
class index extends Controller {

    public function __construct() {
        parent::$_loginException = true;
        parent::__construct();
    }

    public function index() {
        if(\XWork\Core\Session::get(SESSION_ONLOGIN)==1){
            $this->_view->load('index', true);
            $this->_view->loadSelfScript('index/index');
            $this->_view->assign('__MAIN_MENU', $this->getMenu());
            $this->_view->assign('TEMPORADA_ACTUAL', Session::get('current_temporada_desc')['desc']);
            $this->_view->assign('ALL_TEMPORADAS', $this->getAllTemporadasDropDown());
            $this->_view->assign('ACOPIO_ACTUAL', Session::get('current_acopio_desc')['desc']);
            $this->_view->assign('ALL_ACOPIOS', $this->getAllAcopiosDropDown());
            $this->_view->renderizar();
        } else {
            $this->redireccionar('index/login/logout');
        }
    }
    
    private function getAllTemporadasDropDown() {
        $all = Session::get('temporadas');
        $r = '';
        for ($i = 0; $i < count($all); $i++) {
            $a = $all[$i];
            $active = '';
            if($i!=0){
                $r .= '<li class="divider"></li>';
            }
            if(Session::get('current_temporada') == $a['id']){
                $active = ' class="active"';
            }
            $r .= '<li ' . $active . '>
                        <a href="#" data-v="' . base64_encode($a['id']) . '">
                            <div>' . $a['desc'] . '</div>
                        </a>
                    </li>';
        }
        return $r;
    }
    
    private function getAllAcopiosDropDown() {
        $all = Session::get('acopios');
        $r = '';
        for ($i = 0; $i < count($all); $i++) {
            $a = $all[$i];
            $active = '';
            if($i!=0){
                $r .= '<li class="divider"></li>';
            }
            if(Session::get('current_acopio') == $a['id']){
                $active = ' class="active"';
            }
            $r .= '<li ' . $active . '>
                        <a href="#" data-v="' . base64_encode($a['id']) . '">
                            <div>' . $a['desc'] . '</div>
                        </a>
                    </li>';
        }
        return $r;
    }

    public function getId() {
        return -1;
    }

    private function getMenu() {
        require_once CONTROLLERS . DS . DEFAULT_MENU . '.controller.php';
        $menu = new \XWork\App\Controllers\index\menu();
        return $menu->index();
    }

}
