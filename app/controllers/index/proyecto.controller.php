<?php

namespace XWork\App\Controllers\index;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use XWork\Core\Controller;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        index.controller.php
 * @desc        Punto de Arranque de la Aplicacion
 * @subpackage  XWork\App\Controller\index
 *
 */
class proyecto extends Controller {
    
    private $proyectoModel;

    public function __construct() {
        parent::__construct();
        $this->proyectoModel = $this->loadModel('index', 'proyecto');
        $this->isAjax();
    }

    public function index() {
        $this->_view->load('index', false);
        $this->_view->loadSelfScript('index/proyecto');
        $this->_view->renderizar();
    }

    public function getId() {
        return 6;
    }

    /*     * ***************************************** */
    /*
      /*      AJAX ACTIONS
      /*
      /******************************************* */

    public function proyect_all() {
        $a = $this->createNewInstanceOfView();
        $a->load('proyect_all', false);
        $a->assign('VARIABLE_PRUEBA', $this->proyectoModel->getVariablePrueba());
        $view = $a->renderizar(false);
        echo \XWork\Core\Response::JSONDATA(array('body'=>$view));
    }
    
    public function sprint_all(){
        $a = $this->createNewInstanceOfView();
        $a->load('sprint_all', false);
        $a->assign('VARIABLE_VIEJA', $this->proyectoModel->getVariableVieja());
        $view = $a->renderizar(false);
        echo \XWork\Core\Response::JSONDATA(array('body'=>$view));
        }
        
        public function proyect_link(){
        $a = $this->createNewInstanceOfView();
        $a->load('proyect_link', false);
        $a->assign('PROYECT_LINK', $this->proyectoModel->getVariableLink());
        $view = $a->renderizar(false);
        echo \XWork\Core\Response::JSONDATA(array('body'=>$view));
        }
        
        public function sprint_link(){
        $a = $this->createNewInstanceOfView();
        $a->load('sprint_link', false);
        $a->assign('SPRINT_LINK', $this->proyectoModel->getVariableSprint());
        $view = $a->renderizar(false);
        echo \XWork\Core\Response::JSONDATA(array('body'=>$view));
        }
        
        public function createProyect(){
        $a = $this->createNewInstanceOfView();
        $a->load('create_proyect', false);
        $a->assign('CREATE_PROYECT', $this->proyectoModel->createProyect());
        $view = $a->renderizar(false);
        echo \XWork\Core\Response::JSONDATA(array('body'=>$view));
        }
        
        

}
