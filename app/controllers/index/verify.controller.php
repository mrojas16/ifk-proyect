<?php

namespace XWork\App\Controllers\index;

if (!defined('XWORKVERSION')) {
    die('{"RESPONSE":0,"DATA":{"MESSAGE":"FORBIDDEN ACCESS"}}');
}

use \XWork\Core\Controller;
use \XWork\Core\Response;
use \XWork\Core\Request;
use \XWork\Core\Requests\Post;
use \XWork\Core\Session;

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        verify.controller.php
 * @desc        Sistema general de verificaciones, utiliza solo SJAX como medio de traspaso
 * @subpackage  XWork\App\Controller\index
 *
 */
class verify extends Controller {
    
    private $verifyModel;

    public function __construct() {
        parent::__construct();
        $this->verifyXDOMRequest();
        $this->verifyModel = $this->loadModel('index','verify');
    }

    public function index() {
        echo Response::JSONFALSE;
    }

    public function getId() {
        return -1;
    }

    private function verifyXDOMRequest() {
        $a = getallheaders();
        if (!empty($a) && isset($a['XJENGINE_XWORK']) && (isset($a['XDOM_METHOD']) && ($a['XDOM_METHOD'] == "SJAX" || $a['XDOM_METHOD'] == "JQUERY"))) {
            return;
        } else {
            $r = new Request();
            $this->redireccionar('#index/dashboard');
        }
    }
    
    public function no_repeat() {
        $p = Post::getAll();
        $params = explode(',', $p['_p']);
        echo $this->verifyModel->noRepeat($p['_v'],$p['_k'],$params[0],$params[1]);
    }
    
    public function max_stock() {
        $pk = Post::get('_k');
        $lote = Post::get('_l');
        $bod = Post::get('_b');
        echo $this->verifyModel->maxStock($pk,$bod,$lote);       
    }
    
    public function lote() {
        $value = Post::get('_v');
        $bod = Post::get('_b');
        echo $this->verifyModel->lote($value,$bod);       
    }
    
    public function session() {
        $last = Session::get('last_timestamp');
        $now = date("Y-n-j H:i:s"); 
        $transcurred = ((int)strtotime($now) - (int)($last));
        
        if($transcurred>=(SESSION_TIME*60)){
            echo Response::JSONFALSE;
        } else {
            echo Response::JSONTRUE;
        }
    }
    
    public function rut_trabajador() {
        $value = Post::get('_v');
        echo $this->verifyModel->rutTrabajador($value);  
    }
    
    public function topswitch() {
        $tipo = Post::get('_t');
        $id = base64_decode(Post::get('id'));
        if($tipo == "acopio"){
            Session::set('current_acopio', $id);
            Session::set('current_acopio_desc',$this->getDesc($id,1));

        } else if($tipo == "temporada"){
            Session::set('current_temporada', $id);
            Session::set('current_temporada_desc',$this->getDesc($id,2));
        } else {
            echo Response::JSONFALSE;
            die();
        }
        echo Response::JSONTRUE;
    }
    
    public function getDesc($id,$tipo) {
        if ($tipo == 1) {
            $h = Session::get('acopios');
        } else {
            $h = Session::get('temporadas');
        }
        for ($i = 0; $i < count($h); $i++) {
            $a = $h[$i];
            if($a['id'] == $id){
                return $a;
            }
        }
        return array('id'=>'','desc'=>'');
    }

}
